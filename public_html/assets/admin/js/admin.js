$(function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': window.Laravel.csrfToken
    }
  });

  new Blazy();

  $('.dropdown-select').on('click', '.dropdown-menu > li > a', function (e) {
    var option = $(this);
    e.preventDefault();

    $(e.delegateTarget).find('.dropdown-text').text(option.text());
    $(e.delegateTarget).find('.dropdown-value').val(option.data('value'));
  });

  $('.delete-form').on('click', 'button', function (e) {
    e.preventDefault();
    var form = $(e.delegateTarget);
    bootbox.confirm('Tem a certeza que deseja apagar este registo?', function (result) {
      if (result) {
        form.submit();
      }
    });
  });

  tinymce.init({
    selector: "textarea.text-editor",
    plugins: [
      "advlist autolink link image lists charmap hr",
      "searchreplace wordcount visualblocks visualchars code fullscreen nonbreaking",
      "table contextmenu directionality template textcolor paste textcolor colorpicker textpattern"
    ],

    language: 'pt_PT',
    skin: 'other',
    convert_urls: false,

    toolbar1: "styleselect formatselect | undo redo | bold italic underline strikethrough",
    toolbar2: "alignleft aligncenter alignright alignjustify | outdent indent | bullist numlist | blockquote link unlink image table | hr removeformat | subscript superscript | charmap | code searchreplace",
    paste_as_text: true,
    menubar: false,
    toolbar_items_size: 'small',
    entity_encoding: "raw",
    file_browser_callback: function (field_name, url, type, win) {

      var textarea = $(tinymce.activeEditor.getElement());

      if (type === 'file') {
        link = textarea.data('plugin-file-src');
      } else {
        link = textarea.data('plugin-image-src');
      }

      tinymce.activeEditor.windowManager.open({
        title: 'Seleccionar Ficheiro',
        file: link,
        width: 984,
        height: 600,
        resizable: 'yes'
      }, {
        oninsert: function (url) {
          win.document.getElementById(field_name).value = url;
        }
      });
      return false;
    }
  });

  $(document).on('click', '[data-plugin-file-choose-select]', function (e) {
    e.preventDefault();
    var windowManager = top.tinymce.activeEditor.windowManager;

    if (windowManager.getParams()) {
      windowManager.getParams().oninsert($(this).data('plugin-file-choose-select'));
      windowManager.close();
    }

  });

  $("input[type='radio']").on('change', function () {
    var radio = $(this);
    var collapse = $('[data-collapsedBy=' + radio.attr('name') + ']');

    if (collapse.length){
      if (radio.val() == '1') {
        collapse.show();
      } else {
        collapse.hide();
      }
    }
  });

  $('select.form-control:not(.simple)').selectize({
    plugins: ['remove_button'],
    create: false
  });

  $('[data-plugin-selectize-tags]').selectize({
    plugins: ['remove_button'],
    delimiter: ',',
    persist: false,
    create: function (input) {
      return {
        value: input,
        text: input
      }
    }
  });

  $('[data-plugin-mini-colors]').colorpicker();

  $('[data-plugin-datepicker]').datepicker({
    format: "yyyy-mm-dd",
    weekStart: 1,
    autoclose: true,
    orientation: "top auto"
  });

  $('[data-inputmask]').inputmask();

  var fileChoose = $('[data-plugin-file-choose]');

  fileChoose.on('click', '.file-choose-clear', function (e) {
    e.preventDefault();

    $(e.delegateTarget).find('input[type=text]').val('');
  });

  fileChoose.on('click', '.file-choose-button', function (e) {
    e.preventDefault();

    var fileChooseInput = $(e.delegateTarget).find('input[type=text]');
    var url = $(this).data('url');

    var iframe = $('<iframe class="iframe" src="' + url + '" style="width:100%;height:100%;border:none;margin:0 auto;display:block;"></iframe>');

    fileChooseInput.attr('data-file-choose-active', '');

    popup = '<div class="modal" tabindex="-1" role="dialog" style="visibility:hidden;">';
    popup += '<div class="modal-dialog" style="width:90%">';
    popup += '<div class="modal-content">';
    popup += '<div class="modal-header">';
    popup += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
    popup += '<h4 class="modal-title">Seleccionar Ficheiro</h4>';
    popup += '</div>';
    popup += '<div class="modal-body" style="padding:0"></div>';
    popup += '</div>';
    popup += '</div>';
    popup += '</div>';

    popup = $(popup);

    iframe.load(function () {
      $(this).height($(this).contents().height());

      iframe.contents().on('click', '[data-plugin-file-choose-select]', function (e) {
        e.preventDefault();
        fileChooseInput.val($(this).data('plugin-file-choose-select'));
        fileChooseInput.removeAttr('data-file-choose-active', '');
        popup.modal('hide');
      });

      popup.css('visibility', 'visible');
    });

    $(window).bind('resize', function () {
      iframe.height(iframe.contents().height());
    });

    popup.find('.modal-body').append(iframe);

    popup.modal('show');
    popup.on('hidden.bs.modal', function () {
      $(this).remove();
    });
  });

  $(document).on('click', '[data-provide=delete-ajax]',function(e) {
    var link = $(this);
    e.preventDefault();

    bootbox.confirm('Tem a certeza que deseja apagar este registo?', function (result) {
      if (result) {
        $.ajax({
          method: 'DELETE',
          url: link.data('href'),
        }).done(function() {
          link.closest('tr').remove();
        });
      }
    });
  });
});
