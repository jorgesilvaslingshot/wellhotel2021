$(function () {
  var fileupload = $('#fileupload');

  fileupload.fileupload({
    dataType: 'json',
    // Enable image resizing, except for Android and Opera,
    // which actually support image resizing, but fail to
    // send Blob objects via XHR requests:
    disableImageResize: /Android(?!.*Chrome)|Opera/
      .test(window.navigator.userAgent),
    previewMaxWidth: 50,
    previewMaxHeight: 50,
    previewCrop: true,
    formData: {
      directory: fileupload.data('directory')
    }
  })
    .on('fileuploadadd', function (e, data) {
      data.context = [];

      $.each(data.files, function (index, file) {
        var fileNode = $('<td/>')
          .text(file.name)
          .addClass('text-middle');

        var node = $('<tr/>')
            .addClass('fade')
            .append($('<td/>').css('width', '1%'))
            .append(fileNode)
            .append($('<td/>').css('width', '1%').html('<div class="file-feedback"><i class="fa fa-circle-o-notch fa-spin"></i></div>').addClass('text-middle'))
          ;
        data.context.push(node);
      });
      $('#uploads-table').children('tbody').prepend(data.context);
    })
    .on('fileuploadprocessalways', function (e, data) {
      var index = data.index,
        file = data.files[index],
        extension = file.name.split('.').pop().toLowerCase(),
        node = data.context[index].children('td').first();

      if (file.preview) {
        node.append(file.preview);
      }
      else {
        var iconPreview = $('<div class="file-icon"></div>');

        if (['jpg', 'jpeg', 'png', 'gif'].indexOf(extension) >= 0) {
          iconPreview.append('<i class="fa fa-file-image-o"></i>');
        }
        else if (extension == 'pdf') {
          iconPreview.append('<i class="fa fa-file-pdf-o"></i>');
        }
        else if (['doc', 'docx'].indexOf(extension) >= 0) {
          iconPreview.append('<i class="fa fa-file-word-o"></i>');
        }
        else if (['xls', 'xlsx', 'csv'].indexOf(extension) >= 0) {
          iconPreview.append('<i class="fa fa-file-excel-o"></i>');
        }
        else {
          iconPreview.append('<i class="fa fa-file-o"></i>');
        }

        node.append(iconPreview);
      }

      node.parent().addClass('in');
    })
    .on('fileuploaddone', function (e, data) {
      $.each(data.result, function (index, file) {
        var context = data.context[index],
          column1 = context.children('td:eq(0)'),
          column2 = context.children('td:eq(1)'),
          column3 = context.children('td:eq(2)'),
          fileFeedback = column3.find('.fa');


        if (file.success) {
          var href = $('<a>')
            .attr('target', '_blank')
            .prop('href', file.path);

          var link = $('<small/>')
            .text(file.path);

          column2.append('<br/>')
            .append(link)
          ;

          link.wrap(href);

          column1.children().wrap(href);

          if (fileupload.data('popup') == 1) {
            var fileChooseLink = $('<a href="#" class="btn btn-xs btn-success" data-plugin-file-choose-select="' + file.path + '"><i class="fa fa-check" data-toggle="tooltip" data-placement="top" data-original-title="Seleccionar"></i></a>');

            column3.empty().addClass('text-center').append(fileChooseLink);
          }
          else {
            fileFeedback.removeClass()
              .addClass('fa fa-check text-green');
          }

        } else {
          var message = $('<small/>')
            .addClass('text-red')
            .text(file.message);

          column2.append('<br/>')
            .append(message)
          ;

          fileFeedback.removeClass()
            .addClass('fa fa-ban text-red');
        }
      });
    })
    .on('fileuploadfail', function (e, data) {
      var error = $(this).data('on-error');

      $.each(data.files, function (index) {
        var context = data.context[index],
          column2 = context.children('td:eq(1)'),
          column3 = context.children('td:eq(2)'),
          fileFeedback = column3.find('.fa'),
          message = $('<small/>')
            .addClass('text-red')
            .text(error);

        column2.append('<br/>')
          .append(message)
        ;

        fileFeedback.removeClass()
          .addClass('fa fa-ban text-red');
      });
    })
  ;

});
