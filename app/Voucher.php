<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    use Translatable;

    public $translatedAttributes = ['slug', 'name', 'label', 'subtitle', 'description', 'excerpt', 'includes'];
    protected $fillable = ['cover_image', 'cover_image_2', 'publish'];
    protected $with = ['translations'];

    public function getId()
    {
        return $this->id;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getSubtitle()
    {
        return $this->subtitle;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getExcerpt()
    {
        if (!empty($this->excerpt)) {
            return $this->excerpt;
        }

        return str_limit(strip_tags($this->description), 180);
    }

    public function getImage()
    {
        return $this->cover_image;
    }

    public function getThumbnail()
    {
        if (!empty($this->cover_image_2)) {
            return $this->cover_image_2;
        }

        return $this->cover_image_1;
    }

    /**
     * @param null $limit
     * @return Collection
     *
     */
    public function getPublishedVouchers($limit = null)
    {
        return static::where('publish', 1)
            ->orderBy('priority', 'desc')
            ->limit($limit)
            ->get();
    }

    public function getPublishedVouchersLang($limit = null, $locale)
    {
        return static::where('publish', 1)
            ->orderBy('priority', 'desc')
            ->select('id')
            ->whereTranslation('locale', $locale)
            ->limit($limit)
            ->get();
    }

    public static function getPublishedVoucherBySlugAndLocale($slug, $locale)
    {
        return static::where('publish', 1)
            ->whereTranslation('slug', $slug)
            ->whereTranslation('locale', $locale)
            ->first();
    }

    public function setPriorityAttribute($value)
    {
        $this->attributes['priority'] = $value ?: 0;
    }

    public function saveOrCreate($data, $id)
    {
        $el = static::find($id) ?: new static;
        $el->cover_image_2 = $data['cover_image_2'];
        $el->priority = $data['priority'];
        $el->publish = $data['publish'];

        foreach ($data['languages'] as $locale => $values) {
            if ($values['name']) {
                $translation = $el->translateOrNew($locale);
                $translation->name = $values['name'];
                $translation->label = $values['label'];
                $translation->subtitle = $values['subtitle'];
                $translation->description = $values['description'];
                $translation->excerpt = array_get($values, 'excerpt');
            }
        }

        $el->save();
    }
}
