<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvailabilityRequest extends Model
{
    public $fillable = [
        'locale', 'name', 'email', 'phone', 'arrival',
        'departure', 'adults', 'children', 'price', 'product_name', 'product_type',
    ];

    protected $dates = [
        'arrival',
        'departure',
    ];

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getArrival()
    {
        return $this->arrival;
    }

    public function getDeparture()
    {
        return $this->departure;
    }

    public function getAdults()
    {
        return $this->adults;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getProductName()
    {
        return $this->product_name;
    }

    public function getProductType()
    {
        return $this->product_type;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function registerAvailabilityRequest($data, $locale)
    {
        return static::create(array_merge($data, compact('locale')));
    }
}
