<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Phrase extends Model
{
    use Translatable;

    public $translatedAttributes = ['content'];
    public $fillable = ['name', 'slug'];
    public $timestamps = false;
    protected $with = ['translations'];

    public function saveOrCreate($data, $id = null)
    {
        $el = static::updateOrCreate(compact('id'), $data);

        foreach ($data['locales'] as $locale => $values) {
            $translation = $el->translateOrNew($locale);
            $translation->content = $values['content'];
        }

        $el->save();
    }
}
