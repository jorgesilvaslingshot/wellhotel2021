<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = ['title', 'description','image', 'action_title', 'action_link', 'locale', 'priority', 'publish'];

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function hasAction()
    {
        return !empty($this->action_title) && !empty($this->action_link);
    }

    public function getActionTitle()
    {
        return $this->action_title;
    }

    public function getActionLink()
    {
        return $this->action_link;
    }

    public function saveOrCreate($data, $id)
    {
        static::updateOrCreate(compact('id'), $data);
    }

    public function setPriorityAttribute($value)
    {
        $this->attributes['priority'] = $value ?: 0;
    }

    public function getPublishedSlidesByLocale($locale, $limit = null)
    {
        return static::where('publish', 1)
            ->where('locale', $locale)
            ->orderBy('priority', 'desc')
            ->limit($limit)
            ->get();
    }
}
