<?php

namespace App\Traits;

trait Localization
{
    public function getLocale()
    {
        return app('laravellocalization')->getCurrentLocale();
    }
}
