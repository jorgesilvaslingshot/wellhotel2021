<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['author', 'message', 'date', 'locale','priority', 'publish'];

    protected $dates = [
        'date'
    ];

    public function getAuthor()
    {
        return $this->author;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function saveOrCreate($data, $id)
    {
        static::updateOrCreate(compact('id'), $data);
    }

    public function setPriorityAttribute($value)
    {
        $this->attributes['priority'] = $value ?: 0;
    }

    public function getPublishedMessagesByLocale($locale, $limit = null)
    {
        return static::where('publish', 1)
            ->where('locale', $locale)
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->get();
    }
}
