<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    use Translatable;

    public $translatedAttributes = ['title', 'label'];
    public $fillable = ['start_date', 'end_date', 'link', 'image', 'publish'];
    protected $with = ['translations'];

    public function getId()
    {
        return $this->id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getStartDate()
    {
        return $this->start_date;
    }

    public function getEndDate()
    {
        return $this->end_date;
    }

    public function hasLink()
    {
        return !empty($this->link);
    }

    public function getLink()
    {
        return $this->link;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getPublish($yes = true, $no = false)
    {
        return $this->publish ? $yes : $no;
    }

    protected function generateCode()
    {
        $unique = false;
        $code = null;

        while ($unique == false) {
            $code = str_random();

            $unique = static::where('code', $code)->count() == 0;
        }

        return $code;
    }

    public function saveOrCreate($data, $id = null)
    {
        $el = static::updateOrCreate(compact('id'), $data);

        foreach ($data['locales'] as $locale => $values) {
            $translation = $el->translateOrNew($locale);
            $translation->title = $values['title'];
            $translation->label = $values['label'];
        }

        $el->code = $this->generateCode();

        $el->save();
    }

    public function getSimplePageBySlug($slug)
    {
        return static::where('slug', $slug)
            ->first();
    }

    public function getNextPromotion()
    {
        return static::whereDate('start_date', '<=', date('Y-m-d'))
            ->whereDate('end_date', '>=', date('Y-m-d'))
            ->where('publish', 1)
            ->first();
    }
}
