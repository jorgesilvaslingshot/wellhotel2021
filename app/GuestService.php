<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class GuestService extends Model
{
    use Translatable;

    public $translatedAttributes = ['title', 'description'];
    public $fillable = ['priority', 'publish'];
    protected $with = ['translations'];

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setPriorityAttribute($value)
    {
        $this->attributes['priority'] = $value ?: 0;
    }

    public function getPublishedGuestServices()
    {
        return static::where('publish', 1)
            ->orderBy('priority', 'desc')
            ->orderBy('id', 'asc')
            ->get();
    }

    public function saveOrCreate($data, $id = null)
    {
        $el = static::updateOrCreate(compact('id'), $data);

        foreach ($data['languages'] as $locale => $values) {
            $translation = $el->translateOrNew($locale);
            $translation->title = $values['title'];
            $translation->description = $values['description'];
        }

        $el->save();
    }

    public function getSimplePageBySlug($slug)
    {
        return static::where('slug', $slug)
            ->first();
    }
}
