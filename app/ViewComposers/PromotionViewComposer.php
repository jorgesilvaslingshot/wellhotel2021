<?php

namespace App\ViewComposers;

use App\Promotion;
use App\Traits\Localization;
use Illuminate\Contracts\View\View;

class PromotionViewComposer
{
    use Localization;
    /**
     * @var Promotion
     */
    private $promotionRepo;

    /**
     * PromotionViewComposer constructor.
     * @param Promotion $promotionRepo
     */
    public function __construct(Promotion $promotionRepo)
    {
        $this->promotionRepo = $promotionRepo;
    }

    public function compose(View $view)
    {
        $promotion = $this->promotionRepo->getNextPromotion();

        $view->with(compact('promotion'));
    }
}
