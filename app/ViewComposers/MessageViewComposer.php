<?php

namespace App\ViewComposers;

use App\Message;
use App\Traits\Localization;
use Illuminate\Contracts\View\View;

class MessageViewComposer
{
    use Localization;
    /**
     * @var Message
     */
    private $messageRepo;

    /**
     * MessageViewComposer constructor.
     * @param Message $messageRepo
     */
    public function __construct(Message $messageRepo)
    {
        $this->messageRepo = $messageRepo;
    }

    public function compose(View $view)
    {
        $messages = $this->messageRepo->getPublishedMessagesByLocale($this->getLocale(), 3);

        $view->with(compact('messages'));
    }
}
