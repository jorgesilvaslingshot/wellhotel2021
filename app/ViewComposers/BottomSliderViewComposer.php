<?php

namespace App\ViewComposers;

use App\BottomSlide;
use App\Traits\Localization;
use Illuminate\Contracts\View\View;

class BottomSliderViewComposer
{
    use Localization;

    /**
     * @var BottomSlide
     */
    private $bottomSlideRepo;

    /**
     * BottomSliderViewComposer constructor.
     * @param BottomSlide $bottomSlideRepo
     */
    public function __construct(BottomSlide $bottomSlideRepo)
    {
        $this->bottomSlideRepo = $bottomSlideRepo;
    }

    public function compose(View $view)
    {
        $bottomSlides = $this->bottomSlideRepo->getPublishedBottomSlidesByLocale($this->getLocale(), 4);

        $view->with(compact('bottomSlides'));
    }
}
