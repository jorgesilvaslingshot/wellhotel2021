<?php

namespace App\ViewComposers;

use Artesaos\SEOTools\Contracts\SEOTools;
use Illuminate\Contracts\View\View;
use Illuminate\Support\HtmlString;

class SeoViewComposer
{
    /**
     * @var SEOTools
     */
    private $seo;

    public function __construct(SEOTools $seo)
    {
        $this->seo = $seo;
    }

    public function compose(View $view)
    {
        $seo = new HtmlString($this->seo->generate());

        $view->with(compact('seo'));
    }
}
