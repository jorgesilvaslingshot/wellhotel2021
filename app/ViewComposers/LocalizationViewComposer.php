<?php

namespace App\ViewComposers;

use Illuminate\Contracts\View\View;
use Mcamara\LaravelLocalization\LaravelLocalization;

class LocalizationViewComposer
{
    /**
     * @var LaravelLocalization
     */
    private $localization;

    public function __construct(LaravelLocalization $localization)
    {
        $this->localization = $localization;
    }

    public function compose(View $view)
    {
        $view->with('locale', $this->localization->getCurrentLocale());
    }
}
