<?php

namespace App\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Contracts\Config\Repository as Config;

class AnalyticsViewComposer
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function compose(View $view)
    {
        $view->with('analyticsTrackingId', $this->config->get('app.analytics_tracking_id'));
    }
}
