<?php

namespace App\Events;

use App\ContactRequest;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ContactRequestRegistered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var ContactRequest
     */
    private $contactRequest;

    /**
     * Create a new event instance.
     *
     * @param ContactRequest $contactRequest
     */
    public function __construct(ContactRequest $contactRequest)
    {
        $this->contactRequest = $contactRequest;
    }

    /**
     * @return ContactRequest
     */
    public function getContactRequest(): ContactRequest
    {
        return $this->contactRequest;
    }
}
