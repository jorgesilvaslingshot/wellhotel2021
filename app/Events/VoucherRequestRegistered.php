<?php

namespace App\Events;

use App\VoucherRequest;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class VoucherRequestRegistered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $voucherRequest;

    public function __construct(VoucherRequest $voucherRequest)
    {
        $this->voucherRequest = $voucherRequest;
    }

    public function getVoucherRequest(): VoucherRequest
    {
        return $this->voucherRequest;
    }
}
