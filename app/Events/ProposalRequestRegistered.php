<?php

namespace App\Events;

use App\ProposalRequest;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ProposalRequestRegistered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $proposalRequest;

    public function __construct(ProposalRequest $proposalRequest)
    {
        $this->proposalRequest = $proposalRequest;
    }

    public function getProposalRequest(): ProposalRequest
    {
        return $this->proposalRequest;
    }
}
