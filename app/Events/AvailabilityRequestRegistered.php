<?php

namespace App\Events;

use App\AvailabilityRequest;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AvailabilityRequestRegistered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var AvailabilityRequest
     */
    private $availabilityRequest;

    /**
     * Create a new event instance.
     *
     * @param AvailabilityRequest $availabilityRequest
     */
    public function __construct(AvailabilityRequest $availabilityRequest)
    {
        $this->availabilityRequest = $availabilityRequest;
    }

    /**
     * @return AvailabilityRequest
     */
    public function getAvailabilityRequest(): AvailabilityRequest
    {
        return $this->availabilityRequest;
    }
}
