<?php

namespace App\Listeners;

use App\Events\AvailabilityRequestRegistered;
use App\Mail\AvailabilityRequestAutoResponseEmail;
use Illuminate\Mail\Mailer;

class SendAvailabilityRequestAutoResponseEmail
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  AvailabilityRequestRegistered $event
     * @return void
     */
    public function handle(AvailabilityRequestRegistered $event)
    {
        $mail = new AvailabilityRequestAutoResponseEmail($event->getAvailabilityRequest());
        $to = $event->getAvailabilityRequest()->getEmail();

        $this->mailer->to($to)
            ->send($mail);
    }
}
