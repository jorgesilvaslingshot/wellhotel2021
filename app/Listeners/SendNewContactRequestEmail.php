<?php

namespace App\Listeners;

use App\Events\ContactRequestRegistered;
use App\Mail\NewContactRequestEmail;
use Illuminate\Mail\Mailer;

class SendNewContactRequestEmail
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  ContactRequestRegistered $event
     * @return void
     */
    public function handle(ContactRequestRegistered $event)
    {
        $mail = new NewContactRequestEmail($event->getContactRequest());
        $to = explode(',', setting('contact-request-emails'));

        $this->mailer->to($to)
            ->send($mail);
    }
}
