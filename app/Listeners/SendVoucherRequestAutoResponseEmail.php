<?php

namespace App\Listeners;

use App\Events\VoucherRequestRegistered;
use App\Mail\VoucherRequestAutoResponseEmail;
use Illuminate\Mail\Mailer;

class SendVoucherRequestAutoResponseEmail
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  VoucherRequestRegistered $event
     * @return void
     */
    public function handle(VoucherRequestRegistered $event)
    {
        $mail = new VoucherRequestAutoResponseEmail($event->getVoucherRequest());
        $to = $event->getVoucherRequest()->getEmail();

        $this->mailer->to($to)
            ->send($mail);
    }
}
