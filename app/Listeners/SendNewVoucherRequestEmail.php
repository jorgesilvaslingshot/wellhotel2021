<?php

namespace App\Listeners;

use App\Events\VoucherRequestRegistered;
use App\Mail\NewVoucherRequestEmail;
use Illuminate\Mail\Mailer;

class SendNewVoucherRequestEmail
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  AvailabilityRequestRegistered $event
     * @return void
     */
    public function handle(VoucherRequestRegistered $event)
    {
        $mail = new NewVoucherRequestEmail($event->getVoucherRequest());
        $to = explode(',', setting('availability-request-emails'));

        $this->mailer->to(/*$to*/'jorge.silva@slingshot.pt')
            ->send($mail);
    }
}
