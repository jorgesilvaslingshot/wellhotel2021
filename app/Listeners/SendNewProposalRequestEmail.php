<?php

namespace App\Listeners;

use App\Events\ProposalRequestRegistered;
use App\Mail\NewProposalRequestEmail;
use Illuminate\Mail\Mailer;

class SendNewProposalRequestEmail
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  AvailabilityRequestRegistered $event
     * @return void
     */
    public function handle(ProposalRequestRegistered $event)
    {
        $mail = new NewProposalRequestEmail($event->getProposalRequest());
        $to = explode(',', setting('availability-request-emails'));

        $this->mailer->to(/*$to*/'jorge.silva@slingshot.pt')
            ->send($mail);
    }
}
