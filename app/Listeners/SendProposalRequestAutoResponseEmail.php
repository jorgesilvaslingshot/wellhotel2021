<?php

namespace App\Listeners;

use App\Events\ProposalRequestRegistered;
use App\Mail\ProposalRequestAutoResponseEmail;
use Illuminate\Mail\Mailer;

class SendProposalRequestAutoResponseEmail
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  ProposalRequestRegistered $event
     * @return void
     */
    public function handle(ProposalRequestRegistered $event)
    {
        $mail = new ProposalRequestAutoResponseEmail($event->getProposalRequest());
        $to = $event->getProposalRequest()->getEmail();

        $this->mailer->to($to)
            ->send($mail);
    }
}
