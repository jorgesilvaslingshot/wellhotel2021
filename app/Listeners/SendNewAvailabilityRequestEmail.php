<?php

namespace App\Listeners;

use App\Events\AvailabilityRequestRegistered;
use App\Mail\NewAvailabilityRequestEmail;
use Illuminate\Mail\Mailer;

class SendNewAvailabilityRequestEmail
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  AvailabilityRequestRegistered $event
     * @return void
     */
    public function handle(AvailabilityRequestRegistered $event)
    {
        $mail = new NewAvailabilityRequestEmail($event->getAvailabilityRequest());
        $to = explode(',', setting('availability-request-emails'));

        $this->mailer->to(/*$to*/'admin@test.com')
            ->send($mail);
    }
}
