<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoucherRequest extends Model
{
    public $fillable = [
        'locale', 'name', 'email', 'phone', 'adults', 'children', 'description', 'voucher_type'
    ];

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getAdults()
    {
        return $this->adults;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getVoucherType()
    {
        return $this->voucher_type;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function registerVoucherRequest($data, $locale)
    {

        return static::create(array_merge($data, compact('locale')));
    }
}
