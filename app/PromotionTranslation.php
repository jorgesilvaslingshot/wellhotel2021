<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionTranslation extends Model
{
    protected $fillable = ['title', 'label'];
    public $timestamps = false;
}
