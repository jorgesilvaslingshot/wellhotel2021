<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MastheadTranslation extends Model
{
    protected $fillable = ['title', 'description'];
    public $timestamps = false;
}
