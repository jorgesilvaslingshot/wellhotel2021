<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class EventType extends Model
{
    use Translatable;

    public $translatedAttributes = ['slug','name'];
    protected $with = ['translations'];
    protected $fillable = ['priority', 'publish'];

    public function events() : HasMany
    {
        return $this->hasMany(Event::class);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEvents()
    {
        return $this->events;
    }

    public function getPublishedEventsType($limit = null)
    {
        return static::where('publish', 1)
            ->orderBy('priority', 'desc')
            ->limit($limit)
            ->get();
    }

    public function getPublishedEventTypesWithEvents()
    {
        return static::where('publish', 1)
            ->orderBy('priority', 'desc')
            ->orderBy('id', 'asc')
            ->with(['events' => function ($query) {
                $query->where('publish', 1)
                    ->orderBy('priority', 'desc')
                    ->orderBy('id', 'desc');
            }])
            ->has('events')
            ->get();
    }

    public function saveOrCreate($data, $id = null)
    {
        $el = static::updateOrCreate(compact('id'), $data);

        foreach ($data['locales'] as $locale => $values) {
            $translation = $el->translateOrNew($locale);
            $translation->name = $values['name'];
        }

        $el->save();
    }

    public function setPriorityAttribute($value)
    {
        $this->attributes['priority'] = $value ?: 0;
    }
}
