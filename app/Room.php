<?php

namespace App;

use Carbon\Carbon;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Room extends Model
{
    use Translatable;
    public $translatedAttributes = ['slug', 'name', 'label', 'title', 'description', 'excerpt', 'additional_title', 'additional_description', 'room_size', 'max_guests', 'bed_size', 'other_amenities', 'landscape'];

    public $fillable = [
        'price',
        'max_guests_numeric',
        'masthead_image', 'cover_image', 'cover_image_2',
        'priority', 'publish'
    ];
    protected $with = ['translations', 'amenities', 'type'];

    public function amenities(): BelongsToMany
    {
        return $this->belongsToMany(Amenity::class, 'room_amenities', 'room_id', 'amenity_id');
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(RoomType::class, 'room_type_id');
    }

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    public function price_intervals()
    {
        return $this->hasMany(RoomPriceInterval::class);
    }

    public function prices()
    {
        return $this->hasManyThrough(RoomPrice::class, RoomPriceInterval::class, 'room_id', 'room_price_interval_id');
    }

    public function prices_now()
    {
        return $this->prices()->where('room_prices.date', '=', Carbon::now()->format('Y-m-d'));
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getExcerpt()
    {
        if (empty($this->excerpt)) {
            return str_limit(strip_tags($this->description), 300);
        }

        return $this->excerpt;
    }

    public function getAdditionalTitle()
    {
        return $this->additional_title;
    }

    public function getAdditionalDescription()
    {
        return $this->additional_description;
    }

    public function getMaxGuests()
    {
        return $this->max_guests;
    }

    public function getLandscape()
    {
        return $this->landscape;
    }

    public function getRoomSize()
    {
        return $this->room_size;
    }

    public function getBedSize()
    {
        return $this->bed_size;
    }

    public function getOtherAmenities()
    {
        if (empty($this->other_amenities)) {
            return [];
        }

        return explode(PHP_EOL, $this->other_amenities);
    }

    public function getMastheadImage()
    {
        return $this->masthead_image;
    }

    public function getCoverImage()
    {
        return $this->cover_image;
    }

    public function getCoverImage2()
    {
        return $this->cover_image_2;
    }

    public function hasDiscount()
    {
        return $this->getDiscount() > 0;
    }

    public function getDiscount()
    {
        //dd(setting('room_discount', 0));
        return setting('room_discount', 0);
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPriceNowWithDiscount()
    {
        if ($this->hasDiscount()) {
            return round($this->getPriceNow() - $this->getPriceNow() * ($this->getDiscount() / 100));
        }

        return $this->getPriceNow();
    }

    public function getPriceNow()
    {
        if ($this->prices_now->count() > 0) {
            return $this->prices_now->first()->getPrice();
        }

        return $this->price;
    }

    public function getMaxGuestsNumeric()
    {
        return $this->max_guests_numeric;
    }

    public function hasGallery()
    {
        return !empty($this->gallery);
    }

    public function getGallery()
    {
        return $this->gallery;
    }

    public function hasAmenities()
    {
        return $this->amenities->count() > 0;
    }

    public function getAmenities()
    {
        return $this->amenities;
    }

    public function getPriceIntervals()
    {
        return $this->price_intervals;
    }

    public function getOrderedPriceIntervals()
    {
        return $this->getPriceIntervals()->sortBy(function ($priceInterval) {
            return $priceInterval->getStartDate();
        });
    }

    public function saveOrCreate($data, $id)
    {
        $el = static::find($id) ?: new static;
        $el->price = $data['price'];
        $el->max_guests_numeric = $data['max_guests_numeric'];
        $el->gallery_id = $data['gallery_id'];
        $el->masthead_image = $data['masthead_image'];
        $el->cover_image = $data['cover_image'];
        $el->cover_image_2 = $data['cover_image_2'];
        $el->room_type_id = $data['room_type_id'];
        $el->priority = $data['priority'] ?? 0;
        $el->publish = $data['publish'];


        foreach ($data['languages'] as $locale => $values) {
            if ($values['name']) {
                $translation = $el->translateOrNew($locale);
                $translation->name = $values['name'];
                $translation->label = $values['label'];
                $translation->title = $values['title'];
                $translation->description = $values['description'];
                $translation->excerpt = $values['excerpt'];
                $translation->additional_title = $values['additional_title'];
                $translation->additional_description = $values['additional_description'];
                $translation->room_size = $values['room_size'];
                $translation->bed_size = $values['bed_size'];
                $translation->max_guests = $values['max_guests'];
                $translation->other_amenities = $values['other_amenities'];
            }
        }

        $el->save();
        $el->amenities()->sync($data['amenities'] ?? null);
    }

    public static function getPublishedRoomBySlugAndLocale($slug, $locale)
    {
        return static::where('publish', 1)
            ->whereTranslation('slug', $slug)
            ->whereTranslation('locale', $locale)
            ->first();
    }

    /**
     * @param null $limit
     * @return Collection
     *
     */
    public function getPublishedRooms($limit = null)
    {
        return static::where('publish', 1)
            ->orderBy('priority', 'desc')
            ->orderBy('id', 'asc')
            ->limit($limit)
            ->get();
    }

    public function getPublishedRoomsByRoomType($room_type_id)
    {
        return static::where('room_type_id', $room_type_id)
            ->where('publish', 1)
            ->orderBy('priority', 'desc')
            ->get();
    }

    public function getRoomPriceByIdAndDates($id, $from, $to)
    {
        $query = \DB::table('rooms AS n')
            ->selectRaw('n.id,coalesce(sum(s.price), 0) + n.price * (datediff(?, ?) - count(s.price) + 1) as price', [$to, $from])
            ->leftJoin('room_price_intervals AS i', 'i.room_id', '=', 'n.id')
            ->leftJoin('room_prices AS s', function ($join) use ($from, $to) {
                $join->on('i.id', '=', 's.room_price_interval_id')
                    ->whereRaw('s.date between ? and ?', [$from, $to]);
            })
            ->groupBy(['n.id','n.price'])
            ->where('n.id', '=', $id)
            ->first();

        $current_price = data_get($query, 'price', null);

        if (is_null($current_price)) {
            return null;
        }

        //dd($current_price);

        return round($current_price - ($current_price * ($this->getDiscount() / 100)));
    }
}
