<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class PackTypeTranslation extends Model
{
    use Sluggable;

    protected $fillable = ['name'];
    public $timestamps = false;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
            ]
        ];
    }

    public function scopeWithUniqueSlugConstraints(Builder $query, Model $model)
    {
        $locale = $model->locale;
        return $query->where('locale', $locale);
    }
}
