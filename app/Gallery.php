<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Gallery extends Model
{
    use Translatable;
    public $translatedAttributes = ['name', 'description'];
    protected $with = ['translations', 'items'];

    public function items(): HasMany
    {
        return $this->hasMany(GalleryItem::class, 'gallery_id')->orderBy('priority', 'desc')->orderBy('id', 'asc');
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function getItemsToModal()
    {
        return $this->items->map(function ($item) {
            return [
                'title' => $item->getLabel(),
                'href' => $item->getImage(),
            ];
        });
    }

    public function getFirstImage()
    {
        return optional($this->items->get(0))->getImage();
    }

    public function getSecondImage()
    {
        return optional($this->items->get(1))->getImage();
    }

    public function saveOrCreate($data, $id)
    {
        $el = static::find($id) ?: new static;

        foreach ($data['languages'] as $locale => $values) {
            if ($values['name']) {
                $translation = $el->translateOrNew($locale);
                $translation->name = $values['name'];
                $translation->description = $values['description'];
            }
        }

        $el->save();
    }
}
