<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class PackType extends Model
{
    use Translatable;

    public $translatedAttributes = ['slug', 'name'];
    protected $with = ['translations'];
    protected $fillable = ['priority', 'publish'];

    public function packs()
    {
        return $this->hasMany(Pack::class);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPacks()
    {
        return $this->packs;
    }

    public function getPublishPackTypesWithPacks()
    {
        return static::where('publish', 1)
            ->orderBy('priority', 'desc')
            ->orderBy('id', 'desc')
            ->with(['packs' => function ($query) {
                $query->where('publish', 1)
                    ->orderBy('priority', 'desc')
                    ->orderBy('id', 'desc');
            }])
            ->has('packs')
            ->get();
    }

    public function saveOrCreate($data, $id = null)
    {
        $el = static::updateOrCreate(compact('id'), $data);

        foreach ($data['locales'] as $locale => $values) {
            $translation = $el->translateOrNew($locale);
            $translation->name = $values['name'];
        }

        $el->save();
    }

    public function setPriorityAttribute($value)
    {
        $this->attributes['priority'] = $value ?: 0;
    }
}
