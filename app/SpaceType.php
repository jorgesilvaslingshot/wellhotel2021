<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class SpaceType extends Model
{
    use Translatable;

    public $translatedAttributes = ['slug','name'];
    protected $with = ['translations'];
    protected $fillable = ['priority', 'publish'];

    public function spaces() : HasMany
    {
        return $this->hasMany(Space::class);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSpaces()
    {
        return $this->spaces;
    }

    public function getPublishedSpaceTypesWithSpaces()
    {
        return static::where('publish', 1)
            ->orderBy('priority', 'desc')
            ->orderBy('id', 'asc')
            ->with(['spaces' => function ($query) {
                $query->where('publish', 1)
                    ->orderBy('priority', 'desc')
                    ->orderBy('id', 'desc');
            }])
            ->has('spaces')
            ->get();
    }

    public function saveOrCreate($data, $id = null)
    {
        $el = static::updateOrCreate(compact('id'), $data);

        foreach ($data['locales'] as $locale => $values) {
            $translation = $el->translateOrNew($locale);
            $translation->name = $values['name'];
        }

        $el->save();
    }

    public function setPriorityAttribute($value)
    {
        $this->attributes['priority'] = $value ?: 0;
    }
}
