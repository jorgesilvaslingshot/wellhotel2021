<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    use Translatable;

    public $translatedAttributes = ['label', 'title', 'description'];
    public $fillable = ['slug', 'image', 'thumbnail', 'gallery_id'];
    protected $with = ['translations'];

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    public function hasGallery()
    {
        return !empty($this->gallery);
    }

    public function getGallery()
    {
        return $this->gallery;
    }

    public function getBlockBySlug($slug)
    {
        return static::where('slug', $slug)
            ->first();
    }

    public function saveOrCreate($data, $id = null)
    {
        $el = static::updateOrCreate(compact('id'), $data);

        foreach ($data['locales'] as $locale => $values) {
            $translation = $el->translateOrNew($locale);
            $translation->title = $values['title'];
            $translation->label = $values['label'];
            $translation->description = $values['description'];
        }

        $el->save();
    }
}
