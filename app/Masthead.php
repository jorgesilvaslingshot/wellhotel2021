<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Masthead extends Model
{
    use Translatable;

    public $translatedAttributes = ['title', 'description'];
    public $fillable = ['slug', 'image'];
    protected $with = ['translations'];

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getMastheadBySlug($slug)
    {
        return static::where('slug', $slug)
            ->first();
    }

    public function saveOrCreate($data, $id = null)
    {
        $el = static::updateOrCreate(compact('id'), $data);

        foreach ($data['locales'] as $locale => $values) {
            $translation = $el->translateOrNew($locale);
            $translation->title = $values['title'];
            $translation->description = $values['description'];
        }

        $el->save();
    }
}
