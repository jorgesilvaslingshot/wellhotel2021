<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class RoomType extends Model
{
    use Translatable;
    public $translatedAttributes = ['slug', 'name'];
    protected $with = ['translations'];
    protected $fillable = ['priority', 'publish'];

    public function rooms(): HasMany
    {
        return $this->hasMany(Room::class, 'room_type_id');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getRooms()
    {
        return $this->rooms;
    }

    public function getPublishedRoomTypesWithRooms()
    {
        return static::where('publish', 1)
            ->orderBy('priority', 'desc')
            //->orderBy('id', 'asc')
            ->with(['rooms' => function ($query) {
                $query->where('publish', 1)
                    ->orderBy('priority', 'desc');
            }])
            ->has('rooms')
            ->orderBy('priority', 'desc')
            ->get();
    }

    public function setPriorityAttribute($value)
    {
        $this->attributes['priority'] = $value ?: 0;
    }

    /**
     * @param $locale
     * @return RoomType
     */
    public static function getFirstPublishedRoomTypeByLocale($locale)
    {
        return static::where('publish', 1)
            ->whereTranslation('locale', $locale)
            ->first();
    }

    public static function getPublishedRoomTypeBySlugAndLocale($slug, $locale)
    {
        return static::where('publish', 1)
            ->whereTranslation('slug', $slug)
            ->whereTranslation('locale', $locale)
            ->first();
    }
}
