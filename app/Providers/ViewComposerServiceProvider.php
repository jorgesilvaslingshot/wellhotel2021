<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\ViewComposers;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['view']->composers([
            ViewComposers\LocalizationViewComposer::class => 'layouts.*',
            ViewComposers\SeoViewComposer::class => 'layouts.*',
            ViewComposers\AnalyticsViewComposer::class => 'partials.analytics',
            ViewComposers\PromotionViewComposer::class => 'partials.promotion',
            ViewComposers\BottomSliderViewComposer::class => 'partials.bottom-slider',
            ViewComposers\MessageViewComposer::class => ['partials.footer', 'partials.comment-slider'],
        ]);

        $this->app['view']->share('phrases', frases_para_idioma_actual());
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
