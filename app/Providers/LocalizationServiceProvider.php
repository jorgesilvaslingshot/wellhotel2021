<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class LocalizationServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app['config']->get('laravellocalization.enable')) {
            $this->app->register('Mcamara\LaravelLocalization\LaravelLocalizationServiceProvider');
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }
}
