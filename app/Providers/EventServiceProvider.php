<?php

namespace App\Providers;

use App\Events\AvailabilityRequestRegistered;
use App\Events\ProposalRequestRegistered;
use App\Events\VoucherRequestRegistered;
use App\Events\ContactRequestRegistered;
use App\Listeners\SendAvailabilityRequestAutoResponseEmail;
use App\Listeners\SendProposalRequestAutoResponseEmail;
use App\Listeners\SendNewAvailabilityRequestEmail;
use App\Listeners\SendNewProposalRequestEmail;
use App\Listeners\SendNewContactRequestEmail;
use App\Listeners\SendNewVoucherRequestEmail;
use App\Listeners\SendVoucherRequestAutoResponseEmail;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ContactRequestRegistered::class => [
            SendNewContactRequestEmail::class,
        ],
        AvailabilityRequestRegistered::class => [
            SendNewAvailabilityRequestEmail::class,
            SendAvailabilityRequestAutoResponseEmail::class,
        ],

        ProposalRequestRegistered::class => [
            SendNewProposalRequestEmail::class,
            SendProposalRequestAutoResponseEmail::class,
        ],

        VoucherRequestRegistered::class => [
            SendNewVoucherRequestEmail::class,
            SendVoucherRequestAutoResponseEmail::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
