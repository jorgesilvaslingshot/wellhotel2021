<?php

namespace App\Providers;

use App\Admin\MyForm;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->extend('form', function ($service, $app) {
            $form = new MyForm($app['html'], $app['url'], $app['view'], $app['session.store']->token());
            $form->setSessionStore($app['session.store']);
            return $form;
        });
    }
}
