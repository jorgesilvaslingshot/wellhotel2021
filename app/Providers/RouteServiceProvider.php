<?php

namespace App\Providers;

use App\Experience;
use App\Pack;
use App\Room;
use App\RoomType;
use App\Space;
use App\Traits\Localization;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    use Localization;

    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::bind('space', function ($value) {
            return Space::getPublishedSpaceBySlugAndLocale($value, $this->getLocale()) ?? abort(404);
        });

        Route::bind('room_type', function ($value) {
            return RoomType::getPublishedRoomTypeBySlugAndLocale($value, $this->getLocale()) ?? abort(404);
        });

        Route::bind('room', function ($value) {
            return Room::getPublishedRoomBySlugAndLocale($value, $this->getLocale()) ?? abort(404);
        });

        Route::bind('pack', function ($value) {
            return Pack::getPublishedPackBySlugAndLocale($value, $this->getLocale()) ?? abort(404);
        });

        Route::bind('experience', function ($value) {
            return Experience::getPublishedExperienceBySlugAndLocale($value, $this->getLocale()) ?? abort(404);
        });

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
