<?php

namespace App\Mail;

use App\VoucherRequest;
use App\Traits\Localization;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VoucherRequestAutoResponseEmail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var VoucherRequest
     */
    private $voucherRequest;

    /**
     * Create a new message instance.
     *
     * @param voucherRequest $voucherRequest
     */
    public function __construct(VoucherRequest $voucherRequest)
    {
        $this->voucherRequest = $voucherRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown("emails.{$this->voucherRequest->getLocale()}.voucher-request-auto-response")
            ->subject(trans('emails.subjects.voucher-request-auto-response', [], $this->voucherRequest->getLocale()))
            ->with('voucherRequest', $this->voucherRequest);
    }
}
