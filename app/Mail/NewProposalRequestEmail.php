<?php

namespace App\Mail;

use App\ProposalRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewProposalRequestEmail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var ProposalRequest
     */
    private $proposalRequest;

    /**
     * Create a new message instance.
     *
     * @param ProposalRequest $proposalRequest
     */
    public function __construct(ProposalRequest $proposalRequest)
    {
        $this->proposalRequest = $proposalRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.new-proposal-request')
            ->subject('Pedido de Proposta')
            ->replyTo($this->proposalRequest->getEmail(), $this->proposalRequest->getName())
            ->with('proposalRequest', $this->proposalRequest);
    }
}
