<?php

namespace App\Mail;

use App\AvailabilityRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewAvailabilityRequestEmail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var AvailabilityRequest
     */
    private $availabilityRequest;

    /**
     * Create a new message instance.
     *
     * @param AvailabilityRequest $availabilityRequest
     */
    public function __construct(AvailabilityRequest $availabilityRequest)
    {
        $this->availabilityRequest = $availabilityRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.new-availability-request')
            ->subject('Pedido de Disponibilidade')
            ->replyTo($this->availabilityRequest->getEmail(), $this->availabilityRequest->getName())
            ->with('availabilityRequest', $this->availabilityRequest);
    }
}
