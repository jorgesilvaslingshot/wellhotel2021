<?php

namespace App\Mail;

use App\AvailabilityRequest;
use App\Traits\Localization;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AvailabilityRequestAutoResponseEmail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var AvailabilityRequest
     */
    private $availabilityRequest;

    /**
     * Create a new message instance.
     *
     * @param AvailabilityRequest $availabilityRequest
     */
    public function __construct(AvailabilityRequest $availabilityRequest)
    {
        $this->availabilityRequest = $availabilityRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown("emails.{$this->availabilityRequest->getLocale()}.availability-request-auto-response")
            ->subject(trans('emails.subjects.availability-request-auto-response', [], $this->availabilityRequest->getLocale()))
            ->with('availabilityRequest', $this->availabilityRequest);
    }
}
