<?php

namespace App\Mail;

use App\ProposalRequest;
use App\Traits\Localization;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProposalRequestAutoResponseEmail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var ProposalRequest
     */
    private $proposalRequest;

    /**
     * Create a new message instance.
     *
     * @param ProposalRequest $proposalRequest
     */
    public function __construct(ProposalRequest $proposalRequest)
    {
        $this->proposalRequest = $proposalRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown("emails.{$this->proposalRequest->getLocale()}.proposal-request-auto-response")
            ->subject(trans('emails.subjects.proposal-request-auto-response', [], $this->proposalRequest->getLocale()))
            ->with('proposalRequest', $this->proposalRequest);
    }
}
