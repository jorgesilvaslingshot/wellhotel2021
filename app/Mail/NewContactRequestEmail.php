<?php

namespace App\Mail;

use App\ContactRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewContactRequestEmail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var ContactRequest
     */
    private $contactRequest;

    /**
     * Create a new message instance.
     *
     * @param ContactRequest $contactRequest
     */
    public function __construct(ContactRequest $contactRequest)
    {
        $this->contactRequest = $contactRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.new-contact-request')
            ->subject('Pedido de Contacto')
            ->replyTo($this->contactRequest->getEmail(), $this->contactRequest->getName())
            ->with('contactRequest', $this->contactRequest);
    }
}
