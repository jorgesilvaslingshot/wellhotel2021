<?php

namespace App\Mail;

use App\VoucherRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewVoucherRequestEmail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var VoucherRequest
     */
    private $voucherRequest;

    /**
     * Create a new message instance.
     *
     * @param VoucherRequest $voucherRequest
     */
    public function __construct(VoucherRequest $voucherRequest)
    {
        $this->voucherRequest = $voucherRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.new-voucher-request')
            ->subject('Pedido de Informação de Voucher')
            ->replyTo($this->voucherRequest->getEmail(), $this->voucherRequest->getName())
            ->with('voucherRequest', $this->voucherRequest);
    }
}
