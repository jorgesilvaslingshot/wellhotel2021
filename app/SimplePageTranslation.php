<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SimplePageTranslation extends Model
{
    protected $fillable = ['title', 'content'];
    public $timestamps = false;
}
