<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Event extends Model
{
    use Translatable;

    public $translatedAttributes = ['slug' ,'name', 'subtitle', 'description', 'title_right'];
    protected $fillable = ['image', 'thumbnail', 'priority', 'publish', 'event_type_id', 'gallery_id'];
    protected $with = ['translations'];

    public function type() : BelongsTo
    {
        return $this->belongsTo(EventType::class, 'event_type_id');
    }

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSubtitle()
    {
        return $this->subtitle;
    }

    public function getTitleRight()
    {
        return $this->title_right;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getThumbnail()
    {
        if (!empty($this->thumbnail)) {
            return $this->thumbnail;
        }

        return $this->image;
    }

    public function getType()
    {
        return $this->type;
    }

    public function hasGallery()
    {
        return !empty($this->gallery);
    }

    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * @param null $limit
     * @return Collection
     *
     */
    public function getPublishedEvent($limit = null)
    {
        return static::where('publish', 1)
            ->orderBy('priority', 'desc')
            ->limit($limit)
            ->first();
    }

    public static function getPublishedEventBySlugAndLocale($slug, $locale)
    {
        return static::where('publish', 1)
            ->whereTranslation('slug', $slug)
            ->whereTranslation('locale', $locale)
            ->first();
    }

    public function setPriorityAttribute($value)
    {
        $this->attributes['priority'] = $value ?: 0;
    }

    public function saveOrCreate($data, $id)
    {
        $el = static::updateOrCreate(compact('id'), $data);

        foreach ($data['languages'] as $locale => $values) {
            if ($values['name']) {
                $translation = $el->translateOrNew($locale);
                $translation->name = $values['name'];
                $translation->description = $values['description'];
            }
        }

        $el->save();
    }
}
