<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryItemTranslation extends Model
{
    protected $fillable = ['label'];
}
