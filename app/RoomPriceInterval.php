<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RoomPriceInterval extends Model
{
    protected $fillable = ['name', 'room_id'];

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function room_prices()
    {
        return $this->hasMany(RoomPrice::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function (RoomPriceInterval $roomPriceInterval) {
            $roomPriceInterval->room_prices()->delete();
        });

        static::saving(function (RoomPriceInterval $roomPriceInterval) {
            $roomPriceInterval->room_prices()->delete();
        });
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getStartDate()
    {
        return $this->room_prices->min('date');
    }

    public function getEndDate()
    {
        return $this->room_prices->max('date');
    }

    public function getPrice()
    {
        return optional($this->room_prices->first())->getPrice();
    }

    public function getRoom()
    {
        return $this->room;
    }

    public function saveOrCreate($data, $id)
    {
        $instance = static::updateOrCreate(compact('id'), $data);

        $price = array_get($data, 'price');
        $priority = array_get($data, 'priority');

        foreach (array_get($data, 'dates', []) as $date) {
            $instance->room_prices()->create(compact('date', 'price', 'priority'));
        }

        return $instance;
    }
}
