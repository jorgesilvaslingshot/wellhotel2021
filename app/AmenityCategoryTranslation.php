<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class AmenityCategoryTranslation extends Model
{
    use Sluggable;

    protected $fillable = ['name', 'amenities_list'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => ['source' => 'name']
        ];
    }

    public function scopeWithUniqueSlugConstraints(Builder $query, Model $model)
    {
        $locale = $model->locale;
        return $query->where('locale', $locale);
    }
}
