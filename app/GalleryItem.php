<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property string image
 * @property int gallery_id
 * @property int priority
 */
class GalleryItem extends Model
{
    use Translatable;
    public $translatedAttributes = ['label'];
    protected $fillable = ['priority', 'image'];

    public function gallery(): BelongsTo
    {
        return $this->belongsTo(Gallery::class, 'gallery_id');
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function saveOrCreate($data, $id)
    {
        $el = static::find($id) ?: new static;
        $el->image = $data['image'];
        $el->gallery_id = $data['gallery_id'];
        $el->priority = $data['priority'];

        foreach ($data['languages'] as $locale => $values) {
            if ($values['label']) {
                $translation = $el->translateOrNew($locale);
                $translation->label = $values['label'];
            }
        }

        $el->save();
    }

    public function setPriorityAttribute($value)
    {
        $this->attributes['priority'] = $value ?: 0;
    }
}
