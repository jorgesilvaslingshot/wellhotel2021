<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public function allAsArrayList()
    {
        return $this->pluck('slug', 'slug')->toArray();
    }
}
