<?php

namespace App\Console\Tasks;

use Illuminate\Contracts\Cache\Repository as Cache;

class MonitorQueueListener
{
    /**
     * @var Cache
     */
    private $cache;

    /**
     * @param Cache $cache
     */
    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    public function handle()
    {
        if ($this->shouldRun()) {
            $this->cache->forever('queue.pid', $this->run());
        }
    }

    /**
     * @return bool
     */
    protected function shouldRun()
    {
        $pid = $this->cache->get('queue.pid');

        return !is_null($pid) ? exec("ps -p $pid --no-heading | awk '{print $1}'") == '' : true;
    }

    /**
     * @return string
     */
    protected function run()
    {
        $artisan = base_path('artisan');

        return exec("php -d register_argc_argv=On $artisan queue:listen --tries=3 > /dev/null & echo $!");
    }
}
