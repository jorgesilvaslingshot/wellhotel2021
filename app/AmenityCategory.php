<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class AmenityCategory extends Model
{
    use Translatable;
    public $translatedAttributes = ['slug', 'name', 'amenities_list'];
    protected $with = ['translations'];
    protected $fillable = ['priority', 'publish'];

    public function getId()
    {
        return $this->id;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAmenitiesList()
    {
        if (empty($this->amenities_list)) {
            return [];
        }

        return explode(PHP_EOL, $this->amenities_list);
    }

    public function getPublishAmenityCategoriesWithAmenities()
    {
        return static::where('publish', 1)
            ->orderBy('priority', 'desc')
            ->orderBy('id', 'asc')
            ->get();
    }

    public function saveOrCreate($data, $id = null)
    {
        $el = static::updateOrCreate(compact('id'), $data);

        foreach ($data['languages'] as $locale => $values) {
            if ($values['name']) {
                $translation = $el->translateOrNew($locale);
                $translation->name = $values['name'];
                $translation->amenities_list = $values['amenities_list'];
            }
        }

        $el->save();
    }

    public function setPriorityAttribute($value)
    {
        $this->attributes['priority'] = $value ?: 0;
    }
}
