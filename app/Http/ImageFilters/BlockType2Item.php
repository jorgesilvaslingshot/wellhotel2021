<?php

namespace App\Http\ImageFilters;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class BlockType2Item implements FilterInterface
{
    /**
     * Applies filter to given image
     *
     * @param  Image $image
     * @return Image
     */
    public function applyFilter(Image $image)
    {
        /*return $image->resize(800, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->encode(null, 80);*/

        return $image->fit(620, 620)->encode(null, 80);
    }
}
