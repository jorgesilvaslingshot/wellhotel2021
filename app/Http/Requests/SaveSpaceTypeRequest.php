<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveSpaceTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'locales.pt.name' => 'required',
            'locales.en.name' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'locales.pt.name' => 'Nome português',
            'locales.en.name' => 'Nome inglês',
        ];
    }
}
