<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveRoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'languages.pt.name' => ['required'],
            'languages.pt.description' => ['required'],
            'masthead_image' => 'required',
            'room_type_id' => ['required'],
            'max_guests_numeric' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'languages.pt.name' => 'Nome (pt)',
            'languages.pt.description' => 'Descrição (pt)',
            'languages.en.name' => 'Nome (en)',
            'languages.en.description' => 'Descrição (en)',
            'masthead_image' => 'imagem (topo)',
            'room_type_id' => 'Tipo de quarto',
            'max_guests_numeric' => 'Ocupação máxima',
        ];
    }
}
