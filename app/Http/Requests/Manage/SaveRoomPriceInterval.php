<?php

namespace App\Http\Requests\Manage;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class SaveRoomPriceInterval extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
            'price' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nome',
            'date_start' => 'data início',
            'date_end' => 'data fim',
            'price' => 'preço',
        ];
    }

//    public function withValidator(Validator $validator)
//    {
//        $this->setDates();
//
//        $validator->after(function ($validator) {
//            $existed = DB::table('room_prices')
//                ->join('room_price_intervals', 'room_prices.room_price_interval_id', '=', 'room_price_intervals.id')
//                ->where('room_prices.room_price_interval_id', '!=', $this->id)
//                ->where('room_price_intervals.room_id', '=', $this->room_id)
//                ->get()
//                ->pluck('date');
//
//            if ($existed->intersect($this->dates)->count() > 0) {
//                //dd('O intervalo de datas colide com um intervalo já existente.');
//                $validator->errors()->add('date', 'O intervalo de datas colide com um intervalo já existente.');
//            }
//        });
//    }

    public function setDates()
    {
        $dates = [];

        if (!empty($this->date_start) && !empty($this->date_end)) {
            $from = Carbon::parse($this->date_start);
            $to = Carbon::parse($this->date_end);
            $dates = $this->generateDateRange($from, $to);
        }

        $this->merge(compact('dates'));
    }

    protected function generateDateRange(Carbon $start_date, Carbon $end_date)
    {
        $dates = [];

        for ($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }

        return $dates;
    }


}
