<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SavePhraseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'locales.pt.content' => ['required'],
        ];
    }

    public function attributes()
    {
        return [
            'locales.pt.content' => 'Conteúdo português',
            'locales.en.content' => 'Conteúdo inglês',
            'locales.es.content' => 'Conteúdo espanhol',
        ];
    }
}
