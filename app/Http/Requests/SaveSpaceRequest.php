<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveSpaceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'languages.pt.name' => ['required'],
            'space_type_id' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'languages.pt.name' => 'Nome (pt)',
            'languages.en.name' => 'Nome (en)',
            'space_type_id' => 'Tipo espaço',
        ];
    }
}
