<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveExperienceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'languages.pt.name' => ['required'],
            'languages.pt.description' => ['required'],
            //'experience_type_id' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'languages.pt.name' => 'Nome (pt)',
            'languages.pt.description' => 'Descrição (pt)',
            'languages.pt.includes' => 'Inclui (pt)',
            'languages.en.name' => 'Nome (en)',
            'languages.en.description' => 'Descrição (en)',
            //'experience_type_id' => 'Tipo experiência',
        ];
    }
}
