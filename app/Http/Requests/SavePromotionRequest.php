<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SavePromotionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'locales.pt.title' => 'required',
            'locales.en.title' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'image' => 'required',
            'publish' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'locales.pt.title' => 'Título português',
            'locales.en.title' => 'Título inglês',
            'start_date' => 'Data início',
            'end_date' => 'Data fim',
            'image' => 'Imagem',
            'publish' => 'Publicado',
        ];
    }
}
