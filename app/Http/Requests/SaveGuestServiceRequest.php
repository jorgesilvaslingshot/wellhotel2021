<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveGuestServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'languages.pt.title' => 'required',
            'languages.en.title' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'languages.pt.title' => 'Título português',
            'languages.en.title' => 'Título inglês',
        ];
    }
}
