<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'author' => 'required',
            'message' => 'required',
            'date' => 'required',
            'locale' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'author' => 'autor',
            'message' => 'mensagem',
            'date' => 'data',
            'locale' => 'idioma',
        ];
    }
}
