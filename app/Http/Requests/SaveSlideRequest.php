<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveSlideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required',
            'locale' => 'required',
            'image' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'título',
            'locale' => 'idioma',
            'image' => 'imagem',
        ];
    }
}
