<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveGalleryItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //'languages.pt.label' => ['required'],
            //'priority' => ['integer'],
            'image' => ['required'],
        ];
    }

    public function attributes()
    {
        return [
            'languages.pt.label' => 'Nome (pt)',
            'languages.en.label' => 'Nome (en)',
            'priority' => 'Prioridade',
            'image' => 'Imagem',
        ];
    }
}
