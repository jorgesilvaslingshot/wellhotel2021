<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveSimplePageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'locales.pt.title' => 'required',
            'locales.en.title' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nome',
            'locales.pt.title' => 'Título português',
            'locales.en.title' => 'Título inglês',
        ];
    }
}
