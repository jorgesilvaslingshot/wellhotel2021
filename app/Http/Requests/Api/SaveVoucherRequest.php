<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class SaveVoucherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'voucher_type' => 'required',
            'description'=>'required',
            'adults' => 'required|integer',
            'children' => 'required|integer',
            'name' => 'required',
            'email' => 'required|email|max:191',
            'phone' => 'required|max:191',
        ];
    }

    // public function attributes()
    // {
    //     $type = $this->get('product_type', 'room');

    //     return [
    //         'product_name' => trans("validation.attributes.$type")
    //     ];
    // }
}
