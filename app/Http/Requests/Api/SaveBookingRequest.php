<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class SaveBookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_type' => 'required',
            'product_name' => 'required',
            'arrival' => 'required|date',
            'departure' => 'required|date',
            'adults' => 'required|integer',
            'children' => 'required|integer',
            'price' => '',
            'name' => 'required',
            'email' => 'required|email|max:191',
            'phone' => 'required|max:191',
        ];
    }

    public function attributes()
    {
        $type = $this->get('product_type', 'room');

        return [
            'product_name' => trans("validation.attributes.$type")
        ];
    }
}
