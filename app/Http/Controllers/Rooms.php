<?php


namespace App\Http\Controllers;

use App\Masthead;
use App\Room;
use App\RoomType;
use Artesaos\SEOTools\Traits\SEOTools;

class Rooms
{
    use SEOTools;

    /**
     * @var Masthead
     */
    private $mastheadRepo;
    /**
     * @var RoomType
     */
    private $roomTypeRepo;

    /**
     * Rooms constructor.
     * @param Masthead $mastheadRepo
     * @param RoomType $roomTypeRepo
     */
    public function __construct(Masthead $mastheadRepo, RoomType $roomTypeRepo)
    {
        $this->mastheadRepo = $mastheadRepo;
        $this->roomTypeRepo = $roomTypeRepo;
    }

    public function __invoke()
    {
        $masthead = $this->mastheadRepo->getMastheadBySlug('rooms-and-suites');
        $roomTypes = $this->roomTypeRepo->getPublishedRoomTypesWithRooms();

        $this->setSeo();

        return view('site.rooms.list', compact('masthead', 'roomTypes'));
    }

    protected function setSeo()
    {
        $this->seo()->setTitle(trans('site.rooms-and-suites'));
    }
}
