<?php

namespace App\Http\Controllers;

use App\Page;
use App\Traits\Localization;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageDetail
{
    use Localization;

    /**
     * @var Page
     */
    private $pageRepo;

    public function __construct(Page $pageRepo)
    {
        $this->pageRepo = $pageRepo;
    }

    /**
     * @param Request $request
     */
    public function __invoke(Request $request)
    {
        $page = $this->pageRepo->findPageByUrlAndLocale(str_replace($this->getLocale() . '/', '', $request->path()), $this->getLocale());

        if (is_null($page)) {
            throw new  NotFoundHttpException;
        }

        return view()->first([
            'site.pages.' . $page->getView(),
            'site.pages.detail'
        ], compact('page'));
    }

}
