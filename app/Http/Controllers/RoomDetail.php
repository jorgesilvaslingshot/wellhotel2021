<?php


namespace App\Http\Controllers;

use App\Room;
use Artesaos\SEOTools\Traits\SEOTools;

class RoomDetail
{
    use SEOTools;

    /**
     * @var Room
     */
    private $roomRepo;

    /**
     * RoomDetail constructor.
     * @param Room $roomRepo
     */
    public function __construct(Room $roomRepo)
    {
        $this->roomRepo = $roomRepo;
    }

    public function __invoke(Room $room)
    {
        $rooms = $this->roomRepo->getPublishedRooms();

        $index = $rooms->search(function ($item) use ($room) {
            return $item->id == $room->getId();
        });
        $roomNumber = $index + 1;
        $totalRooms = $rooms->count();

        $prevIndex = abs($index - 1 + $totalRooms) % $totalRooms;
        $nextIndex = abs($index + 1) % $totalRooms;

        $prevRoom = $rooms->get($prevIndex);
        $nextRoom = $rooms->get($nextIndex);

        $this->setSeo($room);

        return view('site.rooms.detail', compact('room', 'rooms', 'roomNumber', 'totalRooms', 'prevRoom', 'nextRoom'));
    }

    protected function setSeo(Room $room)
    {
        $this->seo()->setTitle($room->getName());
        $this->seo()->addImages($room->getMastheadImage());
    }
}
