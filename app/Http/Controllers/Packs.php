<?php

namespace App\Http\Controllers;

use App\Experience;
use App\Masthead;
use App\Pack;
use Artesaos\SEOTools\Traits\SEOTools;

class Packs
{
    use SEOTools;

    /**
     * @var Pack
     */
    private $packRepo;
    /**
     * @var Masthead
     */
    private $mastheadRepo;
    private $experienceRepo;

    /**
     * Packs constructor.
     * @param Pack $packRepo
     * @param Masthead $mastheadRepo
     */
    public function __construct(Experience $experienceRepo, Pack $packRepo, Masthead $mastheadRepo)
    {
        $this->packRepo = $packRepo;
        $this->experienceRepo = $experienceRepo;

        $this->mastheadRepo = $mastheadRepo;
    }

    public function __invoke()
    {
        $masthead = $this->mastheadRepo->getMastheadBySlug('packs');
        $packs = $this->packRepo->getPublishedPacks();

        $experiences = $this->experienceRepo->getPublishedExperiences();

        $this->setSeo();

        return view('site.packs.list', compact('masthead', 'packs', 'experiences'));
    }

    protected function setSeo()
    {
        $this->seo()->setTitle(trans('site.packs'));
    }
}
