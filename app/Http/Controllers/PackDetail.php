<?php

namespace App\Http\Controllers;

use App\Pack;
use Artesaos\SEOTools\Traits\SEOTools;

class PackDetail
{
    use SEOTools;

    /**
     * @var Pack
     */
    private $packRepo;

    /**
     * @param Pack $packRepo
     */
    public function __construct(Pack $packRepo)
    {
        $this->packRepo = $packRepo;
    }

    public function __invoke(Pack $pack)
    {
        $packs = $this->packRepo->getPublishedPacks();

        $index = $packs->search(function ($item) use ($pack) {
            return $item->id == $pack->getId();
        });
        $packNumber = $index + 1;
        $totalPacks = $packs->count();

        $prevIndex = abs($index - 1 + $totalPacks) % $totalPacks;
        $nextIndex = abs($index + 1) % $totalPacks;

        $prevPack = $packs->get($prevIndex);
        $nextPack = $packs->get($nextIndex);

        $this->setSeo($pack);

        return view('site.packs.detail', compact('pack', 'packs', 'packNumber', 'totalPacks', 'prevPack', 'nextPack'));
    }

    protected function setSeo(Pack $pack)
    {
        $this->seo()->setTitle($pack->getName());
        $this->seo()->addImages($pack->getImage());
    }
}
