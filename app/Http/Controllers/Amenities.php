<?php

namespace App\Http\Controllers;

use App\Amenity;
use App\AmenityCategory;
use App\Masthead;
use Artesaos\SEOTools\Traits\SEOTools;

class Amenities
{
    use SEOTools;

    /**
     * @var Amenity
     */
    private $amenityRepo;
    /**
     * @var Masthead
     */
    private $mastheadRepo;
    /**
     * @var AmenityCategory
     */
    private $amenityCategoryRepo;

    /**
     * Amenities constructor.
     * @param Amenity $amenityRepo
     * @param Masthead $mastheadRepo
     * @param AmenityCategory $amenityCategoryRepo
     */
    public function __construct(Amenity $amenityRepo, Masthead $mastheadRepo, AmenityCategory $amenityCategoryRepo)
    {
        $this->amenityRepo = $amenityRepo;
        $this->mastheadRepo = $mastheadRepo;
        $this->amenityCategoryRepo = $amenityCategoryRepo;
    }

    public function __invoke()
    {
        $masthead = $this->mastheadRepo->getMastheadBySlug('amenities');
        $amenityCategories = $this->amenityCategoryRepo->getPublishAmenityCategoriesWithAmenities();
        $mainAmenities = $this->amenityRepo->getPublishedMainAmenities(8);

        $this->setSeo();

        return view('site.amenities', compact('masthead', 'amenityCategories', 'mainAmenities'));
    }

    protected function setSeo()
    {
        $this->seo()->setTitle(trans('site.amenities'));
    }
}
