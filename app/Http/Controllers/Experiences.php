<?php

namespace App\Http\Controllers;

use App\Experience;
use App\Masthead;
use Artesaos\SEOTools\Traits\SEOTools;

class Experiences
{
    use SEOTools;

    /**
     * @var Experience
     */
    private $experienceRepo;
    /**
     * @var Masthead
     */
    private $mastheadRepo;

    /**
     * Packs constructor.
     * @param Experience $experienceTypeRepo
     * @param Masthead $mastheadRepo
     */
    public function __construct(Experience $experienceRepo, Masthead $mastheadRepo)
    {
        $this->experienceRepo = $experienceRepo;
        $this->mastheadRepo = $mastheadRepo;
    }

    public function __invoke()
    {
        $masthead = $this->mastheadRepo->getMastheadBySlug('experiences');
        $experiences = $this->experienceRepo->getPublishedExperiences();

        $this->setSeo();

        return view('site.experiences.list', compact('masthead', 'experiences'));
    }

    protected function setSeo()
    {
        $this->seo()->setTitle(trans('site.experiences'));
    }
}
