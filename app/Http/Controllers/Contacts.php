<?php

namespace App\Http\Controllers;

use App\SimplePage;
use Artesaos\SEOTools\Traits\SEOTools;

class Contacts
{
    use SEOTools;

    /**
     * @var SimplePage
     */
    private $simplePageRepo;

    /**
     * Contacts constructor.
     * @param SimplePage $simplePageRepo
     */
    public function __construct(SimplePage $simplePageRepo)
    {
        $this->simplePageRepo = $simplePageRepo;
    }

    public function __invoke()
    {
        $contactsPage = $this->simplePageRepo->getSimplePageBySlug('contacts');

        $this->setSeo();

        return view('site.contacts', compact('contactsPage'));
    }

    protected function setSeo()
    {
        $this->seo()->setTitle(trans('site.contacts'));
    }
}
