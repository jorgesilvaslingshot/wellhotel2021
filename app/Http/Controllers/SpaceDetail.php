<?php

namespace App\Http\Controllers;

use App\Space;
use Artesaos\SEOTools\Traits\SEOTools;

class SpaceDetail
{
    use SEOTools;

    /**
     * @var Space
     */
    private $spaceRepo;

    /**
     * @param Space $spaceRepo
     */
    public function __construct(Space $spaceRepo)
    {
        $this->spaceRepo = $spaceRepo;
    }

    public function __invoke(Space $space)
    {
        $spaces = $this->spaceRepo->getPublishedSpaces();

        $index = $spaces->search(function ($item) use ($space) {
            return $item->id == $space->getId();
        });
        $spaceNumber = $index + 1;
        $totalSpaces = $space->count();

        $prevIndex = abs($index - 1 + $totalSpaces) % $totalSpaces;
        $nextIndex = abs($index + 1) % $totalSpaces;

        $prevSpace = $spaces->get($prevIndex);
        $nextSpace = $spaces->get($nextIndex);

        $this->setSeo($space);

        return view('site.spaces.detail', compact('space', 'spaces', 'spaceNumber', 'totalSpaces', 'prevSpace', 'nextSpace'));
    }

    protected function setSeo(Space $space)
    {
        $this->seo()->setTitle($space->getName());
    }
}
