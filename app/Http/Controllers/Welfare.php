<?php


namespace App\Http\Controllers;

use App\Amenity;
use App\Block;
use App\GuestService;
use App\Masthead;
use App\Space;
use App\SpaceType;
use Artesaos\SEOTools\Traits\SEOTools;

class Welfare
{
    use SEOTools;

    /**
     * @var Masthead
     */
    private $mastheadRepo;
    /**
     * @var Block
     */
    private $blockRepo;
    /**
     * @var Amenity
     */
    private $amenityRepo;
    /**
     * @var GuestService
     */
    private $guestServiceRepo;
    /**
     * @var SpaceType
     */
    private $spaceRepo;

    /**
     * Welfare constructor.
     * @param Masthead $mastheadRepo
     * @param Block $blockRepo
     * @param Space $spaceRepo
     * @param Amenity $amenityRepo
     * @param GuestService $guestServiceRepo
     */
    public function __construct(Masthead $mastheadRepo, Block $blockRepo, Space $spaceRepo, Amenity $amenityRepo, GuestService $guestServiceRepo)
    {
        $this->mastheadRepo = $mastheadRepo;
        $this->spaceRepo = $spaceRepo;
        $this->blockRepo = $blockRepo;
        $this->amenityRepo = $amenityRepo;
        $this->guestServiceRepo = $guestServiceRepo;
    }

    public function __invoke()
    {
        $masthead = $this->mastheadRepo->getMastheadBySlug('welfare');

        $spaces = $this->spaceRepo->getPublishedWelfareSpaces();

        $this->setSeo();

        return view('site.welfare', compact('masthead', 'spaces'));
    }

    protected function setSeo()
    {
        $this->seo()->setTitle(trans('site.the-hotel'));
    }
}
