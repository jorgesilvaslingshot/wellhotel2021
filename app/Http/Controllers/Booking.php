<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\EventType;
use App\Voucher;
use Illuminate\Http\Request;
use App\Vacation;



class Booking
{

    private $eventTypeRepo;

    public function __construct(EventType $eventTypeRepo, Voucher $voucherTypeRepo){
        $this->eventTypeRepo = $eventTypeRepo;
        $this->voucherTypeRepo = $voucherTypeRepo;
    }

    public function __invoke(Request $request)
    {
        $data = Vacation::addSelect('date_start AS from')->addSelect('date_end AS to')->get();

        $id = $request->get('id');
        $type = $request->get('type');
        $close = $this->getCloseUrl();

        return view('site.booking', compact('id', 'type', 'close', 'data'));
    }

    protected function getCloseUrl()
    {
        if (\URL::previous() != route('booking')) {
            session()->flash('_previous_close', \URL::previous());

            return \URL::previous(route('routes.welcome'));
        }

        $previous = session()->get('_previous_close');

        session()->reflash();

        return $previous ?? route('routes.welcome');
    }

    // pedido de proposta evento

    public function event(Request $request)
    {
        $data = Vacation::addSelect('date_start AS from')->addSelect('date_end AS to')->get();

        $events_type = $this->eventTypeRepo->listsTranslations('name')->pluck('name', 'id')->toArray();

        $id = $request->get('id');
        $type = $request->get('type');
        $close = $this->getCloseUrl();

        return view('site.booking-event', compact('id', 'type', 'close', 'data', 'events_type'));
    }

    // pedido de info voucher

    public function voucher(Request $request)
    {
        $data = Vacation::addSelect('date_start AS from')->addSelect('date_end AS to')->get();

        $id = $request->get('id');

        $close = $this->getCloseUrl();

        return view('site.booking-voucher', compact('id', 'close', 'data'));
    }
}
