<?php


namespace App\Http\Controllers;

use App\Event as Events;
use App\EventType;
use App\Masthead;
use Artesaos\SEOTools\Traits\SEOTools;

class Event
{
    use SEOTools;

    /**
     * @var Masthead
     */
    private $mastheadRepo;
    /**
     * @var Block
     */
    private $eventRepo;

    private $eventTypeRepo;

    /**
     * About constructor.
     * @param Masthead $mastheadRepo
     * @param Block $blockRepo
     * @param Space $spaceRepo
     * @param Amenity $amenityRepo
     * @param GuestService $guestServiceRepo
     */
    public function __construct(Masthead $mastheadRepo, Events $eventRepo, EventType $eventTypeRepo)
    {
        $this->mastheadRepo = $mastheadRepo;
        $this->eventRepo = $eventRepo;
        $this->eventTypeRepo = $eventTypeRepo;
    }

    public function __invoke()
    {

        $masthead = $this->mastheadRepo->getMastheadBySlug('event');

        $event = $this->eventRepo->getPublishedEvent();

        $events_type = $this->eventTypeRepo->listsTranslations('name')->pluck('name', 'id')->toArray();

        $this->setSeo();

        return view('site.event', compact('masthead', 'event', 'events_type'));
    }

    protected function setSeo()
    {
        $this->seo()->setTitle(trans('site.the-hotel'));
    }

}
