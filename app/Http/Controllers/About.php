<?php


namespace App\Http\Controllers;

use App\Amenity;
use App\Block;
use App\GuestService;
use App\Masthead;
use App\Space;
use App\SpaceType;
use Artesaos\SEOTools\Traits\SEOTools;

class About
{
    use SEOTools;

    /**
     * @var Masthead
     */
    private $mastheadRepo;
    /**
     * @var Block
     */
    private $blockRepo;
    /**
     * @var Amenity
     */
    private $amenityRepo;
    /**
     * @var GuestService
     */
    private $guestServiceRepo;
    /**
     * @var SpaceType
     */
    private $spaceRepo;

    /**
     * About constructor.
     * @param Masthead $mastheadRepo
     * @param Block $blockRepo
     * @param Space $spaceRepo
     * @param Amenity $amenityRepo
     * @param GuestService $guestServiceRepo
     */
    public function __construct(Masthead $mastheadRepo, Block $blockRepo, Space $spaceRepo, Amenity $amenityRepo, GuestService $guestServiceRepo)
    {
        $this->mastheadRepo = $mastheadRepo;
        $this->spaceRepo = $spaceRepo;
        $this->blockRepo = $blockRepo;
        $this->amenityRepo = $amenityRepo;
        $this->guestServiceRepo = $guestServiceRepo;
    }

    public function __invoke()
    {

        $masthead = $this->mastheadRepo->getMastheadBySlug('about');
        $locationBlock = $this->blockRepo->getBlockBySlug('location');
        $spaces = $this->spaceRepo->getPublishedAboutSpaces();
        $location2Block = $this->blockRepo->getBlockBySlug('location-2');
        $mainAmenities = $this->amenityRepo->getPublishedMainAmenities(10);
        $guestServices = $this->guestServiceRepo->getPublishedGuestServices();

        $this->setSeo();

        return view('site.about', compact('masthead', 'locationBlock', 'spaces', 'location2Block', 'mainAmenities', 'guestServices'));
    }

    protected function setSeo()
    {
        $this->seo()->setTitle(trans('site.the-hotel'));
    }

    public function sustainability()
    {

        $masthead = $this->mastheadRepo->getMastheadBySlug('sustainability');
        $locationBlock = $this->blockRepo->getBlockBySlug('sustainability');

        $this->seo()->setTitle(trans('routes.sustainability'));

        return view('site.detail-sustainability', compact('masthead', 'locationBlock'));
    }

}
