<?php

namespace App\Http\Controllers;

use App\Experience;
use Artesaos\SEOTools\Traits\SEOTools;

class ExperienceDetail
{
    use SEOTools;

    /**
     * @var Experience
     */
    private $experienceRepo;

    /**
     * @param Experience $experienceRepo
     */
    public function __construct(Experience $experienceRepo)
    {
        $this->experienceRepo = $experienceRepo;
    }

    public function __invoke(Experience $experience)
    {
        $experiences = $this->experienceRepo->getPublishedExperiences();

        $index = $experiences->search(function ($item) use ($experience) {
            return $item->id == $experience->getId();
        });
        $experienceNumber = $index + 1;
        $totalExperiences = $experiences->count();

        $prevIndex = abs($index - 1 + $totalExperiences) % $totalExperiences;
        $nextIndex = abs($index + 1) % $totalExperiences;

        $prevExperience = $experiences->get($prevIndex);
        $nextExperience = $experiences->get($nextIndex);

        $this->setSeo($experience);

        return view('site.experiences.detail', compact('experience', 'experiences', 'experienceNumber', 'totalExperiences', 'prevExperience', 'nextExperience'));
    }

    protected function setSeo(Experience $experience)
    {
        $this->seo()->setTitle($experience->getName());
        $this->seo()->setDescription($experience->getExcerpt());
        $this->seo()->addImages($experience->getImage());
    }
}
