<?php

namespace App\Http\Controllers\Api;

use App\Pack;
use App\Room;
use App\Traits\Localization;

class GetBookingProducts
{
    use Localization;
    /**
     * @var Room
     */
    private $roomRepo;
    /**
     * @var Pack
     */
    private $packRepo;

    /**
     * GetBookingProducts constructor.
     * @param Room $roomRepo
     * @param Pack $packRepo
     */
    public function __construct(Room $roomRepo, Pack $packRepo)
    {
        $this->roomRepo = $roomRepo;
        $this->packRepo = $packRepo;
    }

    public function __invoke()
    {
        return [
            'room' => $this->getRooms(),
            'pack' => $this->getPacks(),
        ];
    }

    protected function getRooms()
    {
        return $this->roomRepo->getPublishedRooms()->map(function ($item) {
            /**
             * @var Room $item
             */

            return [
                'id' => $item->getId(),
                'name' => $item->getName(),
                'max_guests' => $item->getMaxGuestsNumeric(),
            ];
        });
    }

    protected function getPacks()
    {
        return $this->packRepo->getPublishedPacks()->map(function ($item) {
            /**
             * @var Room $item
             */

            return [
                'id' => $item->getId(),
                'name' => $item->getName(),
                'max_guests' => $item->getMaxGuestsNumeric(),
            ];
        });
    }
}
