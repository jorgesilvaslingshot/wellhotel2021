<?php

namespace App\Http\Controllers\Api;

use App\VoucherRequest;
use App\Events\VoucherRequestRegistered;
use App\Http\Requests\Api\SaveVoucherRequest;
use App\Traits\Localization;

class SubmitVoucherRequest
{
    use Localization;
    /**
     * @var VoucherRequest
     */
    private $voucherRequestRepo;


    /**
     * SubmitBookingRequest constructor.
     * @param VoucherRequest $$VoucherRequestRepo
     */
    public function __construct(VoucherRequest $voucherRequestRepo)
    {
        $this->voucherRequestRepo = $voucherRequestRepo;
    }

    public function __invoke(SaveVoucherRequest $request)
    {
        $voucherRequest = $this->voucherRequestRepo->registerVoucherRequest(
            $request->validated(),
            $this->getLocale()
        );

        event(new VoucherRequestRegistered($voucherRequest));

        return ['message' => trans('site.voucher-request-has-been-sent')];
    }
}
