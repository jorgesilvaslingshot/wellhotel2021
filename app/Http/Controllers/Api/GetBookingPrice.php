<?php

namespace App\Http\Controllers\Api;

use App\Pack;
use App\Room;
use App\RoomPrice;
use App\PackPrice;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class GetBookingPrice
{
    /**
     * @var Room
     */
    private $roomRepo;
    /**
     * @var Pack
     */
    private $packRepo;

    /**
     * GetBookingPrice constructor.
     * @param Room $roomRepo
     * @param Pack $packRepo
     */
    public function __construct(Room $roomRepo, Pack $packRepo)
    {
        $this->roomRepo = $roomRepo;
        $this->packRepo = $packRepo;
    }

    public function __invoke(Request $request)
    {
        switch ($request->get('product_type')) {
            case 'room':
                return $this->getRoomPrice($request);
                break;
            case 'pack':
                return $this->getPackPrice($request);
                break;
            default:
                return null;
        }
    }

    protected function getRoomPrice(Request $request)
    {

        $productId = $request->get('product_id');
        $checkin = Carbon::parse($request->get('arrival'));
        $checkout = Carbon::parse($request->get('departure'))->subDay();

        $defaultPrice = Room::where('id', $productId)->first();

        $total = 0;

        $curDate = $checkin;

        while ($curDate <= $checkout)
        {

            $singlePrice = RoomPrice::where('date', $curDate)->whereHas('room_price_interval', function ($query) use ($productId){
                $query->where('room_id', $productId);
            })->orderBy('priority', 'Desc')->first();

            if (!is_null($singlePrice))
            {
                $total += /*$defaultPrice->price + */$singlePrice->price;

            }
            else
            {
                $total += $defaultPrice->price;

                if ($curDate->format('D') == 'Sat')
                {
                    $total += 10;

                }

            }


            $curDate->addDay();

        }

        return $total;

    }

    protected function getPackPrice(Request $request)
    {
        $productId = $request->get('product_id');
        $checkin = Carbon::parse($request->get('arrival'));
        $checkout = Carbon::parse($request->get('departure'))->subDay();

        $defaultPrice = Pack::where('id', $productId)->first();

        $total = 0;

        $curDate = $checkin;


        while ($curDate <= $checkout)
        {

            $singlePrice = PackPrice::where('date', $curDate)->whereHas('pack_price_interval', function ($query) use ($productId){
                $query->where('pack_id', $productId);
            })->orderBy('priority', 'Desc')->first();


            if (!is_null($singlePrice))
            {
                $total += $singlePrice->price;

            }
            else
            {
                $total += $defaultPrice->price;

                if ($curDate->format('D') == 'Sat' && $request->get('product_id') != '4')
                {
                    $total += 10;

                }

            }

            $curDate->addDay();

        }

        return $total;
    }

}
