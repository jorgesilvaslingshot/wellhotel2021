<?php

namespace App\Http\Controllers\Api;

use App\ContactRequest;
use App\Events\ContactRequestRegistered;
use App\Http\Requests\Api\SaveContactRequest;
use App\Traits\Localization;

class SubmitContactRequest
{
    use Localization;

    /**
     *
     * @var ContactRequest
     */
    private $contactRequestRepo;

    /**
     * ContactRequest constructor.
     * @param ContactRequest $contactRequestRepo
     */
    public function __construct(ContactRequest $contactRequestRepo)
    {
        $this->contactRequestRepo = $contactRequestRepo;
    }

    public function __invoke(SaveContactRequest $request)
    {
        $contactRequest = $this->contactRequestRepo->registerContactRequest(
            $request->get('name'),
            $request->get('email'),
            $request->get('message'),
            $this->getLocale()
        );

        event(new ContactRequestRegistered($contactRequest));

        return ['message' => trans('site.message-has-been-sent')];
    }
}
