<?php

namespace App\Http\Controllers\Api;

//use App\Voucher;
use App\VoucherTranslation;
use App\Traits\Localization;

class GetVoucherType
{
    use Localization;

    private $voucherTypeRepo;

    /**
     * GetBookingProducts constructor.
     * @param Room $roomRepo
     * @param Pack $packRepo
     */
    public function __construct(VoucherTranslation $voucherTypeRepo)
    {
        $this->voucherTypeRepo = $voucherTypeRepo;
    }

    public function __invoke()
    {
        return [
            $this->getVoucherType()
        ];
    }

    protected function getVoucherType()
    {
        return $this->voucherTypeRepo->getPublishedVouchers($this->getLocale());
        //return $this->voucherTypeRepo->listsTranslations('label')->pluck('label', 'id');
    }
}
