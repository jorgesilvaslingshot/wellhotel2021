<?php

namespace App\Http\Controllers\Api;

use App\ProposalRequest;
use App\Events\ProposalRequestRegistered;
use App\Http\Requests\Api\SaveProposalRequest;
use App\Traits\Localization;

class SubmitProposalRequest
{
    use Localization;
    /**
     * @var ProposalRequest
     */
    private $proposalRequestRepo;


    /**
     * SubmitBookingRequest constructor.
     * @param ProposalRequest $$proposalRequestRepo
     */
    public function __construct(ProposalRequest $proposalRequestRepo)
    {
        $this->proposalRequestRepo = $proposalRequestRepo;
    }

    public function __invoke(SaveProposalRequest $request)
    {
        $proposalRequest = $this->proposalRequestRepo->registerProposalRequest(
            $request->validated(),
            $this->getLocale()
        );

        event(new ProposalRequestRegistered($proposalRequest));

        return ['message' => trans('site.availability-request-has-been-sent')];
    }
}
