<?php

namespace App\Http\Controllers\Api;

use App\Pack;
use App\Room;
use App\RoomPrice;
use App\PackPrice;
use App\Vacation;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class GetVacationsIntervals
{

    public function __construct()
    {
    }

    public function __invoke()
    {
    }


    protected function getVacations(Request $request)
    {

        $date_start = Carbon::parse($request->get('date_start'));
        $date_end = Carbon::parse($request->get('date_end'));

        dd([$date_start, $date_end]);

    }

}
