<?php

namespace App\Http\Controllers\Api;

use App\AvailabilityRequest;
use App\Events\AvailabilityRequestRegistered;
use App\Http\Requests\Api\SaveBookingRequest;
use App\Traits\Localization;

class SubmitBookingRequest
{
    use Localization;
    /**
     * @var AvailabilityRequest
     */
    private $availabilityRequestRepo;


    /**
     * SubmitBookingRequest constructor.
     * @param AvailabilityRequest $availabilityRequestRepo
     */
    public function __construct(AvailabilityRequest $availabilityRequestRepo)
    {
        $this->availabilityRequestRepo = $availabilityRequestRepo;
    }

    public function __invoke(SaveBookingRequest $request)
    {
        $availabilityRequest = $this->availabilityRequestRepo->registerAvailabilityRequest(
            $request->validated(),
            $this->getLocale()
        );

        event(new AvailabilityRequestRegistered($availabilityRequest));

        return ['message' => trans('site.availability-request-has-been-sent')];
    }
}
