<?php

namespace App\Http\Controllers\Api;

use App\EventType;
use App\Pack;
use App\Room;
use App\Traits\Localization;

class GetEventType
{
    use Localization;

    private $eventTypeRepo;

    /**
     * GetBookingProducts constructor.
     * @param Room $roomRepo
     * @param Pack $packRepo
     */
    public function __construct(EventType $eventTypeRepo)
    {
        $this->eventTypeRepo = $eventTypeRepo;
    }

    public function __invoke()
    {
        return [
            $this->getEventType()
        ];
    }

    protected function getEventType()
    {
        return $this->eventTypeRepo->listsTranslations('name')->pluck('name', 'id')->toArray();
    }
}
