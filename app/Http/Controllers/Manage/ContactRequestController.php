<?php

namespace App\Http\Controllers\Manage;

use App\ContactRequest;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class ContactRequestController extends ResourceController
{
    public $datagridRowTemplate = 'manage.contact-requests.row';

    /**
     * @var ContactRequest
     */
    private $contactRequests;

    public function __construct(ContactRequest $contactRequests)
    {
        $this->contactRequests = $contactRequests;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->contactRequests->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('locale', 'Idioma', false, true)
            ->addColumn('email', 'E-mail', true, true)
            ->get();
    }

    public function edit($id)
    {
        $data = $this->contactRequests->findOrNew($id);
        $this->form = view('manage.contact-requests.form', compact('data'));
    }

    public function save()
    {
        return $this->saveSucess();
    }
}
