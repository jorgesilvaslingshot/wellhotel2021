<?php

namespace App\Http\Controllers\Manage;

use App\Amenity;
use App\Gallery;
use App\Http\Requests\SaveRoomRequest;
use App\Language;
use App\Room;
use App\RoomType;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class RoomController extends ResourceController
{
    public $datagridRowTemplate = 'manage.rooms.row';
    /**
     * @var Room
     */
    private $rooms;
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var Gallery
     */
    private $galleries;
    /**
     * @var Amenity
     */
    private $amenities;
    /**
     * @var RoomType
     */
    private $roomTypes;

    public function __construct(Room $rooms, Language $languages, Gallery $galleries, Amenity $amenities, RoomType $roomTypes)
    {
        $this->rooms = $rooms;
        $this->languages = $languages;
        $this->galleries = $galleries;
        $this->amenities = $amenities;
        $this->roomTypes = $roomTypes;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->rooms->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('type', 'Tipo', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('priority', 'Prioridade')
            ->addColumn('publish', 'Publicado', false, false)
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->rooms->findOrNew($id);

        $this->form = view('manage.rooms.form', compact('data'))
            ->with('galleries', $this->galleries->listsTranslations('name')->pluck('name', 'id')->toArray())
            ->with('amenities', $this->amenities->listsTranslations('name')->pluck('name', 'id')->toArray())
            ->with('roomTypes', $this->roomTypes->listsTranslations('name')->pluck('name', 'id')->toArray())
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SaveRoomRequest $request, $id = null)
    {
        $this->rooms->saveOrCreate($request->post(), $id);
        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            Room::find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
