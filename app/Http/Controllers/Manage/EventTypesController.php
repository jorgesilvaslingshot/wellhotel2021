<?php


namespace App\Http\Controllers\Manage;

use App\EventType;
use App\Http\Requests\SaveEventTypeRequest;
use App\Language;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class EventTypesController extends ResourceController
{
    public $datagridRowTemplate = 'manage.event-types.row';
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var EventType
     */
    private $eventTypeRepo;

    /**
     * PackTypesController constructor.
     * @param EventType $eventTypeRepo
     * @param Language $languages
     */
    public function __construct(EventType $eventTypeRepo, Language $languages)
    {
        $this->languages = $languages;
        $this->eventTypeRepo = $eventTypeRepo;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->eventTypeRepo->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('priority', 'Prioridade')
            ->addColumn('publish', 'Publicado')
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->eventTypeRepo->findOrNew($id);
        $this->form = view('manage.event-types.form', compact('data'))
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SaveEventTypeRequest $request, $id = null)
    {
        $this->eventTypeRepo->saveOrCreate($request->all(), $id);

        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            EventType::find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
