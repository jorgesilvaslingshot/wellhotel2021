<?php

namespace App\Http\Controllers\Manage;

use App\Gallery;
use App\Pack;
use App\Space;
use App\Http\Requests\SaveSpaceRequest;
use App\Language;
use App\SpaceType;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class SpacesController extends ResourceController
{
    public $datagridRowTemplate = 'manage.spaces.row';
    /**
     * @var Space
     */
    private $spaceRepo;
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var SpaceType
     */
    private $spaceTypeRepo;
    /**
     * @var Pack
     */
    private $packRepo;
    /**
     * @var Gallery
     */
    private $galleries;

    /**
     * @param Space $spaceRepo
     * @param Language $languages
     * @param Gallery $galleries
     * @param SpaceType $spaceTypeRepo
     * @param Pack $packRepo
     */
    public function __construct(Space $spaceRepo, Language $languages, Gallery $galleries, SpaceType $spaceTypeRepo, Pack $packRepo)
    {
        $this->spaceRepo = $spaceRepo;
        $this->languages = $languages;
        $this->spaceTypeRepo = $spaceTypeRepo;
        $this->packRepo = $packRepo;
        $this->galleries = $galleries;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->spaceRepo->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('priority', 'Prioridade')
            ->addColumn('publish', 'Publicado', false, false)
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->spaceRepo->findOrNew($id);
        $this->form = view('manage.spaces.form', compact('data'))
            ->with('spaceTypes', $this->spaceTypeRepo->listsTranslations('name')->pluck('name', 'id')->toArray())
            ->with('packs', $this->packRepo->listsTranslations('name')->pluck('name', 'id')->toArray())
            ->with('galleries', $this->galleries->listsTranslations('name')->pluck('name', 'id')->toArray())
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SaveSpaceRequest $request, $id = null)
    {
        $this->spaceRepo->saveOrCreate($request->post(), $id);
        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            Space::find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
