<?php


namespace App\Http\Controllers\Manage;

use App\SpaceType;
use App\Http\Requests\SaveSpaceTypeRequest;
use App\Language;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class SpaceTypesController extends ResourceController
{
    public $datagridRowTemplate = 'manage.space-types.row';
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var SpaceType
     */
    private $spaceTypeRepo;

    /**
     * PackTypesController constructor.
     * @param SpaceType $spaceTypeRepo
     * @param Language $languages
     */
    public function __construct(SpaceType $spaceTypeRepo, Language $languages)
    {
        $this->languages = $languages;
        $this->spaceTypeRepo = $spaceTypeRepo;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->spaceTypeRepo->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('priority', 'Prioridade')
            ->addColumn('publish', 'Publicado')
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->spaceTypeRepo->findOrNew($id);
        $this->form = view('manage.space-types.form', compact('data'))
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SaveSpaceTypeRequest $request, $id = null)
    {
        $this->spaceTypeRepo->saveOrCreate($request->all(), $id);

        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            SpaceType::find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
