<?php


namespace App\Http\Controllers\Manage;

use App\GuestService;
use App\Http\Requests\SaveGuestServiceRequest;
use App\Language;
use App\SimplePage;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class GuestServicesController extends ResourceController
{
    public $datagridRowTemplate = 'manage.guest-services.row';
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var SimplePage
     */
    private $guestServiceRepo;

    /**
     * GuestServicesController constructor.
     * @param GuestService $guestServiceRepo
     * @param Language $languages
     */
    public function __construct(GuestService $guestServiceRepo, Language $languages)
    {
        $this->languages = $languages;
        $this->guestServiceRepo = $guestServiceRepo;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->guestServiceRepo->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('title', 'Título', true, true)
            ->addColumn('priotity', 'Prioridade')
            ->addColumn('publish', 'Publicado', false, false)
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->guestServiceRepo->findOrNew($id);
        $this->form = view('manage.guest-services.form', compact('data'))
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SaveGuestServiceRequest $request, $id = null)
    {
        $this->guestServiceRepo->saveOrCreate($request->all(), $id);

        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            GuestService::find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
