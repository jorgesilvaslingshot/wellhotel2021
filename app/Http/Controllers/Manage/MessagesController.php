<?php

namespace App\Http\Controllers\Manage;

use App\Message;
use App\Http\Requests\SaveMessageRequest;
use App\Language;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class MessagesController extends ResourceController
{
    public $datagridRowTemplate = 'manage.messages.row';

    /**
     * @var Message
     */
    private $messageRepo;
    /**
     * @var Language
     */
    private $languages;

    public function __construct(Message $messageRepo, Language $languages)
    {
        $this->messageRepo = $messageRepo;
        $this->languages = $languages;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->messageRepo->query())))
            ->addColumn('id', 'ID', false)
            ->addColumn('author', 'Título')
            ->addColumn('message', 'Mensagem')
            ->addColumn('date', 'Data')
            ->addColumn('locale', 'Idioma')
            ->addColumn('priority', 'Prioridade')
            ->addColumn('publish', 'Publish')
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->messageRepo->findOrNew($id);
        $languages = $this->languages->allAsArrayList();
        $this->form = view('manage.messages.form', compact('data', 'languages'));
    }

    public function save(SaveMessageRequest $request, $id = null)
    {
        $this->messageRepo->saveOrCreate($request->post(), $id);
        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            $this->messageRepo->find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
