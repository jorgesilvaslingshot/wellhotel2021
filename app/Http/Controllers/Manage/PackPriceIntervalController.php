<?php

namespace App\Http\Controllers\Manage;

use App\Http\Requests\Manage\SavePackPriceInterval;
use App\PackPrice;
use App\PackPriceInterval;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Slingshot\Admin\Http\Controllers\ResourceController;

class PackPriceIntervalController extends ResourceController
{
    /**
     * @var PackPriceInterval
     */
    private $packPriceIntervalRepo;

    /**
     * PackPriceIntervalController constructor.
     * @param PackPriceInterval $packPriceIntervalRepo
     */
    public function __construct(PackPriceInterval $packPriceIntervalRepo)
    {
        $this->packPriceIntervalRepo = $packPriceIntervalRepo;
    }

    public function index()
    {
        abort(404);
    }

    public function edit(Request $request, $id = null)
    {
        if (!$request->has('pack_id')) {
            abort(404);
        }

        $data = $this->packPriceIntervalRepo->findOrNew($id);
        $data->pack_id = $request->get('pack_id');

        $this->form = view('manage.pack-price-intervals.form', compact('data'))
            ->with('pack_id', $request->get('pack_id'));
    }

    public function save(SavePackPriceInterval $request, $id = null)
    {
        $from = Carbon::parse($request->get('date_start'));
        $to = Carbon::parse($request->get('date_end'));
        $dates = $this->generateDateRange($from, $to);

        $request->merge(compact('dates'));

        $this->packPriceIntervalRepo->saveOrCreate($request->post(), $id);
        $msg = 'Preço inserido com sucesso!';
        return redirect()->route('admin.packs.edit', $request->get('pack_id'))->with('success', [$msg]);
    }

    public function delete($id)
    {
        try {
            PackPriceInterval::find($id)->delete();
            PackPrice::where('pack_price_interval_id', $id)->delete();

        } catch (\Exception $ignore) {
        }
    }

    protected function generateDateRange(Carbon $start_date, Carbon $end_date)
    {
        $dates = [];

        for($date = $start_date; $date->lte($end_date); $date->addDay()) {

            $dates[] = $date->format('Y-m-d');

        }

        return $dates;
    }
}
