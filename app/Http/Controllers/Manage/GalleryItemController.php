<?php

namespace App\Http\Controllers\Manage;

use App\GalleryItem;
use App\Http\Requests\SaveGalleryItemRequest;
use App\Language;
use Illuminate\Http\Request;
use Slingshot\Admin\Http\Controllers\ResourceController;

class GalleryItemController extends ResourceController
{
    /**
     * @var GalleryItem
     */
    private $galleryItems;
    /**
     * @var Language
     */
    private $languages;

    public function __construct(GalleryItem $galleryItems, Language $languages)
    {
        $this->galleryItems = $galleryItems;
        $this->languages = $languages;
    }

    public function index()
    {
        abort(404);
    }

    public function edit(Request $request, $id = null)
    {
        if (!$request->has('gallery_id')) {
            abort(404);
        }

        $data = $this->galleryItems->findOrNew($id);
        $data->gallery_id = $request->get('gallery_id');
        $languages = $this->languages->allAsArrayList();
        $this->form = view('manage.gallery-items.form', compact('data', 'languages'))
            ->with('gallery_id', $request->get('gallery_id'));
    }

    public function save(SaveGalleryItemRequest $request, $id = null)
    {
        $this->galleryItems->saveOrCreate($request->post(), $id);
        $msg = 'Imagem inserida com sucesso!';
        return redirect()->route('admin.galleries.edit', $request->get('gallery_id'))->with('success', [$msg]);
    }

    public function delete($id)
    {
        try {
            GalleryItem::find($id)->delete();
        } catch (\Exception $ignore) {
        }
    }
}
