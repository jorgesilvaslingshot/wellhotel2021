<?php


namespace App\Http\Controllers\Manage;

use App\Gallery;
use App\Http\Requests\SaveBlockRequest;
use App\Language;
use App\Block;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class BlocksController extends ResourceController
{
    public $datagridRowTemplate = 'manage.blocks.row';
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var Block
     */
    private $blockRepo;
    /**
     * @var Gallery
     */
    private $galleries;

    /**
     * BlocksController constructor.
     * @param Block $blockRepo
     * @param Language $languages
     * @param Gallery $galleries
     */
    public function __construct(Block $blockRepo, Language $languages, Gallery $galleries)
    {
        $this->languages = $languages;
        $this->blockRepo = $blockRepo;
        $this->galleries = $galleries;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->blockRepo->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('title', 'Título', true, true)
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->blockRepo->findOrNew($id);
        $this->form = view('manage.blocks.form', compact('data'))
            ->with('languages', $this->languages->allAsArrayList())
            ->with('galleries', $this->galleries->listsTranslations('name')->pluck('name', 'id')->toArray())
        ;
    }

    public function save(SaveBlockRequest $request, $id = null)
    {
        $this->blockRepo->saveOrCreate($request->all(), $id);

        return $this->saveSucess();
    }
}
