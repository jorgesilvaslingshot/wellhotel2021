<?php

namespace App\Http\Controllers\Manage;

use App\Amenity;
use App\AmenityCategory;
use App\Http\Requests\SaveAmenityRequest;
use App\Language;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class AmenityController extends ResourceController
{
    public $datagridRowTemplate = 'manage.amenities.row';
    /**
     * @var Amenity
     */
    private $amenities;
    /**
     * @var Language
     */
    private $languages;

    public function __construct(Amenity $amenities, Language $languages)
    {
        $this->languages = $languages;
        $this->amenities = $amenities;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->amenities->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('is_main', 'Principal?')
            ->addColumn('priority', 'Prioridade')
            ->addColumn('publish', 'Publicado')
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->amenities->findOrNew($id);
        $this->form = view('manage.amenities.form', compact('data'))
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SaveAmenityRequest $request, $id = null)
    {
        $this->amenities->saveOrCreate($request->post(), $id);
        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            Amenity::find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
