<?php

namespace App\Http\Controllers\Manage;

use App\Experience;
use App\Gallery;
use App\Http\Requests\SavePackRequest;
use App\Language;
use App\Pack;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class PackController extends ResourceController
{
    public $datagridRowTemplate = 'manage.packs.row';
    /**
     * @var Pack
     */
    private $packs;
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var Experience
     */
    private $experiences;

    /**
     * PackController constructor.
     * @param Pack $packs
     * @param Language $languages
     * @param Experience $experiences
     */
    public function __construct(Pack $packs, Gallery $galleries, Language $languages, Experience $experiences)
    {
        $this->packs = $packs;
        $this->languages = $languages;
        $this->galleries = $galleries;
        $this->experiences = $experiences;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->packs->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('priority', 'Prioridade')
            ->addColumn('publish', 'Publicado', false, false)
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->packs->findOrNew($id);
        $this->form = view('manage.packs.form', compact('data'))
            ->with('galleries', $this->galleries->listsTranslations('name')->pluck('name', 'id')->toArray())
            ->with('experiences', $this->experiences->listsTranslations('name')->pluck('name', 'id')->toArray())
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SavePackRequest $request, $id = null)
    {
        $this->packs->saveOrCreate($request->post(), $id);
        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            Pack::find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
