<?php


namespace App\Http\Controllers\Manage;

use App\Http\Requests\SavePromotionRequest;
use App\Language;
use App\Promotion;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class PromotionsController extends ResourceController
{
    public $datagridRowTemplate = 'manage.promotions.row';
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var Promotion
     */
    private $promotionRepo;

    /**
     * PromotionsController constructor.
     * @param Promotion $promotionRepo
     * @param Language $languages
     */
    public function __construct(Promotion $promotionRepo, Language $languages)
    {
        $this->languages = $languages;
        $this->promotionRepo = $promotionRepo;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->promotionRepo->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('title', 'Título', true, true)
            ->addColumn('start_date', 'Data início')
            ->addColumn('end_date', 'Data fim')
            ->addColumn('publish', 'Publicado')
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->promotionRepo->findOrNew($id);
        $this->form = view('manage.promotions.form', compact('data'))
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SavePromotionRequest $request, $id = null)
    {
        $this->promotionRepo->saveOrCreate($request->all(), $id);

        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            $this->promotionRepo->find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
