<?php


namespace App\Http\Controllers\Manage;

use App\Http\Requests\SavePageRequest;
use App\Language;
use App\Page;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class PagesController extends ResourceController
{
    public $datagridRowTemplate = 'manage.pages.row';
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var Page
     */
    private $pageRepo;

    public function __construct(Page $pageRepo, Language $languages)
    {
        $this->languages = $languages;
        $this->pageRepo = $pageRepo;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->pageRepo->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('locale', 'Idioma', true, true)
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->pageRepo->findOrNew($id);
        $this->form = view('manage.pages.form', compact('data'))
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SavePageRequest $request, $id = null)
    {
        $this->pageRepo->saveOrCreate($request->all(), $id);

        return $this->saveSucess();
    }
}
