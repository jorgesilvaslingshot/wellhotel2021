<?php

namespace App\Http\Controllers\Manage;

use App\AmenityCategory;
use App\Http\Requests\SaveAmenityCategoryRequest;
use App\Language;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class AmenityCategoryController extends ResourceController
{
    public $datagridRowTemplate = 'manage.amenity-categories.row';
    /**
     * @var AmenityCategory
     */
    private $amenityCategories;
    /**
     * @var Language
     */
    private $languages;

    public function __construct(AmenityCategory $amenityCategories, Language $languages)
    {
        $this->amenityCategories = $amenityCategories;
        $this->languages = $languages;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->amenityCategories->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('priority', 'Prioridade')
            ->addColumn('publish', 'Publicado', false, false)
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->amenityCategories->findOrNew($id);
        $this->form = view('manage.amenity-categories.form', compact('data'))
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SaveAmenityCategoryRequest $request, $id = null)
    {
        $this->amenityCategories->saveOrCreate($request->post(), $id);
        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            AmenityCategory::find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
