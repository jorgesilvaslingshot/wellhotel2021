<?php

namespace App\Http\Controllers\Manage;

use App\Slide;
use App\Http\Requests\SaveSlideRequest;
use App\Language;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class SlidesController extends ResourceController
{
    public $datagridRowTemplate = 'manage.slides.row';

    /**
     * @var Slide
     */
    private $slides;
    /**
     * @var Language
     */
    private $languages;

    public function __construct(Slide $slides, Language $languages)
    {
        $this->slides = $slides;
        $this->languages = $languages;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->slides->query())))
            ->addColumn('id', 'ID', false)
            ->addColumn('title', 'Título')
            ->addColumn('locale', 'Idioma')
            ->addColumn('priority', 'Prioridade')
            ->addColumn('publish', 'Publish')
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->slides->findOrNew($id);
        $languages = $this->languages->allAsArrayList();
        $this->form = view('manage.slides.form', compact('data', 'languages'));
    }

    public function save(SaveSlideRequest $request, $id = null)
    {
        $this->slides->saveOrCreate($request->post(), $id);
        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            $this->slides->find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
