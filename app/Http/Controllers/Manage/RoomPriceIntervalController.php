<?php

namespace App\Http\Controllers\Manage;

use App\Http\Requests\Manage\SaveRoomPriceInterval;
use App\RoomPrice;
use App\RoomPriceInterval;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Slingshot\Admin\Http\Controllers\ResourceController;

class RoomPriceIntervalController extends ResourceController
{
    /**
     * @var RoomPriceInterval
     */
    private $roomPriceIntervalRepo;

    /**
     * RoomPriceIntervalController constructor.
     * @param RoomPriceInterval $roomPriceIntervalRepo
     */
    public function __construct(RoomPriceInterval $roomPriceIntervalRepo)
    {
        $this->roomPriceIntervalRepo = $roomPriceIntervalRepo;
    }

    public function index()
    {
        abort(404);
    }

    public function edit(Request $request, $id = null)
    {
        if (!$request->has('room_id')) {
            abort(404);
        }

        $data = $this->roomPriceIntervalRepo->findOrNew($id);
        $data->room_id = $request->get('room_id');

        //dd($data);

        $this->form = view('manage.room-price-intervals.form', compact('data'))
            ->with('room_id', $request->get('room_id'));
    }

    public function save(SaveRoomPriceInterval $request, $id = null)
    {

        $from = Carbon::parse($request->get('date_start'));
        $to = Carbon::parse($request->get('date_end'));
        $dates = $this->generateDateRange($from, $to);

        $request->merge(compact('dates'));

        $this->roomPriceIntervalRepo->saveOrCreate($request->post(), $id);
        $msg = 'Preço inserido com sucesso!';
        return redirect()->route('admin.rooms.edit', $request->get('room_id'))->with('success', [$msg]);
    }

    public function delete($id)
    {

        //\DB::transaction(function ($id) use ($id) {

            try {
                RoomPrice::where('room_price_interval_id', $id)->delete();
                RoomPriceInterval::find($id)->delete();

            } catch (\Exception $ignore){

            }

        //});

    }

    protected function generateDateRange(Carbon $start_date, Carbon $end_date)
    {
        $dates = [];

        for ($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');

        }

        return $dates;
    }

}
