<?php

namespace App\Http\Controllers\Manage;

use App\Voucher;
use App\Gallery;
use App\Http\Requests\SaveVoucherRequest;
use App\Language;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class VoucherController extends ResourceController
{
    public $datagridRowTemplate = 'manage.vouchers.row';
    /**
     * @var Voucher
     */
    private $vouchers;
    /**
     * @var Language
     */
    private $languages;

    /**
     * VoucherController constructor.
     * @param Voucher $vouchers
     * @param Language $languages
     */
    public function __construct(Voucher $vouchers, Gallery $galleries, Language $languages)
    {
        $this->vouchers = $vouchers;
        $this->galleries = $galleries;
        $this->languages = $languages;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->vouchers->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('priority', 'Prioridade')
            ->addColumn('publish', 'Publicado', false, false)
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->vouchers->findOrNew($id);
        $this->form = view('manage.vouchers.form', compact('data'))
            ->with('galleries', $this->galleries->listsTranslations('name')->pluck('name', 'id')->toArray())
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SaveVoucherRequest $request, $id = null)
    {
        $this->vouchers->saveOrCreate($request->post(), $id);
        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            Voucher::find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
