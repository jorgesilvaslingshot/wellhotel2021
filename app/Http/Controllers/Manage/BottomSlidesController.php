<?php

namespace App\Http\Controllers\Manage;

use App\BottomSlide;
use App\Http\Requests\SaveBottomSlideRequest;
use App\Language;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class BottomSlidesController extends ResourceController
{
    public $datagridRowTemplate = 'manage.bottom-slides.row';


    private $languages;
    /**
     * @var BottomSlide
     */
    private $bottomSlideRepo;

    /**
     * BottomSlidesController constructor.
     * @param BottomSlide $bottomSlideRepo
     * @param Language $languages
     */
    public function __construct(BottomSlide $bottomSlideRepo, Language $languages)
    {
        $this->languages = $languages;
        $this->bottomSlideRepo = $bottomSlideRepo;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->bottomSlideRepo->query())))
            ->addColumn('id', 'ID', false)
            ->addColumn('title', 'Título')
            ->addColumn('locale', 'Idioma')
            ->addColumn('priority', 'Prioridade')
            ->addColumn('publish', 'Publish')
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->bottomSlideRepo->findOrNew($id);
        $languages = $this->languages->allAsArrayList();
        $this->form = view('manage.bottom-slides.form', compact('data', 'languages'));
    }

    public function save(SaveBottomSlideRequest $request, $id = null)
    {
        $this->bottomSlideRepo->saveOrCreate($request->post(), $id);
        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            $this->bottomSlideRepo->find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
