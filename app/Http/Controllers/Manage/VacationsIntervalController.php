<?php

namespace App\Http\Controllers\Manage;

use App\Http\Requests\Manage\SaveVacationsInterval;
use App\Vacation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class VacationsIntervalController extends ResourceController
{

    public $datagridRowTemplate = 'manage.vacations.row';


    private $vacation;

    public function __construct(Vacation $vacation)
    {
        $this->vacation = $vacation;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->vacation->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('description', 'Descrição', false, true)
            ->addColumn('date_start', 'De', false, true)
            ->addColumn('date_end', 'Até', true, true)
            ->get();
    }

    public function edit(Request $request, $id = null)
    {

        $data = $this->vacation->findOrNew($id);

        $this->form = view('manage.vacations.form', compact('data'));
    }

    public function save(SaveVacationsInterval $request, $id = null)
    {

        //dd($request);

        $this->vacation->updateOrCreate(compact('id'), $request->all());
        $msg = 'Sucesso!';
        return redirect()->route('admin.vacations')->with('success', [$msg]);
    }

    public function delete($id)
    {
        try {
            $this->vacation::find($id)->delete();
        } catch (\Exception $ignore) {
        }
    }

}
