<?php

namespace App\Http\Controllers\Manage;

use App\Gallery;
use App\Pack;
use App\Event;
use App\Http\Requests\SaveEventRequest;
use App\Language;
use App\EventType;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class EventsController extends ResourceController
{
    public $datagridRowTemplate = 'manage.events.row';
    /**
     * @var Event
     */
    private $eventRepo;
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var EventType
     */
    private $eventTypeRepo;
    /**
     * @var Pack
     */
    private $packRepo;
    /**
     * @var Gallery
     */
    private $galleries;

    /**
     * @param event $eventRepo
     * @param Language $languages
     * @param Gallery $galleries
     * @param eventType $eventTypeRepo
     * @param Pack $packRepo
     */
    public function __construct(Event $eventRepo, Language $languages, Gallery $galleries, EventType $eventTypeRepo, Pack $packRepo)
    {
        $this->eventRepo = $eventRepo;
        $this->languages = $languages;
        $this->eventTypeRepo = $eventTypeRepo;
        $this->packRepo = $packRepo;
        $this->galleries = $galleries;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->eventRepo->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('priority', 'Prioridade')
            ->addColumn('publish', 'Publicado', false, false)
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->eventRepo->findOrNew($id);
        $this->form = view('manage.events.form', compact('data'))
            ->with('eventTypes', $this->eventTypeRepo->listsTranslations('name')->pluck('name', 'id')->toArray())
            ->with('galleries', $this->galleries->listsTranslations('name')->pluck('name', 'id')->toArray())
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SaveEventRequest $request, $id = null)
    {
        $this->eventRepo->saveOrCreate($request->post(), $id);
        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            Event::find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
