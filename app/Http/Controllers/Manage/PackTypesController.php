<?php


namespace App\Http\Controllers\Manage;

use App\Http\Requests\SavePackTypeRequest;
use App\Language;
use App\PackType;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class PackTypesController extends ResourceController
{
    public $datagridRowTemplate = 'manage.pack-types.row';
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var PackType
     */
    private $packTypeRepo;

    /**
     * PackTypesController constructor.
     * @param PackType $packTypeRepo
     * @param Language $languages
     */
    public function __construct(PackType $packTypeRepo, Language $languages)
    {
        $this->languages = $languages;
        $this->packTypeRepo = $packTypeRepo;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->packTypeRepo->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('priority', 'Prioridade')
            ->addColumn('publish', 'Publicado')
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->packTypeRepo->findOrNew($id);
        $this->form = view('manage.pack-types.form', compact('data'))
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SavePackTypeRequest $request, $id = null)
    {
        $this->packTypeRepo->saveOrCreate($request->all(), $id);

        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            PackType::find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
