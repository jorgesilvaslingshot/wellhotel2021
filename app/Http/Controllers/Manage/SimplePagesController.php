<?php


namespace App\Http\Controllers\Manage;

use App\Http\Requests\SaveSimplePageRequest;
use App\Language;
use App\SimplePage;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class SimplePagesController extends ResourceController
{
    public $datagridRowTemplate = 'manage.simple-pages.row';
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var SimplePage
     */
    private $simplePageRepo;

    public function __construct(SimplePage $simplePageRepo, Language $languages)
    {
        $this->languages = $languages;
        $this->simplePageRepo = $simplePageRepo;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->simplePageRepo->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('title', 'Título', true, true)
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->simplePageRepo->findOrNew($id);
        $this->form = view('manage.simple-pages.form', compact('data'))
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SaveSimplePageRequest $request, $id = null)
    {
        $this->simplePageRepo->saveOrCreate($request->all(), $id);

        return $this->saveSucess();
    }
}
