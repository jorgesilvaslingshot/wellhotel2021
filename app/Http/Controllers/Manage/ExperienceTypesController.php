<?php


namespace App\Http\Controllers\Manage;

use App\ExperienceType;
use App\Http\Requests\SaveExperienceTypeRequest;
use App\Language;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class ExperienceTypesController extends ResourceController
{
    public $datagridRowTemplate = 'manage.experience-types.row';
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var ExperienceType
     */
    private $experienceTypeRepo;

    /**
     * PackTypesController constructor.
     * @param ExperienceType $experienceTypeRepo
     * @param Language $languages
     */
    public function __construct(ExperienceType $experienceTypeRepo, Language $languages)
    {
        $this->languages = $languages;
        $this->experienceTypeRepo = $experienceTypeRepo;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->experienceTypeRepo->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('priority', 'Prioridade')
            ->addColumn('publish', 'Publicado')
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->experienceTypeRepo->findOrNew($id);
        $this->form = view('manage.experience-types.form', compact('data'))
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SaveExperienceTypeRequest $request, $id = null)
    {
        $this->experienceTypeRepo->saveOrCreate($request->all(), $id);

        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            ExperienceType::find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
