<?php


namespace App\Http\Controllers\Manage;

use App\Phrase;
use App\Http\Requests\SavePhraseRequest;
use App\Language;
use Illuminate\Cache\Repository as CacheRepository;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class PhrasesController extends ResourceController
{
    public $datagridRowTemplate = 'manage.phrases.row';
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var Phrase
     */
    private $phraseRepo;
    /**
     * @var CacheRepository
     */
    private $cacheRepository;

    public function __construct(Phrase $phraseRepo, Language $languages, CacheRepository $cacheRepository)
    {
        $this->languages = $languages;
        $this->phraseRepo = $phraseRepo;
        $this->cacheRepository = $cacheRepository;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->phraseRepo->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->phraseRepo->findOrNew($id);
        $this->form = view('manage.phrases.form', compact('data'))
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SavePhraseRequest $request, $id = null)
    {
        $this->phraseRepo->saveOrCreate($request->all(), $id);
        // colocar na cache
        $locale = \App::getLocale();
        foreach ($this->languages->all() as $language) {
            \App::setLocale($language->slug);
            $frases = $this->phraseRepo->all()->pluck('content', 'slug');
            $this->cacheRepository->forever("phrases_{$language->slug}", $frases);
        }
        \App::setLocale($locale);
        return $this->saveSucess();
    }
}
