<?php


namespace App\Http\Controllers\Manage;

use App\Http\Requests\SaveMastheadRequest;
use App\Language;
use App\Masthead;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class MastheadsController extends ResourceController
{
    public $datagridRowTemplate = 'manage.mastheads.row';
    /**
     * @var Language
     */
    private $languages;
    /**
     * @var Masthead
     */
    private $mastheadRepo;

    public function __construct(Masthead $mastheadRepo, Language $languages)
    {
        $this->languages = $languages;
        $this->mastheadRepo = $mastheadRepo;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->mastheadRepo->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('title', 'Título', true, true)
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->mastheadRepo->findOrNew($id);
        $this->form = view('manage.mastheads.form', compact('data'))
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SaveMastheadRequest $request, $id = null)
    {
        $this->mastheadRepo->saveOrCreate($request->all(), $id);

        return $this->saveSucess();
    }
}
