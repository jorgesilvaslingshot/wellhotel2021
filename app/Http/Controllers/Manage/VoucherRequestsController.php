<?php

namespace App\Http\Controllers\Manage;

use App\VoucherRequest;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class VoucherRequestsController extends ResourceController
{
    public $datagridRowTemplate = 'manage.voucher-requests.row';
    /**
     * @var VoucherRequest
     */
    private $voucherRequests;

    public function __construct(VoucherRequest $voucherRequests)
    {
        $this->voucherRequests = $voucherRequests;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->voucherRequests->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('email', 'E-mail', true, true)
            ->get();
    }

    public function edit($id)
    {
        $data = $this->voucherRequests->findOrNew($id);
        $this->form = view('manage.voucher-requests.form', compact('data'));
    }

    public function save()
    {
        return $this->saveSucess();
    }
}
