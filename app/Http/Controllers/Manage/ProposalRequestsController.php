<?php

namespace App\Http\Controllers\Manage;

use App\ProposalRequest;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class ProposalRequestsController extends ResourceController
{
    public $datagridRowTemplate = 'manage.proposal-requests.row';
    /**
     * @var ProposalRequest
     */
    private $proposalRequests;

    public function __construct(ProposalRequest $proposalRequests)
    {
        $this->proposalRequests = $proposalRequests;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->proposalRequests->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('email', 'E-mail', true, true)
            ->get();
    }

    public function edit($id)
    {
        $data = $this->proposalRequests->findOrNew($id);
        $this->form = view('manage.proposal-requests.form', compact('data'));
    }

    public function save()
    {
        return $this->saveSucess();
    }
}
