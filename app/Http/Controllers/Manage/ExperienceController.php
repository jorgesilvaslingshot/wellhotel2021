<?php

namespace App\Http\Controllers\Manage;

use App\Experience;
use App\Gallery;
use App\Http\Requests\SaveExperienceRequest;
use App\Language;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class ExperienceController extends ResourceController
{
    public $datagridRowTemplate = 'manage.experiences.row';
    /**
     * @var Experience
     */
    private $experiences;
    /**
     * @var Language
     */
    private $languages;

    /**
     * ExperienceController constructor.
     * @param Experience $experiences
     * @param Language $languages
     */
    public function __construct(Experience $experiences, Gallery $galleries, Language $languages)
    {
        $this->experiences = $experiences;
        $this->galleries = $galleries;
        $this->languages = $languages;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->experiences->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('priority', 'Prioridade')
            ->addColumn('publish', 'Publicado', false, false)
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->experiences->findOrNew($id);
        $this->form = view('manage.experiences.form', compact('data'))
            ->with('galleries', $this->galleries->listsTranslations('name')->pluck('name', 'id')->toArray())
            ->with('languages', $this->languages->allAsArrayList());
    }

    public function save(SaveExperienceRequest $request, $id = null)
    {
        $this->experiences->saveOrCreate($request->post(), $id);
        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            Experience::find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
