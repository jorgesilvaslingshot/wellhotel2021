<?php

namespace App\Http\Controllers\Manage;

use App\Gallery;
use App\Http\Requests\SaveGalleryRequest;
use App\Language;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class GalleryController extends ResourceController
{
    public $datagridRowTemplate = 'manage.galleries.row';

    /**
     * @var Gallery
     */
    private $galleries;
    /**
     * @var Language
     */
    private $languages;

    public function __construct(Gallery $galleries, Language $languages)
    {
        $this->galleries = $galleries;
        $this->languages = $languages;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->galleries->query())))
            ->addColumn('id', 'ID', false)
            ->addColumn('name', 'Nome')
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->galleries->findOrNew($id);
        $items = $data->items;
        $languages = $this->languages->allAsArrayList();
        $this->form = view('manage.galleries.form', compact('data', 'items', 'languages'));
    }

    public function save(SaveGalleryRequest $request, $id = null)
    {
        $this->galleries->saveOrCreate($request->post(), $id);
        return $this->saveSucess();
    }

    public function delete($id)
    {
        try {
            Gallery::find($id)->delete();
        } catch (\Exception $ignore) {
        }
        return $this->goBack();
    }
}
