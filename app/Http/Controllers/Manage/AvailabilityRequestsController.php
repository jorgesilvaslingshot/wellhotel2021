<?php

namespace App\Http\Controllers\Manage;

use App\AvailabilityRequest;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Slingshot\Admin\Services\EloquentDatagridProvider;

class AvailabilityRequestsController extends ResourceController
{
    public $datagridRowTemplate = 'manage.availability-requests.row';
    /**
     * @var AvailabilityRequest
     */
    private $availabilityRequests;

    public function __construct(AvailabilityRequest $availabilityRequests)
    {
        $this->availabilityRequests = $availabilityRequests;
    }

    public function index()
    {
        $this->datagrid = (new Datagrid(new EloquentDatagridProvider($this->availabilityRequests->newQuery())))
            ->addColumn('id', 'Id', false, true)
            ->addColumn('name', 'Nome', true, true)
            ->addColumn('email', 'E-mail', true, true)
            ->get();
    }

    public function edit($id)
    {
        $data = $this->availabilityRequests->findOrNew($id);
        $this->form = view('manage.availability-requests.form', compact('data'));
    }

    public function save()
    {
        return $this->saveSucess();
    }
}
