<?php

namespace App\Http\Controllers;

use App\Masthead;
use App\SimplePage;
use Artesaos\SEOTools\Traits\SEOTools;

class PrivacyPolicy
{
    use SEOTools;

    /**
     * @var SimplePage
     */
    private $simplePageRepo;
    /**
     * @var Masthead
     */
    private $mastheadRepo;

    /**
     * Contacts constructor.
     * @param SimplePage $simplePageRepo
     * @param Masthead $mastheadRepo
     */
    public function __construct(SimplePage $simplePageRepo, Masthead $mastheadRepo)
    {
        $this->simplePageRepo = $simplePageRepo;
        $this->mastheadRepo = $mastheadRepo;
    }

    public function __invoke()
    {
        $masthead = $this->mastheadRepo->getMastheadBySlug('privacy-policy');
        $privacyPolicyPage = $this->simplePageRepo->getSimplePageBySlug('privacy-policy');

        $this->setSeo();

        return view('site.privacy-policy', compact('masthead', 'privacyPolicyPage'));
    }

    protected function setSeo()
    {
        $this->seo()->setTitle(trans('site.privacy-policy'));
    }
}
