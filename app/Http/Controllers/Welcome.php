<?php

namespace App\Http\Controllers;

use App\Amenity;
use App\Block;
use App\Voucher;
use App\RoomPrice;
use App\Slide;
use App\Traits\Localization;
use Illuminate\Http\Request;

class Welcome
{
    use Localization;

    /**
     * @var Slide
     */
    protected $slideRepo;
    /**
     * @var Block
     */
    private $blockRepo;
    /**
     * @var Voucher
     */
    private $voucherRepo;
    /**
     * @var Amenity
     */
    private $amenityRepo;

    /**
     * Welcome constructor.
     * @param Slide $slideRepo
     * @param Block $blockRepo
     * @param Voucher $voucherRepo
     * @param Amenity $amenityRepo
     */
    public function __construct(Slide $slideRepo, Block $blockRepo, Voucher $voucherRepo, Amenity $amenityRepo)
    {
        $this->slideRepo = $slideRepo;
        $this->blockRepo = $blockRepo;
        $this->voucherRepo = $voucherRepo;
        $this->amenityRepo = $amenityRepo;
    }

    public function __invoke(Request $request)
    {
        $slides = $this->slideRepo->getPublishedSlidesByLocale($this->getLocale(), 4);
        $aboutUsBlock = $this->blockRepo->getBlockBySlug('about-us');
        $vouchers = $this->voucherRepo->getPublishedVouchers(3);
        $mainAmenities = $this->amenityRepo->getPublishedMainAmenities(4);

        return view('site.welcome', compact('slides', 'aboutUsBlock', 'vouchers', 'mainAmenities'));
    }
}
