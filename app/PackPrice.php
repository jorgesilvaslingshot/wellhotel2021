<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackPrice extends Model
{
    protected $fillable = ['date', 'price', 'pack_price_interval_id', 'priority'];

    public $timestamps = false;

    public $incrementing = false;

    protected $primaryKey = ['date', 'pack_price_interval_id'];

    public function pack_price_interval()
    {
        return $this->belongsTo(PackPriceInterval::class);
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPackPriceInterval()
    {
        return $this->pack_price_interval;
    }
}
