<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomTypeTranslation extends Model
{
    protected $fillable = ['name'];
}
