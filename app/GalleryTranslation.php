<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryTranslation extends Model
{
    protected $fillable = ['name', 'description'];
}
