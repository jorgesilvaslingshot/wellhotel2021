<?php


namespace App\Admin;

use Slingshot\Admin\Services\FormBuilder;

class MyForm extends FormBuilder
{
    /**
     * @param string $name
     * @param string $type
     * @param null $label
     * @param null $value
     * @param array $attributes
     * @param array $extra
     * @return string
     */
    public function boInput($name, $type, $label = null, $value = null, $attributes = [], $extra = [])
    {
        if (empty($extra['sizes'])) {
            $extra['sizes'] = 'col-sm-6 col-md-4';
        }

        $attributes = $this->fieldAttributes($name, $attributes);

        $element = $this->input($type, $name, $value, $attributes);

        return $this->fieldWrapper($name, $label, $element, $extra);
    }
}
