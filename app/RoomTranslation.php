<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class RoomTranslation extends Model
{
    use Sluggable;

    protected $fillable = ['name', 'title', 'label', 'description', 'excerpt', 'additional_title', 'additional_description' , 'other_amenities'];


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => ['source' => 'name']
        ];
    }

    public function scopeWithUniqueSlugConstraints(Builder $query, Model $model)
    {
        $locale = $model->locale;
        return $query->where('locale', $locale);
    }
}
