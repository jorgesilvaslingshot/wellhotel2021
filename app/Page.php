<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use Sluggable;

    protected $fillable = ['url', 'title', 'name', 'content', 'masthead_image', 'masthead_big', 'masthead_description', 'seo_title', 'seo_description', 'seo_image', 'locale', 'publish'];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->name = $model->name ?? strip_tags($model->title);
        });
    }

    public function sluggable(): array
    {
        return [
            'url' => ['source' => 'title']
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getMastheadImage()
    {
        return $this->masthead_image;
    }

    public function getMastheadBig()
    {
        return $this->masthead_big;
    }

    public function getMastheadDescription()
    {
        return $this->masthead_description;
    }

    public function getSeoTitle()
    {
        return $this->seo_title;
    }

    public function getSeoDescription()
    {
        return $this->seo_description;
    }

    public function getSeoImage()
    {
        return $this->seo_image;
    }

    public function getView()
    {
        return $this->view;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function getPublish()
    {
        return $this->publish;
    }

    public function findPageByUrlAndLocale($url, $locale)
    {
        $page = Model::where('url', $url)
            ->where('locale', $locale)
            ->where('publish', 1)
            ->orderBy('id', 'desc')
            ->first();

        return $page;
    }

    public function saveOrCreate($data, $id = null)
    {
        return static::updateOrCreate(compact('id'), $data);
    }
}
