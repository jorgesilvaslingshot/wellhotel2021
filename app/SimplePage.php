<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class SimplePage extends Model
{
    use Translatable;

    public $translatedAttributes = ['title', 'content'];
    public $fillable = ['name', 'slug'];
    protected $with = ['translations'];

    public function getTitle()
    {
        return $this->title;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function saveOrCreate($data, $id = null)
    {
        $el = static::updateOrCreate(compact('id'), $data);

        foreach ($data['locales'] as $locale => $values) {
            $translation = $el->translateOrNew($locale);
            $translation->title = $values['title'];
            $translation->content = $values['content'];
        }

        $el->save();
    }

    public function getSimplePageBySlug($slug)
    {
        return static::where('slug', $slug)
            ->first();
    }
}
