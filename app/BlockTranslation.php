<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockTranslation extends Model
{
    protected $fillable = ['title', 'label', 'content'];
    public $timestamps = false;
}
