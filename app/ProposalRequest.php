<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProposalRequest extends Model
{
    public $fillable = [
        'locale', 'name', 'email', 'phone', 'start_time',
        'end_time', 'adults', 'children', 'description', 'event_type','arrival'
    ];

    protected $dates = [
        'arrival',
    ];

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getArrival()
    {
        return $this->arrival;
    }

    public function getStartTime()
    {
        return $this->start_time;
    }

    public function getEndTime()
    {
        return $this->end_time;
    }

    public function getAdults()
    {
        return $this->adults;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getEventType()
    {
        return $this->event_type;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function registerProposalRequest($data, $locale)
    {

        return static::create(array_merge($data, compact('locale')));
    }
}
