<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Space extends Model
{
    use Translatable;

    public $translatedAttributes = ['slug' ,'name', 'subtitle', 'description', 'excerpt', 'url'];
    protected $fillable = ['image', 'thumbnail', 'priority', 'publish', 'space_type_id', 'gallery_id'];
    protected $with = ['translations'];

    public function type() : BelongsTo
    {
        return $this->belongsTo(SpaceType::class, 'space_type_id');
    }

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSubtitle()
    {
        return $this->subtitle;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getExcerpt()
    {
        return $this->excerpt;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getThumbnail()
    {
        if (!empty($this->thumbnail)) {
            return $this->thumbnail;
        }

        return $this->image;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getType()
    {
        return $this->type;
    }

    public function hasGallery()
    {
        return !empty($this->gallery);
    }

    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * @param null $limit
     * @return Collection
     *
     */
    public function getPublishedSpaces($limit = null)
    {
        return static::where('publish', 1)
            ->orderBy('priority', 'desc')
            ->limit($limit)
            ->get();
    }

    // type_id 5: id do tipo espaço GASTRONOMIA
    public function getPublishedAboutSpaces($limit = null)
    {
        return static::where('publish', 1)
            ->orderBy('priority', 'desc')
            ->where('space_type_id', 5)
            ->limit($limit)
            ->get();
    }

    public function getPublishedWelfareSpaces($limit = null)
    {
        // id 4: id do tipo espaço bem estar
        return static::where('publish', 1)
            ->orderBy('priority', 'desc')
            ->where('space_type_id', 4)
            ->limit($limit)
            ->get();
    }

    public static function getPublishedSpaceBySlugAndLocale($slug, $locale)
    {
        return static::where('publish', 1)
            ->whereTranslation('slug', $slug)
            ->whereTranslation('locale', $locale)
            ->first();
    }

    public function setPriorityAttribute($value)
    {
        $this->attributes['priority'] = $value ?: 0;
    }

    public function saveOrCreate($data, $id)
    {
        $el = static::updateOrCreate(compact('id'), $data);

        foreach ($data['languages'] as $locale => $values) {
            if ($values['name']) {
                $translation = $el->translateOrNew($locale);
                $translation->name = $values['name'];
                $translation->description = $values['description'];
                $translation->excerpt = $values['excerpt'];
                $translation->url = $values['url'];
            }
        }

        $el->save();
    }
}
