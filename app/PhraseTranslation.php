<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhraseTranslation extends Model
{
    protected $fillable = ['content'];
    public $timestamps = false;
}
