<?php
/**
 * Created by PhpStorm.
 * User: mauropinto
 * Date: 19/10/2017
 * Time: 10:42
 */

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class AmenityTranslation extends Model
{
    use Sluggable;

    protected $fillable = ['name'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => ['source' => 'name']
        ];
    }

    public function scopeWithUniqueSlugConstraints(Builder $query, Model $model)
    {
        $locale = $model->locale;
        return $query->where('locale', $locale);
    }
}
