<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property AmenityCategory $category
 * @property Room[] $rooms
 * @property int amenity_category_id
 */
class Amenity extends Model
{
    use Translatable;

    protected $fillable = ['icon', 'is_main', 'priority', 'publish'];
    public $translatedAttributes = ['name'];
    protected $with = ['translations'];

    public function rooms(): BelongsToMany
    {
        return $this->belongsToMany(Room::class, 'room_amenities', 'room_id');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function getPublishedMainAmenities($limit = null)
    {
        return static::where('publish', 1)
            ->where('is_main', 1)
            ->orderBy('priority', 'desc')
            ->orderBy('id', 'asc')
            ->limit($limit)
            ->get();
    }

    public function setPriorityAttribute($value)
    {
        $this->attributes['priority'] = $value ?: 0;
    }

    public function saveOrCreate($data, $id = null)
    {
        $el = static::find($id) ?: new static;
        $el->icon = $data['icon'];
        $el->is_main = $data['is_main'];
        $el->priority = $data['priority'];
        $el->publish = $data['publish'];

        foreach ($data['languages'] as $locale => $values) {
            if ($values['name']) {
                $translation = $el->translateOrNew($locale);
                $translation->name = $values['name'];
            }
        }

        $el->save();
    }
}
