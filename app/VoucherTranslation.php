<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;


class VoucherTranslation extends Model
{
    use Sluggable;

    protected $fillable = ['name', 'subtitle', 'label', 'description', 'excerpt', 'includes'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => ['source' => 'name']
        ];
    }

    public function getPublishedVouchers($locale)
    {
        return static::select('voucher_id', 'name', 'label')->where('locale', $locale)
            ->get();
    }

    public function scopeWithUniqueSlugConstraints(Builder $query, Model $model)
    {
        $locale = $model->locale;
        return $query->where('locale', $locale);
    }
}
