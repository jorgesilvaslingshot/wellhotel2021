<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuestServiceTranslation extends Model
{
    protected $fillable = ['title', 'description'];
    public $timestamps = false;
}
