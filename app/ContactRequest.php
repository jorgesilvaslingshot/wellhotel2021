<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactRequest extends Model
{
    public $fillable = ['locale', 'name', 'email', 'message'];

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function registerContactRequest($name, $email, $message, $locale)
    {
        return static::create(compact('name', 'email', 'message', 'locale'));
    }
}
