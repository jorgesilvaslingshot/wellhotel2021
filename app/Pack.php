<?php

namespace App;

use Carbon\Carbon;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Pack extends Model
{
    use Translatable;

    public $translatedAttributes = ['slug', 'name', 'label', 'subtitle', 'description', 'includes'];
    protected $fillable = ['current_discount', 'current_price', 'cover_image', 'cover_image_2', 'publish'];
    protected $with = ['translations'];

    public function experiences(): BelongsToMany
    {
        return $this->belongsToMany(Experience::class, 'pack_experiences', 'pack_id', 'experience_id');
    }

    public function price_intervals()
    {
        return $this->hasMany(PackPriceInterval::class);
    }

    public function prices()
    {
        return $this->hasManyThrough(PackPrice::class, PackPriceInterval::class, 'pack_id', 'pack_price_interval_id');
    }

    public function prices_now()
    {
        return $this->prices()->where('pack_prices.date', '=', Carbon::now()->format('Y-m-d'));
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getSubtitle()
    {
        return $this->subtitle;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getIncludes()
    {
        return $this->includes;
    }

    public function hasDiscount()
    {
        return $this->getDiscount() > 0;
    }

    public function getDiscount()
    {
        return setting('pack_discount', 0);
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPriceNowWithDiscount()
    {
        if ($this->hasDiscount()) {
            return round($this->getPriceNow() - $this->getPriceNow() * ($this->getDiscount() / 100));
        }

        return $this->getPriceNow();
    }

    public function getPriceNow()
    {
        if ($this->prices_now->count() > 0) {
            return $this->prices_now->first()->getPrice();
        }

        return $this->price;
    }

    public function getMaxGuestsNumeric()
    {
        return $this->max_guests_numeric;
    }

    public function getImage()
    {
        return $this->cover_image;
    }

    public function getThumbnail()
    {
        if (!empty($this->cover_image_2)) {
            return $this->cover_image_2;
        }

        return $this->cover_image_1;
    }

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    public function hasGallery()
    {
        return !empty($this->gallery);
    }

    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * @param null $limit
     * @return Collection
     *
     */
    public function getPublishedPacks($limit = null)
    {
        return static::where('publish', 1)
            ->orderBy('priority', 'desc')
            ->limit($limit)
            ->get();
    }

    public static function getPublishedPackBySlugAndLocale($slug, $locale)
    {
        return static::where('publish', 1)
            ->whereTranslation('slug', $slug)
            ->whereTranslation('locale', $locale)
            ->first();
    }

    public function setPriorityAttribute($value)
    {
        $this->attributes['priority'] = $value ?: 0;
    }

    public function getPriceIntervals()
    {
        return $this->price_intervals;
    }

    public function getOrderedPriceIntervals()
    {
        return $this->getPriceIntervals()->sortBy(function ($priceInterval) {
            return $priceInterval->getStartDate();
        });
    }

    public function saveOrCreate($data, $id)
    {
        $el = static::find($id) ?: new static;
        $el->price = $data['price'];
        $el->max_guests_numeric = $data['max_guests_numeric'];
        $el->cover_image = $data['cover_image'];
        $el->cover_image_2 = $data['cover_image_2'];
        $el->gallery_id = $data['gallery_id'];
        $el->priority = $data['priority'];
        $el->publish = $data['publish'];

        foreach ($data['languages'] as $locale => $values) {
            if ($values['name']) {
                $translation = $el->translateOrNew($locale);
                $translation->name = $values['name'];
                $translation->label = $values['label'];
                $translation->subtitle = $values['subtitle'];
                $translation->description = $values['description'];
                $translation->includes = $values['includes'];
            }
        }

        $el->save();
        $el->experiences()->sync($data['experiences'] ?? null);
    }

    public function getPackPriceByIdAndDates($id, $from, $to)
    {
        $query = \DB::table('packs AS n')
            ->selectRaw('n.id,coalesce(sum(s.price), 0) + n.price * (datediff(?, ?) - count(s.price) + 1) as price', [$to, $from])
            ->leftJoin('pack_price_intervals AS i', 'i.pack_id', '=', 'n.id')
            ->leftJoin('pack_prices AS s', function ($join) use ($from, $to) {
                $join->on('i.id', '=', 's.pack_price_interval_id')
                    ->whereRaw('s.date between ? and ?', [$from, $to]);
            })
            ->groupBy(['n.id','n.price'])
            ->where('n.id', '=', $id)
            ->first();

        $current_price = data_get($query, 'price', null);

        if (is_null($current_price)) {
            return null;
        }

        return round($current_price - ($current_price * ($this->getDiscount() / 100)));
    }
}
