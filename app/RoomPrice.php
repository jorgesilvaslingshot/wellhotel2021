<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomPrice extends Model
{
    protected $fillable = ['date', 'price', 'room_price_interval_id', 'priority'];

    public $timestamps = false;

    public $incrementing = false;

    protected $primaryKey = ['date', 'room_price_interval_id'];

    public function room_price_interval()
    {
        return $this->belongsTo(RoomPriceInterval::class);
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getRoomPriceInterval()
    {
        return $this->room_price_interval;
    }
}
