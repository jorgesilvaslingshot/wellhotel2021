<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackPriceInterval extends Model
{
    protected $fillable = ['name', 'pack_id'];

    public function pack()
    {
        return $this->belongsTo(Pack::class);
    }

    public function pack_prices()
    {
        return $this->hasMany(PackPrice::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function (PackPriceInterval $packPriceInterval) {
            $packPriceInterval->pack_prices()->delete();
        });

        static::saving(function (PackPriceInterval $packPriceInterval) {
            $packPriceInterval->pack_prices()->delete();
        });
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getStartDate()
    {
        return $this->pack_prices->min('date');
    }

    public function getEndDate()
    {
        return $this->pack_prices->max('date');
    }

    public function getPrice()
    {
        return optional($this->pack_prices->first())->getPrice();
    }

    public function getPack()
    {
        return $this->pack;
    }

    public function saveOrCreate($data, $id)
    {
        $instance = static::updateOrCreate(compact('id'), $data);

        $price = array_get($data, 'price');
        $priority = array_get($data, 'priority');

        foreach (array_get($data, 'dates', []) as $date) {
            $instance->pack_prices()->create(compact('date', 'price', 'priority'));
        }

        return $instance;
    }
}
