<?php

use Illuminate\Database\Seeder;

class Languages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = DB::table('languages');
        $table->insert([
            'name' => 'Português',
            'slug' => 'pt',
        ]);
        $table->insert([
            'name' => 'English',
            'slug' => 'en',
        ]);
    }
}
