<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->index();
            $table->string('image')->nullable();
            $table->string('thumbnail')->nullable();
            $table->integer('gallery_id')->index()->nullable();
            $table->timestamps();
        });

        Schema::create('block_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('block_id')->unsigned();
            $table->text('title');
            $table->text('label')->nullable();
            $table->longText('description')->nullable();
            $table->string('locale')->index();

            $table->unique(['block_id','locale']);
            $table->foreign('block_id')->references('id')->on('blocks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_translations');
        Schema::dropIfExists('blocks');
    }
}
