<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guest_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('priority')->nullable();
            $table->boolean('publish');
            $table->timestamps();
        });

        Schema::create('guest_service_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('guest_service_id')->unsigned();

            $table->text('title');
            $table->longText('description');

            $table->string('locale')->index();

            $table->unique(['guest_service_id','locale']);
            $table->foreign('guest_service_id')->references('id')->on('guest_services')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guest_service_translations');
        Schema::dropIfExists('guest_services');
    }
}
