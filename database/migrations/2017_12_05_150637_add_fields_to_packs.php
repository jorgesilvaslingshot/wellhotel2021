<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToPacks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packs', function (Blueprint $table) {
            $table->integer('pack_type_id')->after('cover_image_2')->index();
            $table->boolean('publish')->after('cover_image_2');
            $table->integer('priority')->after('cover_image_2')->nullable();
        });

        Schema::table('pack_translations', function (Blueprint $table) {
            $table->string('subtitle')->after('name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pack_translations', function (Blueprint $table) {
            $table->dropColumn('subtitle');
        });

        Schema::table('packs', function (Blueprint $table) {
            $table->dropColumn('pack_type_id', 'priority', 'publish');
        });
    }
}
