<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBottomSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bottom_slides', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->text('label');
            $table->string('action_title')->nullable();
            $table->string('action_link')->nullable();
            $table->string('image');
            $table->string('locale')->index();
            $table->integer('priority')->nullable();
            $table->boolean('publish');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bottom_slides');
    }
}
