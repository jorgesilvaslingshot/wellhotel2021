<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pack_types', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('pack_type_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pack_type_id')->unsigned();
            $table->string('slug')->index();
            $table->text('name');
            $table->string('locale')->index();

            $table->unique(['pack_type_id','locale']);
            $table->foreign('pack_type_id')->references('id')->on('pack_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pack_type_translations');
        Schema::dropIfExists('pack_types');
    }
}
