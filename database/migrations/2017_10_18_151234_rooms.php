<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('current_discount')->nullable();
            $table->string('current_price')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('cover_image_2')->nullable();
            $table->unsignedInteger('gallery_id')->nullable();
            $table->foreign('gallery_id')->references('id')->on('galleries')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
        \Schema::create('room_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('room_id');
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->string('name');
            $table->string('slug');
            $table->text('description');
            $table->text('room_size')->nullable();
            $table->text('max_guests')->nullable();
            $table->text('bed_size')->nullable();
            $table->char('locale', 2);
            $table->foreign('locale')->references('slug')->on('languages')->onDelete('cascade');
            $table->timestamps();
        });
        \Schema::create('room_amenities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('room_id');
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->unsignedInteger('amenity_id');
            $table->foreign('amenity_id')->references('id')->on('amenities')->onDelete('cascade');
            $table->unique(['room_id', 'amenity_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('room_amenities');
        \Schema::drop('room_translations');
        \Schema::drop('rooms');
    }
}
