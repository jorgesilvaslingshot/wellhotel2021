<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Experiences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('current_discount')->nullable();
            $table->string('current_price')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('cover_image_2')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        \Schema::create('experience_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('experience_id');
            $table->foreign('experience_id')->references('id')->on('experiences')->onDelete('cascade');
            $table->char('locale', 2);
            $table->foreign('locale')->references('slug')->on('languages')->onDelete('cascade');
            $table->string('name');
            $table->string('slug');
            $table->text('description')->nullable();
            $table->text('includes')->nullable();
            $table->timestamps();
        });
        \Schema::create('pack_experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pack_id');
            $table->foreign('pack_id')->references('id')->on('packs')->onDelete('cascade');
            $table->unsignedInteger('experience_id');
            $table->foreign('experience_id')->references('id')->on('experiences')->onDelete('cascade');
            $table->unique(['pack_id', 'experience_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('pack_experiences');
        \Schema::drop('experience_translations');
        \Schema::drop('experiences');
    }
}
