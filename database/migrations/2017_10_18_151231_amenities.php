<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Amenities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('amenity_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });
        \Schema::create('amenity_category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('amenity_category_id');
            $table->foreign('amenity_category_id')->references('id')->on('amenity_categories')->onDelete('cascade');
            $table->char('locale', 2);
            $table->foreign('locale')->references('slug')->on('languages')->onDelete('cascade');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });
        \Schema::create('amenities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('amenity_category_id');
            $table->foreign('amenity_category_id')->references('id')->on('amenity_categories')->onDelete('cascade');
            $table->string('icon')->nullable();
            $table->timestamps();
        });
        \Schema::create('amenity_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('amenity_id');
            $table->foreign('amenity_id')->references('id')->on('amenities')->onDelete('cascade');
            $table->char('locale', 2);
            $table->foreign('locale')->references('slug')->on('languages')->onDelete('cascade');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('amenity_translations');
        \Schema::drop('amenities');
        \Schema::drop('amenity_category_translations');
        \Schema::drop('amenity_categories');
    }
}
