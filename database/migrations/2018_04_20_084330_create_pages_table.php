<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');

            $table->string('url')->index();
            $table->text('title');
            $table->string('name')->nullable();
            $table->longText('content')->nullable();

            $table->string('masthead_image', 255)->nullable();
            $table->text('masthead_description')->nullable();

            $table->string('seo_title', 70)->nullable();
            $table->string('seo_description', 156)->nullable();
            $table->string('seo_image', 255)->nullable();

            $table->string('view')->nullable();
            $table->string('locale', 2);
            $table->boolean('publish');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
