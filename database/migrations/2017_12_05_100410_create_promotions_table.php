<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->string('image');
            $table->string('link')->nullable();
            $table->boolean('publish');
            $table->timestamps();
        });

        Schema::create('promotion_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('promotion_id')->unsigned();
            $table->text('title');
            $table->text('label')->nullable();
            $table->string('locale')->index();

            $table->unique(['promotion_id','locale']);
            $table->foreign('promotion_id')->references('id')->on('promotions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion_translations');
        Schema::dropIfExists('promotions');
    }
}
