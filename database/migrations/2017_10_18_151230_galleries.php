<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Galleries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('galleries', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
        });
        \Schema::create('gallery_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('gallery_id');
            $table->foreign('gallery_id')->references('id')->on('galleries')->onDelete('cascade');
            $table->char('locale', 2);
            $table->foreign('locale')->references('slug')->on('languages')->onDelete('cascade');
            $table->string('name');
            $table->timestamps();
        });
        \Schema::create('gallery_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('gallery_id');
            $table->foreign('gallery_id')->references('id')->on('galleries')->onDelete('cascade');
            $table->integer('priority');
            $table->string('image');
            $table->timestamps();
        });
        \Schema::create('gallery_item_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('gallery_item_id');
            $table->foreign('gallery_item_id')->references('id')->on('gallery_items')->onDelete('cascade');
            $table->char('locale', 2);
            $table->foreign('locale')->references('slug')->on('languages')->onDelete('cascade');
            $table->string('label');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('gallery_item_translations');
        \Schema::drop('gallery_items');
        \Schema::drop('gallery_translations');
        \Schema::drop('galleries');
    }
}
