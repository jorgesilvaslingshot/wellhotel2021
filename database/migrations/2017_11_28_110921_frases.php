<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Frases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('frases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('slug');
            $table->unique('slug');
            $table->boolean('allow_html');
        });
        \Schema::create('frases_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('frase_id');
            $table->foreign('frase_id')->references('id')->on('frases')->onDelete('cascade');
            $table->char('locale', 2);
            $table->foreign('locale')->references('slug')->on('languages')->onDelete('cascade');
            $table->text('content')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('frases_translations');
        \Schema::dropIfExists('frases');
    }
}
