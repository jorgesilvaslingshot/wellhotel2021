<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LanguagesAndPhrases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createLanguages();
        $this->createPhrases();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('phrases_translations');
        \Schema::drop('phrases');
        \Schema::drop('languages');
    }

    private function createLanguages()
    {
        \Schema::create('languages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->char('slug', 2);
            $table->unique('slug');
        });
    }

    private function createPhrases()
    {
        \Schema::create('phrases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->unique('slug');
            $table->boolean('allow_html');
        });
        \Schema::create('phrase_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('phrase_id');
            $table->foreign('phrase_id')->references('id')->on('phrases')->onDelete('cascade');
            $table->char('locale', 2);
            $table->foreign('locale')->references('slug')->on('languages')->onDelete('cascade');
            $table->text('content');
        });
    }
}
