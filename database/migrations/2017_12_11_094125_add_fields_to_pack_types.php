<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToPackTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pack_types', function (Blueprint $table) {
            $table->integer('priority')->nullable()->after('id');
            $table->boolean('publish')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pack_types', function (Blueprint $table) {
            $table->dropColumn('priority', 'publish');
        });
    }
}
