<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AvailabilityRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('availability_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->char('locale', 2);
            $table->foreign('locale')->references('slug')->on('languages')->onDelete('cascade');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->date('arrival');
            $table->date('departure');
            $table->unsignedTinyInteger('adults')->default(0);
            $table->unsignedTinyInteger('children')->default(0);
            $table->string('product_name');
            $table->string('product_type');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('availability_requests');
    }
}
