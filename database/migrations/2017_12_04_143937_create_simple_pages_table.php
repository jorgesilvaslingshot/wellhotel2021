<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimplePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simple_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->index();
            $table->string('name');
            $table->boolean('routable')->nullable()->default(true);
            $table->timestamps();
        });

        Schema::create('simple_page_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('simple_page_id')->unsigned();
            $table->text('title');
            $table->longText('content')->nullable();
            $table->string('locale')->index();

            $table->unique(['simple_page_id','locale']);
            $table->foreign('simple_page_id')->references('id')->on('simple_pages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simple_page_translations');
        Schema::dropIfExists('simple_pages');
    }
}
