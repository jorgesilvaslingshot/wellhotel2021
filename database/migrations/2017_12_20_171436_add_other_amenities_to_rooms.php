<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOtherAmenitiesToRooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('room_translations', function (Blueprint $table) {
            $table->longText('other_amenities')->nullable()->after('bed_size');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('room_translations', function (Blueprint $table) {
            $table->dropColumn('other_amenities');
        });
    }
}
