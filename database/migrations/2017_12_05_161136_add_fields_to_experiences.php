<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToExperiences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('experiences', function (Blueprint $table) {
            $table->integer('experience_type_id')->after('cover_image_2')->index();
            $table->boolean('publish')->after('cover_image_2');
            $table->integer('priority')->after('cover_image_2')->nullable();
        });

        Schema::table('experience_translations', function (Blueprint $table) {
            $table->string('subtitle')->after('name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('experience_translations', function (Blueprint $table) {
            $table->dropColumn('subtitle');
        });

        Schema::table('experiences', function (Blueprint $table) {
            $table->dropColumn('experience_type_id', 'priority', 'publish');
        });
    }
}
