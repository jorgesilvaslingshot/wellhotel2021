<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperienceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experience_types', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('experience_type_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('experience_type_id')->unsigned();
            $table->string('slug')->index();
            $table->text('name');
            $table->string('locale')->index();

            $table->unique(['experience_type_id','locale']);
            $table->foreign('experience_type_id')->references('id')->on('experience_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experience_type_translations');
        Schema::dropIfExists('experience_types');
    }
}
