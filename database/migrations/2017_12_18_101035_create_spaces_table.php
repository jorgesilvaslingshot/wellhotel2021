<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spaces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('thumbnail')->nullable();
            $table->integer('priority')->nullable();
            $table->boolean('publish');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('space_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('space_id')->unsigned();
            $table->string('slug')->index();
            $table->text('name');
            $table->string('label')->nullable();
            $table->longText('description')->nullable();
            $table->text('excerpt')->nullable();

            $table->string('locale')->index();

            $table->unique(['space_id','locale']);
            $table->foreign('space_id')->references('id')->on('spaces')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('space_translations');
        Schema::dropIfExists('spaces');
    }
}
