<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpaceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('space_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('priority')->nullable();
            $table->boolean('publish');
            $table->timestamps();
        });

        Schema::create('space_type_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('space_type_id')->unsigned();
            $table->string('slug')->index();
            $table->text('name');
            $table->string('locale')->index();

            $table->unique(['space_type_id','locale']);
            $table->foreign('space_type_id')->references('id')->on('space_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('space_type_translations');
        Schema::dropIfExists('space_types');
    }
}
