<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToAmenityCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amenity_categories', function (Blueprint $table) {
            $table->integer('priority')->nullable()->after('id');
            $table->boolean('publish')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amenity_categories', function (Blueprint $table) {
            $table->dropColumn('priority', 'publish');
        });
    }
}
