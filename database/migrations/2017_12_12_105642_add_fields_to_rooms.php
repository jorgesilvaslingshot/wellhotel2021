<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToRooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rooms', function (Blueprint $table) {
            $table->string('masthead_image')->after('current_price');
            $table->integer('priority')->nullable()->after('cover_image_2');
            $table->boolean('publish')->after('priority');
        });

        Schema::table('room_translations', function (Blueprint $table) {
            $table->text('label')->nullable()->after('name');
            $table->text('title')->nullable()->after('label');
            $table->text('additional_title')->nullable()->after('updated_at');
            $table->text('excerpt')->nullable()->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rooms', function (Blueprint $table) {
            $table->dropColumn('masthead_image', 'priority', 'publish');
        });

        Schema::table('room_translations', function (Blueprint $table) {
            $table->dropColumn('label', 'title', 'additional_title', 'excerpt');
        });
    }
}
