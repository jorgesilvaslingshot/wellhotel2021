<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToRoomTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('room_types', function (Blueprint $table) {
            $table->boolean('publish')->after('id');
            $table->integer('priority')->nullable()->after('id');
        });

        Schema::table('room_type_translations', function (Blueprint $table) {
            $table->string('slug')->index()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('room_type_translations', function (Blueprint $table) {
            $table->dropColumn('slug');
        });

        Schema::table('room_types', function (Blueprint $table) {
            $table->dropColumn('priority', 'publish');
        });
    }
}
