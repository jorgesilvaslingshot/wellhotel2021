<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMastheadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mastheads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->index();
            $table->string('image');
            $table->timestamps();
        });

        Schema::create('masthead_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('masthead_id')->unsigned();

            $table->text('title');
            $table->longText('description')->nullable();

            $table->string('locale')->index();

            $table->unique(['masthead_id','locale']);
            $table->foreign('masthead_id')->references('id')->on('mastheads')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('masthead_translations');
        Schema::dropIfExists('mastheads');
    }
}
