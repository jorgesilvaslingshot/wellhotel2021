# Slingshot Admin #

Slingshot Admin is an administrative package for Laravel 5.3.

### Install ###

Add the package in your composer.json by executing the command:

```
#!shell

composer require slingshot/admin
```

### Providers and Facades ###

Next, add the following service providers and facades to config/app.php:

```
#!php

'providers' => [
    anlutro\LaravelSettings\ServiceProvider::class
    Slingshot\Admin\Providers\AdminServiceProvider::class
],

'aliases' = [
    'Form'  => Collective\Html\FormFacade::class,
    'HTML'  => Collective\Html\HtmlFacade::class,
]
```

### Run install command ###

Finally run the following command:

```
#!shell

php artisan admin:install
```

