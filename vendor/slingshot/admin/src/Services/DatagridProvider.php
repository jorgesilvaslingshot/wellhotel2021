<?php
/**
 * Created by PhpStorm.
 * User: rubenestevao
 * Date: 6/12/15
 * Time: 9:52 AM
 */

namespace Slingshot\Admin\Services;


interface DatagridProvider
{
    public function sortBy($column, $direction);

    public function searchBy($column, $search);

    public function getTotal();

    public function paginate($offset, $per_page);

    public function getItems();
}
