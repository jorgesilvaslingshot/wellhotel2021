<?php

namespace Slingshot\Admin\Services;

use Closure;
use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Routing\Router;


class Menu
{

    protected $menus = [];

    protected $links = [];

    protected $currentMenu;

    protected $defaults = [
        'icon' => 'fa fa-circle-o',
        'label' => '',
        'labelClass' => 'label-default',
        'title' => ''
    ];

    protected $linkDefaults = [
        'namespace' => '',
        'create' => true,
        'edit' => true,
        'delete' => true,
        'autoRoutes' => true
    ];

    public function __construct(Config $config, Router $router)
    {
        $this->router = $router;
        $menus = $config->get('admin.menu', []);
        $this->menus = $this->setDefaults($menus);
    }

    protected function setDefaults($menus)
    {
        $formatMenus = [];

        foreach ($menus as $menu) {
            $menu['isHeader'] = $this->isHeader($menu);
            $menu['isLink'] = $this->isLink($menu);
            $menu['isDropdown'] = $this->isDropdown($menu);
            $menu['isOnlyRoutes'] = $this->isOnlyRoutes($menu);

            if (isset($menu['label']) && is_object($menu['label']) && ($menu['label'] instanceof Closure)) {
                $menu['label'] = $menu['label']();
            }

            if ($menu['isLink']) {
                $menu['routeAction'] = '\\' . $menu['controller'];
                $menu['active'] = false;

                $menu = array_merge($this->defaults, $this->linkDefaults, $menu);

                array_push($this->links, $menu);
            } elseif ($menu['isDropdown']) {
                $menu = array_merge($this->defaults, $menu);

                $menu['children'] = $this->setDefaults($menu['children']);
            }

            if (($menu['isHeader'] || $menu['isLink'] || $menu['isDropdown']) && !$menu['isOnlyRoutes']) {
                array_push($formatMenus, $menu);
            }
        }

        return $formatMenus;
    }

    protected function setState($menus, &$parentActive = false)
    {
        $stateMenus = [];

        foreach ($menus as &$menu) {
            if ($menu['isLink']) {
                if ($menu['key'] == $this->getCurrentKey()) {
                    $menu['active'] = true;
                    $parentActive = true;
                }
            } elseif ($menu['isDropdown']) {
                $menu['children'] = $this->setState($menu['children'], $menu['active']);
            }

            array_push($stateMenus, $menu);
        }

        return $stateMenus;
    }

    protected function isHeader($menu)
    {
        return !empty($menu['header']);
    }

    protected function isLink($menu)
    {
        return !empty($menu['key']) && empty($menu['children']) && !empty($menu['controller']);
    }

    protected function isDropdown($menu)
    {
        return !empty($menu['children']);
    }

    protected function isOnlyRoutes($menu)
    {
        return isset($menu['onlyRoutes']) && $menu['onlyRoutes'] == true;
    }

    protected function getCurrentKey()
    {
        $action = $this->router->getCurrentRoute()->getAction();

        if (!isset($action['menu'])) {
            return null;
        }

        return $action['menu'];
    }

    public function getMenus()
    {
        return $this->setState($this->menus);
    }

    public function getLinks()
    {
        return $this->links;
    }

    public function getCurrentMenu()
    {
        if (!isset($this->currentMenu)) {
            foreach ($this->links as $link) {
                if ($link['key'] == $this->getCurrentKey()) {
                    return $this->currentMenu = $link;
                }
            }
        }

        return $this->currentMenu;

    }
}
