<?php

namespace Slingshot\Admin\Services;

use Illuminate\Database\Eloquent\Builder as Builder;

class ArrayDatagridProvider implements DatagridProvider
{
    const SORT_ASC = 'asc';

    protected $array;

    public function __construct(array $array)
    {
        $this->array = $array;

        return $this;
    }

    public function sortBy($column, $direction)
    {
        usort($this->array, function ($a, $b) use ($column, $direction) {
            if ($direction == self::SORT_ASC) {
                return strcmp($a[$column], $b[$column]);
            }

            return strcmp($b[$column], $a[$column]);
        });

        return $this;
    }

    public function searchBy($column, $search)
    {
        $this->array = array_filter($this->array, function ($array) use ($column, $search) {

            return strpos(strtolower($array[$column]), strtolower($search)) !== false;
        });

        return $this;
    }

    public function getTotal()
    {
        return count($this->array);
    }

    public function paginate($offset, $per_page)
    {
        $this->array = array_slice($this->array, $offset, $per_page);

        return $this;
    }

    public function getItems()
    {
        return $this->array;
    }
}
