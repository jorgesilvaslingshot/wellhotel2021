<?php

namespace Slingshot\Admin\Services;

use Illuminate\Database\Eloquent\Builder as Builder;

class EloquentDatagridProvider implements DatagridProvider
{
    protected $query;

    protected $relation_fields = [];

    public function __construct(Builder $query)
    {
        $this->query = $query;

        return $this;
    }

    public function sortBy($field, $direction)
    {
        $field = $this->renameField($field);

        $this->query = $this->query->orderBy($field, $direction);

        return $this;
    }

    public function searchBy($field, $search)
    {
        $field = $this->renameField($field);

        $this->query = $this->query->where($field, 'like', '%' . $search . '%');

        return $this;
    }

    public function getTotal()
    {
        return $this->query->count();
    }

    public function paginate($offset, $per_page)
    {
        $this->query = $this->query->skip($offset)->take($per_page);

        return $this;
    }

    public function getItems()
    {
        return $this->query->select($this->getTable() . '.*')->get();
    }

    protected function renameField($field)
    {
        $split = $this->splitField($field);

        if ($split) {
            $relation = $this->query->getRelation($split[0]);
            $table = $relation->getRelated()->getTable();

            if (!in_array($field, $this->relation_fields)) {
                $one = $relation->getQualifiedForeignKey();
                $two = $relation->getQualifiedOwnerKeyName();

                $this->query = $this->query->leftJoin($table, $one, '=', $two);

                array_push($this->relation_fields, $field);
            }

            return sprintf('%s.%s', $table, $split[1]);
        }

        return sprintf('%s.%s', $this->getTable(), $field);
    }

    protected function splitField($field)
    {
        $split = explode('.', $field);

        if (count($split) > 1) {
            return $split;
        }

        return false;
    }

    protected function getTable()
    {
        return $this->query->getModel()->getTable();
    }
}
