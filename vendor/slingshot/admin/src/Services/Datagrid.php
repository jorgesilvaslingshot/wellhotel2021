<?php

namespace Slingshot\Admin\Services;

class Datagrid
{
    const SORT_ASC = 'asc';

    const SORT_DESC = 'desc';

    protected $provider;

    protected $request;

    protected $columns = [];

    protected $sort_by;

    protected $default_sort_by = null;

    protected $sort;

    protected $default_sort = 'desc';

    protected $search_by;

    protected $default_search_by = null;

    protected $search;

    protected $page;

    protected $per_page = 20;

    protected $total = 0;

    protected $sort_columns = [];

    protected $search_columns = [];

    public function __construct(DatagridProvider $provider = null)
    {
        $this->provider = $provider;
    }

    public function source(DatagridProvider $provider)
    {
        return new static($provider);
    }

    public function addColumn($column, $title = '', $search = true, $sort = true)
    {
        $this->columns[$column] = $title;

        if ($search) {
            array_push($this->search_columns, $column);

            if (empty($this->default_search_by)) {
                $this->default_search_by = $column;
            }
        }

        if ($sort) {
            array_push($this->sort_columns, $column);

            if (empty($this->default_sort_by)) {
                $this->default_sort_by = $column;
            }
        }

        return $this;
    }

    public function setDefaultSort($direction)
    {
        $direction = strtolower($direction);
        if (in_array($direction, [self::SORT_ASC,self::SORT_DESC])) {
            $this->default_sort = $direction;
        } else {
            $this->default_sort = 'desc';
        }

        return $this;
    }

    public function setDefaultSortBy($column)
    {
        $this->default_sort_by = $column;

        return $this;
    }

    public function setDefaultSearchBy($column)
    {
        $this->default_search_by = $column;

        return $this;
    }

    public function setPerPage($per_page)
    {
        $this->per_page = (int)$per_page;

        return $this;
    }

    public function get()
    {
        $items = $this->build();
        $columns = $this->columns;
        $total = $this->total;
        $sort_by = $this->sort_by;
        $sort = $this->sort;
        $search_by = $this->search_by;
        $search = $this->search;
        $per_page = $this->per_page;
        $page = $this->page;
        $search_columns = $this->search_columns;
        $sort_columns = $this->sort_columns;

        return compact('columns', 'items', 'total', 'sort_by', 'sort', 'search_by', 'search', 'per_page', 'page',
            'search_columns', 'sort_columns');
    }

    protected function setRequestInfo()
    {
        $this->sort_by = static::getParam('sort_by', $this->default_sort_by);
        $this->sort = static::getParam('sort', $this->default_sort);
        $this->search_by = static::getParam('search_by', $this->default_search_by);
        $this->search = static::getParam('search');
        $this->page = static::getParam('page');

        if (!in_array($this->sort_by, $this->sort_columns)) {
            $this->sort_by = $this->default_sort_by;
        }

        if (!in_array($this->search_by, $this->search_columns)) {
            $this->search_by = $this->default_search_by;
        }

        if (!in_array($this->sort, [self::SORT_ASC,self::SORT_DESC])) {
            $this->sort = $this->default_sort;
        }
    }

    protected static function getParam($key, $default = null)
    {
        return isset($_GET[$key]) ? $_GET[$key] : $default;
    }

    protected function getOffset()
    {
        $offset = $this->per_page * ($this->page - 1);

        if ($offset >= $this->total) {
            $offset = round($this->total / $this->per_page) - 1;
        }

        return $offset;
    }

    protected function build()
    {
        $this->setRequestInfo();

        if (!empty($this->search_by) && !empty($this->search)) {
            $this->provider = $this->provider->searchBy($this->search_by, $this->search);
        }

        $this->total = $this->provider->getTotal();

        if (!empty($this->sort_by) && !empty($this->sort)) {
            $this->provider = $this->provider->sortBy($this->sort_by, $this->sort);
        }

        $offset = $this->getOffset();

        return $this->provider
            ->paginate($offset, $this->per_page)
            ->getItems();
    }
}
