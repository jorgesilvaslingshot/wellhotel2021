<?php

namespace Slingshot\Admin\Services;

class FormBuilder extends \Collective\Html\FormBuilder
{
    protected function fieldWrapper($name, $label, $element, $extra = [])
    {
        if (empty($extra['sizes'])) {
            $extra['sizes'] = 'col-sm-10 col-md-8';
        }

        $out = '<div class="form-group">';
        $out .= $this->fieldLabel($name, $label);
        $out .= '<div class="' . $extra['sizes'] . '">';
        $out .= $element;
        $out .= '</div>';
        if (!empty($extra['help'])) {
            $out .= '<div class="pull-left">';
            $out .= '<div class="form-control-static">';
            $out .= $extra['help'];
            $out .= '</div>';
            $out .= '</div>';
        }
        $out .= '</div>';
        return $out;
    }

    protected function fieldLabel($name, $label)
    {
        if (is_null($label)) {
            return '';
        }

        $name = str_replace('[]', '', $name);

        $out = '<label for="' . $name . '" class="col-sm-2 control-label">';
        $out .= $label . '</label>';

        return $out;
    }

    protected function fieldAttributes($name, $attributes = [])
    {
        $name = str_replace('[]', '', $name);

        return array_merge(['class' => 'form-control', 'id' => $name], $attributes);
    }

    public function boOpen($data = [], $options = [])
    {
        $options = array_merge(['class' => 'form-horizontal'], $options);

        return $this->model($data, $options);
    }

    public function boClose()
    {
        return $this->close();
    }

    /**
     * Input of $type
     *
     * @param string $name input name
     * @param string $type input type
     * @param null $label input label
     * @param null $value input value
     * @param array $attributes input attributes
     * @param array $extra
     * @return string
     */
    public function boInput($name, $type, $label = null, $value = null, $attributes = [], $extra = [])
    {
        if (empty($extra['sizes'])) {
            $extra['sizes'] = 'col-sm-6 col-md-4';
        }

        $attributes = $this->fieldAttributes($name, $attributes);

        $element = $this->input($type, $name, $value, $attributes);

        return $this->fieldWrapper($name, $label, $element, $extra);
    }

    public function boText($name, $label = null, $value = null, $attributes = [], $extra = [])
    {
        return $this->boInput($name, 'text', $label, $value, $attributes, $extra);
    }

    public function boPassword($name, $label = null, $attributes = [], $extra = [])
    {
        return $this->boInput($name, 'password', $label, null, $attributes, $extra);
    }

    public function boTags($name, $label = null, $value = null, $attributes = [], $extra = [])
    {
        $attributes[] = 'data-plugin-selectize-tags';

        return $this->boText($name, $label, $value, $attributes, $extra);
    }

    public function boColor($name, $label = null, $value = null, $attributes = [], $extra = [])
    {
        $attributes[] = 'data-plugin-mini-colors';

        return $this->boText($name, $label, $value, $attributes, $extra);
    }

    public function boDate($name, $label = null, $value = null, $attributes = [], $extra = [])
    {
        $attributes[] = 'data-plugin-datepicker';

        return $this->boText($name, $label, $value, $attributes, $extra);
    }

    public function boTextarea($name, $label = null, $value = null, $attributes = [])
    {
        $attributes = $this->fieldAttributes($name, $attributes);

        $element = $this->textarea($name, $value, $attributes);

        return $this->fieldWrapper($name, $label, $element);
    }

    public function boTexteditor($name, $label = null, $value = null, $attributes = [])
    {
        $src = [
            'class' => 'form-control text-editor',
            'data-plugin-file-src' => $this->url->route('admin.file.choose', 'documents'),
            'data-plugin-image-src' => $this->url->route('admin.file.choose', 'images')
        ];

        $attributes = array_merge($src, $attributes);

        $attributes = $this->fieldAttributes($name, $attributes);

        $element = $this->textarea($name, $value, $attributes);

        return $this->fieldWrapper($name, $label, $element);
    }

    public function boSelect($name, $label = null, $options, $value = null, $attributes = [], $extra = [])
    {
        if (empty($extra['sizes'])) {
            $extra['sizes'] = 'col-sm-6 col-md-4';
        }

        $attributes = $this->fieldAttributes($name, $attributes);

        if (empty($attributes['multiple']) && empty($attributes['placeholder'])) {
            $attributes['placeholder'] = '';
        }

        $element = $this->select($name, $options, $value, $attributes);

        return $this->fieldWrapper($name, $label, $element, $extra);
    }

    public function boSelectMultiple($name, $label = null, $options, $value = null, $attributes = [], $extra = [])
    {
        $attributes = array_merge($attributes, ['multiple' => true]);
        $name .= '[]';

        return $this->boSelect($name, $label, $options, $value, $attributes, $extra);
    }

    public function boRadioGroup($name, $label = null, $options, $value = null, $extra = [])
    {
        $element = '';

        foreach ($options as $val => $option) {
            $checked = !is_null($value) ? $val == $value : null;

            $element .= '<label class="radio-inline">';
            $element .= $this->radio($name, $val, $checked);
            $element .= ' ';
            $element .= $option;
            $element .= '</label> ';
        }

        return $this->fieldWrapper($name, $label, $element, $extra);
    }

    public function boBoolean($name, $label = null, $value = 0, $extra = [])
    {
        $options = [
            1 => 'Sim',
            0 => 'Não'
        ];

        return $this->boRadioGroup($name, $label, $options, $value, $extra);
    }

    public function boCheckboxGroup($name, $label = null, $options, $value = [], $extra = [])
    {
        $element = '';
        $name .= '[]';

        foreach ($options as $val => $option) {
            $checked = !empty($value) && is_array($value) ? in_array($val, $value) : null;

            $element .= '<label class="checkbox-inline">';
            $element .= $this->checkbox($name, $val, $checked);
            $element .= ' ';
            $element .= $option;
            $element .= '</label> ';
        }

        return $this->fieldWrapper($name, $label, $element, $extra);
    }

    public function boFile($name, $label = null, $directory, $value = null, $attributes = [], $extra = [])
    {
        $attributes = $this->fieldAttributes($name, $attributes);

        $element = '<div class="input-group" data-plugin-file-choose>';
        $element .= $this->text($name, $value, $attributes);
        $element .= '<span class="input-group-btn">';
        $element .= '<a class="btn btn-primary btn-flat file-choose-button" href="#" data-url="' . $this->url->route('admin.file.choose',
                $directory) . '"><span class="fa fa-folder-open"></span></a>';
        $element .= '<a href="#" class="btn btn-danger btn-flat file-choose-clear" aria-hidden="true"><i class="fa fa-times"></i></a>';
        $element .= '</span>';
        $element .= '</div>';

        return $this->fieldWrapper($name, $label, $element, $extra);
    }

    public function boTextStatic($label = null, $value, $extra = [])
    {
        $name = $label;

        $element = '<p class="form-control-static">';
        $element .= $value;
        $element .= '</p>';

        return $this->fieldWrapper($name, $label, $element, $extra);
    }

    public function boDateRange($name, $label, $valueStart = null, $valueEnd = null)
    {
        $nameStart = sprintf('%s_start', $name);
        $nameEnd = sprintf('%s_end', $name);
        $attributes = ['class' => 'form-control', 'id' => $nameStart];

        $element = '<div class="input-daterange input-group" data-plugin-datepicker id="datepicker">';
        $element .= $this->text($nameStart, $valueStart, $attributes);
        $element .= '<span class="input-group-addon">até</span>';

        $attributes['id'] = $nameEnd;

        $element .= $this->text($nameEnd, $valueEnd, $attributes);
        $element .= '</div>';

        $extra = ['sizes' => 'col-md-5 col-sm-7'];

        return $this->fieldWrapper($name, $label, $element, $extra);
    }

    public function boBooleanGroup($name, $label = null, $value = 0, $extra = [])
    {
        $currentValue = $this->getValueAttribute($name);

        $element = $this->boBoolean($name, $label, $value, $extra);
        $element .= '<div data-collapsedBy="' . $name . '" ' . (!$currentValue ? 'style="display:none" ' : '') . '>';

        return $element;
    }

    public function boCloseBooleanGroup()
    {
        return '</div>';
    }

    public function boTopicGroup($title)
    {
        $element = '<div class="form-group-topic">';
        $element .= '<h4 class="margin-bottom">' . $title . '</h4>';

        return $element;
    }

    public function boCloseTopicGroup()
    {
        return '</div>';
    }
}
