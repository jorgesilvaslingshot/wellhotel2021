<?php

namespace Slingshot\Admin\Http\Controllers;


use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Http\Request;


class RemindController extends Controller
{

    public function __construct(PasswordBroker $password)
    {
        $this->password = $password;
    }

    public function index()
    {
        return view('admin::remind');
    }

    public function remind(Request $request)
    {
        $credentials = $request->only('email');

        $response = $this->password->sendResetLink($credentials, function ($message) {
            $message->subject('Redifinir palavra-passe');
        });

        switch ($response) {
            case PasswordBroker::RESET_LINK_SENT:
                return redirect()->route('admin.login')->with('success', trans('admin::' . $response));

            case PasswordBroker::INVALID_USER:
                return redirect()->back()->with('error', trans('admin::' . $response));
        }
    }

}
