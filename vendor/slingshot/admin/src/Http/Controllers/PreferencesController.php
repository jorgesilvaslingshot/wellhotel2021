<?php

namespace Slingshot\Admin\Http\Controllers;

use Slingshot\Admin\Entities\User;
use Slingshot\Admin\Http\Requests\SavePreferencesRequest;
use Illuminate\Contracts\Auth\Guard;

class PreferencesController extends Controller
{

    public function __construct(Guard $auth, User $user)
    {
        $this->auth = $auth;
        $this->userRepo = $user;
    }

    public function index()
    {
        $user = $this->userRepo->getUserOrCreate($this->auth->id());

        return view('admin::preferences')
            ->with('user', $user);
    }

    public function update(SavePreferencesRequest $request)
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $newPassword = $request->get('new_password');
        $password = null;

        if ($request->get('update_password') == '1') {
            $password = bcrypt($newPassword);
        }

        $this->userRepo->saveUser($this->auth->id(),$name, $email, $password);

        return redirect()->back()->with('success', ['As suas preferências foram guardadas com sucesso.']);
    }

}
