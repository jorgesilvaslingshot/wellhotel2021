<?php

namespace Slingshot\Admin\Http\Controllers;


use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class ResetController extends Controller
{

    public function __construct(PasswordBroker $password)
    {
        $this->password = $password;
    }

    public function index($token = null)
    {
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        return view('admin::reset')
            ->with('token', $token);
    }

    public function reset(Request $request)
    {
        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = $this->password->reset($credentials, function ($user, $password) {
            $user->password = bcrypt($password);
            $user->save();
        });

        switch ($response) {
            case PasswordBroker::PASSWORD_RESET:
                return redirect()->route('admin.login')->with('success',
                    'A sua palavra-passe foi guardada com sucesso!');

            default:
                return redirect()->back()
                    ->withInput($request->only('email'))
                    ->with('error', trans('admin::' . $response));
        }
    }

}
