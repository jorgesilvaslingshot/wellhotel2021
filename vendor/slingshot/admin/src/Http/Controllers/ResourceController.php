<?php

namespace Slingshot\Admin\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;

abstract class ResourceController extends Controller
{
    protected $config;

    public $request;

    public $datagrid = [];

    public $form = null;

    public $mainTemplate = 'admin::lock-template';

    public $datagridToolbarTemplate = 'admin::datagrid.toolbar';

    public $datagridRowTemplate = null;

    public $title = null;

    public $subtitle = null;

    public $indexLink;

    public $createLink = false;

    public $editLink = false;

    public $deleteLink = false;

    protected function setConfig()
    {
        $this->config = app('Slingshot\Admin\Services\Menu')->getCurrentMenu();

        $routes = app('routes');

        $this->config['controller'] = static::class;

        $hasEdit = !empty($routes->getByAction($this->config['controller'] . '@edit'));
        $hasDelete = !empty($routes->getByAction($this->config['controller'] . '@delete'));

        $this->title = $this->config['title'];
        $this->indexLink = action('\\' . $this->config['controller'] . '@index');
        if ($this->config['create'] && $hasEdit) {
            $this->createLink = action('\\' . $this->config['controller'] . '@edit');
        }
        if ($this->config['edit'] && $hasEdit) {
            $this->editLink = action('\\' . $this->config['controller'] . '@edit', '%s');
        }
        if ($this->config['delete'] && $hasDelete) {
            $this->deleteLink = action('\\' . $this->config['controller'] . '@delete', '%s');
        }
    }

    protected function getPagination()
    {
        $pagination = new LengthAwarePaginator($this->datagrid['items'], $this->datagrid['total'],
            $this->datagrid['per_page'],
            $this->datagrid['page']);

        return $pagination->setPath('')->appends($this->getQuery())->render();
    }

    protected function getUrlWithoutQuery()
    {
        return parse_url($this->request->getRequestUri(), PHP_URL_PATH);
    }

    protected function getQuery()
    {
        return $this->request->query();
    }

    protected function setComposers()
    {
        view()->composer('admin::datagrid.search', function ($view) {
            $view
                ->with('search_columns', $this->datagrid['search_columns'])
                ->with('columns', $this->datagrid['columns'])
                ->with('search_by', $this->datagrid['search_by'])
                ->with('search', $this->datagrid['search'])
                ->with('url_path', $this->getUrlWithoutQuery());
        });

        view()->composer('admin::datagrid.toolbar', function ($view) {
            $view
                ->with('createLink', $this->createLink);
        });

        view()->composer('admin::datagrid.header', function ($view) {
            $view
                ->with('columns', $this->datagrid['columns'])
                ->with('sort_columns', $this->datagrid['sort_columns'])
                ->with('query', $this->getQuery())
                ->with('sort_by', $this->datagrid['sort_by'])
                ->with('sort', $this->datagrid['sort']);
        });

        view()->composer('admin::datagrid.actions', function ($view) {
            $view
                ->with('editLink', $this->editLink)
                ->with('deleteLink', $this->deleteLink);
        });

        view()->composer('admin::datagrid.template', function ($view) {
            $view
                ->with('title', $this->title)
                ->with('subtitle', $this->subtitle)
                ->with('pagination', $this->getPagination())
                ->with('data', $this->datagrid['items'])
                ->with('mainTemplate', $this->mainTemplate)
                ->with('datagridToolbarTemplate', $this->datagridToolbarTemplate)
                ->with('datagridRowTemplate', $this->datagridRowTemplate);
        });

        view()->composer('admin::form.template', function ($view) {
            $view
                ->with('title', $this->title)
                ->with('subtitle', $this->subtitle)
                ->with('mainTemplate', $this->mainTemplate)
                ->with('form', $this->form);
        });

        view()->composer('admin::form.actions', function ($view) {
            $view
                ->with('url', $this->indexLink);
        });
    }

    public function callAction($method, $parameters)
    {
        $this->request = \Route::getCurrentRequest();

        $this->setConfig();
        $this->setComposers();

        $response = call_user_func_array(array($this, $method), $parameters);

        if (is_null($response) && !empty($this->datagrid)) {
            $response = view('admin::datagrid.template');
        }

        if (is_null($response) && !empty($this->form)) {
            $response = view('admin::form.template');
        }

        return $response;
    }

    public function goBack()
    {
        return redirect()->back();
    }

    public function saveSucess($msg = null)
    {
        if (empty($msg)) {
            $msg = 'O registo foi guardado com sucesso!';
        }

        return redirect($this->indexLink)->with('success', [$msg]);
    }
}
