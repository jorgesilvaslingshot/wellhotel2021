<?php


namespace Slingshot\Admin\Http\Controllers;


use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\StringInput;

class ArtisanController extends Controller
{
    public function index()
    {
        $commands = array_keys(\Artisan::all());
        $commandList = array_combine($commands, $commands);
        return view('admin::artisan')->with('commands', $commandList);
    }

    public function execute(Request $request)
    {
        $commandName = $request->get('command');
        $args = $request->get('arguments');
        $return = redirect()->back();

        try {
            \Artisan::call($commandName, $this->parseArguments($commandName, $args));
            $return->with('success', ['O comando foi executado com sucesso!']);
        } catch (RuntimeException $e) {
            $return->with('warning', [$e->getMessage()]);
        }

        return $return;
    }

    /**
     * Parses the argument string into an array based on the selected command's definition
     *
     * @param string $commandName
     * @param string $arguments
     * @return array
     */
    private function parseArguments($commandName, $arguments)
    {
        /* @var $command Command */
        $command = \Artisan::all()[$commandName];
        $stringInput = new StringInput($arguments);
        $stringInput->bind($command->getDefinition());
        $stringInput->validate();
        return $stringInput->getArguments();
    }
}
