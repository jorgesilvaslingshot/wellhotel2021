<?php

namespace Slingshot\Admin\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Session\Store as Session;


class LoginController extends Controller
{

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function index()
    {
        return view('admin::login');
    }

    public function login(Request $request)
    {
        $credentials = [];
        $credentials['email'] = $request->get('email');
        $credentials['password'] = $request->get('password');

        if (!$this->auth->attempt($credentials)) {
            return redirect()->back()
                ->withInput($request->flashOnly('email'))
                ->with('error', 'Não foi possível efetuar o login. Tente novamente.');
        }

        return redirect()->route('admin.dashboard');
    }

    public function logout()
    {
        $this->auth->logout();

        return redirect()->route('admin.login');
    }

}
