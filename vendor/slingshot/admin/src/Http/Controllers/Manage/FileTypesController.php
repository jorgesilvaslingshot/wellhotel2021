<?php

namespace Slingshot\Admin\Http\Controllers\Manage;

use Slingshot\Admin\Entities\FileType;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Http\Requests\SaveFileTypesRequest;
use Slingshot\Admin\Services\Datagrid;

class FileTypesController extends ResourceController
{
    public $datagridRowTemplate = 'admin::manage.fileTypes.row';

    public function __construct(FileType $fileTypeRepo)
    {
        $this->fileTypeRepo = $fileTypeRepo;
    }

    public function index()
    {
        $datagrid = new Datagrid($this->fileTypeRepo->getFileTypesDatagrid());

        $this->datagrid = $datagrid
            ->addColumn('id', 'ID', false)
            ->addColumn('directory', 'Directoria')
            ->addColumn('title', 'Titulo')
            ->addColumn('extensions', 'Extensões')
            ->addColumn('max_file_size', 'Tamanho Máximo')
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->fileTypeRepo->getFileTypeOrCreate($id);

        $this->form = view('admin::manage.fileTypes.form', compact('data'));
    }

    public function save(SaveFileTypesRequest $request, $id = null)
    {
        $directory = $request->get('directory');
        $title = $request->get('title');
        $extensions = $request->get('extensions');
        $maxFileSize = $request->get('max_file_size');

        $this->fileTypeRepo->saveFileType($id, $directory, $title, $extensions, $maxFileSize);

        return $this->saveSucess();
    }

    public function delete($id)
    {
        $this->fileTypeRepo->destroy($id);

        return $this->goBack();
    }
}
