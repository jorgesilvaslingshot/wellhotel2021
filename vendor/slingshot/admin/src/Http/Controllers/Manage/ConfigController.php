<?php

namespace Slingshot\Admin\Http\Controllers\Manage;

use anlutro\LaravelSettings\SettingsManager;
use Illuminate\Http\Request;
use Slingshot\Admin\Http\Controllers\ResourceController;

class ConfigController extends ResourceController
{
    public function __construct(SettingsManager $settingsManager)
    {
        $this->settingsManager = $settingsManager;
    }

    public function index()
    {
        $data = $this->settingsManager->all();

        if (view()->exists($view = config('admin.config-view'))) {
            $this->form = view($view, compact('data'));
        }
    }

    public function save(Request $request)
    {
        $this->settingsManager->set($request->input());
        $this->settingsManager->save();

        return $this->saveSucess('As configurações foram guardadas com sucesso!');
    }
}
