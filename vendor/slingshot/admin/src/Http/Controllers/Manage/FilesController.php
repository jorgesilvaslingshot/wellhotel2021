<?php

namespace Slingshot\Admin\Http\Controllers\Manage;

use Slingshot\Admin\Entities\File;
use Slingshot\Admin\Entities\FileType;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Services\Datagrid;
use Illuminate\Routing\ResponseFactory;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class FilesController extends ResourceController
{
    public $datagridToolbarTemplate = 'admin::manage.files.toolbar';

    public $datagridRowTemplate = 'admin::manage.files.row';

    public $addLink = null;

    public $popup = false;

    public function __construct(
        ResponseFactory $response,
        FileType $fileTypeRepo,
        File $fileRepo
    ) {
        $this->response = $response;
        $this->fileTypeRepo = $fileTypeRepo;
        $this->fileRepo = $fileRepo;
    }

    public function index($directory = null)
    {
        $fileType = $this->fileTypeRepo->getFileType($directory);

        $datagrid = new Datagrid($fileType->getFilesDatagrid());

        $this->datagrid = $datagrid->addColumn('preview', '', false, false)
            ->addColumn('path', 'Ficheiro')
            ->addColumn('created_at', 'Data')
            ->get();

        $this->createLink = action('\\' . $this->config['controller'] . '@add', $fileType->directory);
        $this->indexLink = action('\\' . $this->config['controller'] . '@index', '%s');
        $this->subtitle = $fileType->title;

        view()->composer($this->datagridToolbarTemplate, function ($view) use ($fileType) {
            $view->with('otherFileTypes', $fileType->getOtherFileTypes())
                ->with('indexLink', $this->indexLink);
        });
    }

    public function add($directory)
    {
        session()->put('info', ['Arraste os ficheiros para a página ou pressione o botão Adicionar']);

        $fileType = $this->fileTypeRepo->getFileType($directory);

        if (is_null($fileType)) {
            throw new NotFoundHttpException;
        }

        $this->indexLink = action('\\' . $this->config['controller'] . '@index', $fileType->directory);
        $this->addLink = action('\\' . $this->config['controller'] . '@add', $fileType->directory);

        return view('admin::manage.files.add')
            ->with('mainTemplate', $this->mainTemplate)
            ->with('fileType', $fileType)
            ->with('addLink', $this->addLink)
            ->with('indexLink', $this->indexLink)
            ->with('popup', $this->popup);
    }

    public function save($directory)
    {
        $fileType = $this->fileTypeRepo->getFileType($directory);

        $extensions = $fileType->getArrayExtensions();

        $files = $this->request->file('files');

        $responses = [];

        $response = [];

        foreach ($files as $file) {
            if (!$file->isValid()) {
                return json_encode(['success' => 0, 'message' => 'Não é possível mover o ficheiro.']);
            }

            $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize() / 1024;

            $response['success'] = 0;

            if (!in_array($extension, $extensions)) {
                $response['message'] = 'A extensão do ficheiro é inválida.';
            } elseif ($size > $fileType->max_file_size) {
                $response['message'] = 'O tamanho do ficheiro excede o permitido.';
            } else {
                $newName = strtolower(preg_replace('/[^a-zA-Z0-9]/', '_', $originalName));
                $folderPath = sprintf('%s/%s/', config('admin.files-path'), $directory);

                $i = 2;
                $path = sprintf('%s%s.%s', $folderPath, $newName, $extension);
                $auxName = $newName;
                while (file_exists($path)) {
                    $newName = sprintf('%s-%s', $auxName, $i);
                    $path = sprintf('%s%s.%s', $folderPath, $newName, $extension);
                    $i++;
                }

                $newName .= sprintf('.%s', $extension);

                if ($file->move(public_path($folderPath), $newName)) {
                    $newFile = $this->fileRepo->registerFile($path, $size);
                    $fileType->addFile($newFile);

                    $response['success'] = 1;
                    $response['path'] = $newFile->getPath();
                } else {
                    $response['message'] = 'Não é possível mover o ficheiro.';
                }
            }

            array_push($responses, $response);
        }

        return $this->response->json($responses);
    }

    public function delete($id)
    {
        $this->fileRepo->destroy($id);

        return $this->goBack();
    }
}
