<?php

namespace Slingshot\Admin\Http\Controllers\Manage;

use Slingshot\Admin\Entities\User;
use Slingshot\Admin\Http\Controllers\ResourceController;
use Slingshot\Admin\Http\Requests\SaveUsersRequest;
use Slingshot\Admin\Services\Datagrid;

class UsersController extends ResourceController
{
    public $datagridRowTemplate = 'admin::manage.users.row';

    public function __construct(User $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function index()
    {
        $datagrid = new Datagrid($this->userRepo->getUsersDatagrid());

        $this->datagrid = $datagrid
            ->addColumn('id', 'ID', false)
            ->addColumn('name', 'Nome')
            ->addColumn('email', 'Email')
            ->addColumn('updated_at', 'Última alteração')
            ->get();
    }

    public function edit($id = null)
    {
        $data = $this->userRepo->getUserOrCreate($id);

        if ($data->getId()) {
            $this->form = view('admin::manage.users.updateForm', compact('data'));
        } else {
            $this->form = view('admin::manage.users.addForm');
        }
    }

    public function save(SaveUsersRequest $request, $id = null)
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $password = null;

        if ($request->has('password')) {
            $password = bcrypt($request->get('password'));
        }

        if ($request->has('new_password')) {
            $password = bcrypt($request->get('new_password'));
        }

        $this->userRepo->saveUser($id, $name, $email, $password);

        return $this->saveSucess();
    }

    public function delete($id)
    {
        $this->userRepo->destroy($id);

        return $this->goBack();
    }
}
