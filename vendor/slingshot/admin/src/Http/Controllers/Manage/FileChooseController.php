<?php

namespace Slingshot\Admin\Http\Controllers\Manage;

class FileChooseController extends FilesController
{
    public $mainTemplate = 'admin::popupTemplate';

    public $datagridToolbarTemplate = 'admin::datagrid.toolbar';

    public $datagridRowTemplate = 'admin::manage.fileChoose.row';

    public $popup = true;
}
