<?php

$this->app['view']->composer('admin::lock-template', function ($view) {
    $user = $this->app['request']->user()->name;

    $view->with('user', $user);
});

$this->app['view']->composer('admin::menu.navbar', function ($view) {
    $menus = $this->app['admin.menu']->getMenus();

    $view->with('menus', $menus);
});
