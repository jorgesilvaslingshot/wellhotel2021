<?php namespace Slingshot\Admin\Http\Middleware;

use Closure;

class Lang
{
    public function handle($request, Closure $next)
    {
        app()->setLocale('pt');

        return $next($request);
    }

}
