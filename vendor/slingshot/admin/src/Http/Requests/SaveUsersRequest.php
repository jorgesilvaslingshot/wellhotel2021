<?php namespace Slingshot\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveUsersRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if($this->request->get('update_password') != '1' ) {
            $this->request->remove('new_password');
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:users,email,' . $this->route('id'), 'max:255'],
            'password' => ['sometimes', 'required', 'min:6', 'confirmed'],
            'new_password' => ['sometimes' ,'required', 'min:6', 'confirmed']
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nome',
            'email' => 'Email',
            'password' => 'Palavra-passe',
            'new_password' => 'Nova Palavra-passe'
        ];
    }

}
