<?php namespace Slingshot\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveFileTypesRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'directory' => 'required|unique:file_types,directory,' . $this->route('id'),
            'title' => 'required',
            'extensions' => 'required',
            'max_file_size' => 'required|numeric'
        ];
    }

    public function attributes()
    {
        return [
            'directory' => 'Directoria',
            'title' => 'Título',
            'extensions' => 'Extensões',
            'max_file_size' => 'Tamanho Máximo'
        ];
    }

}
