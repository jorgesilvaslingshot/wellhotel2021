<?php namespace Slingshot\Admin\Http\Requests;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Foundation\Http\FormRequest;

class SavePreferencesRequest extends FormRequest
{
    public function __construct(Guard $auth, Hasher $hash)
    {
        $this->user = $auth->user();
        $this->hash = $hash;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if($this->request->get('update_password') != '1' ) {
            $this->request->remove('new_password');
        }
    }

    public function validate()
    {
        $this->prepareForValidation();

        $instance = $this->getValidatorInstance();

        $instance->after(function($validator) {
            if (!$this->hash->check($this->get('password'), $this->user->password)) {
                $validator->errors()->add('password', 'A palavra-passe actual está incorreta.');
            }
        });

        if (! $this->passesAuthorization()) {
            $this->failedAuthorization();
        } elseif (! $instance->passes()) {
            $this->failedValidation($instance);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:255'],
            'email' => ['required', 'email', 'unique:users,email,' . $this->user->id, 'max:255'],
            'new_password' => ['sometimes', 'required', 'min:6', 'confirmed']
        ];
    }

    /**
     * Set custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'new_password.required_if' => trans('validation.required')
        ];
    }

    /**
     * Set custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => 'Nome',
            'email' => 'Email',
            'new_password' => 'Nova Palavra-passe'
        ];
    }

}
