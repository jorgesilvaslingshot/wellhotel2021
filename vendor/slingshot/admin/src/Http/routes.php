<?php

$middleware = version_compare(\Illuminate\Foundation\Application::VERSION, '5.2.*', '>=') ? ['web', 'Slingshot\Admin\Http\Middleware\Lang'] : [];

$this->app['router']->group(['prefix' => 'backoffice', 'middleware' => $middleware], function () {

    $this->app['router']->group([
        'middleware' => 'Slingshot\Admin\Http\Middleware\Authenticate',
    ], function () {
        $this->app['router']->group([
            'namespace' => 'Slingshot\Admin\Http\Controllers'
        ], function () {
            $this->app['router']->get('logout', ['as' => 'admin.logout', 'uses' => 'LoginController@logout']);
            $this->app['router']->get('/', ['as' => 'admin.dashboard', 'uses' => 'DashboardController@index']);

            $this->app['router']->get('preferences',
                ['as' => 'admin.preferences', 'uses' => 'PreferencesController@index']);
            $this->app['router']->post('preferences', ['uses' => 'PreferencesController@update']);
            $this->app['router']->get('artisan', [
                'as' => 'admin.artisan',
                'uses' => 'ArtisanController@index',
            ]);
            $this->app['router']->post('artisan', [
                'uses' => 'ArtisanController@execute',
            ]);
        });

        $this->app['router']->group([
            'menu' => 'files',
            'namespace' => 'Slingshot\Admin\Http\Controllers\Manage'
        ], function () {
            $this->app['router']->get('files/{directory?}',
                ['as' => 'admin.files', 'uses' => 'FilesController@index']);
            $this->app['router']->get('files/{directory}/add',
                ['as' => 'admin.files.add', 'uses' => 'FilesController@add']);
            $this->app['router']->get('file-choose/{directory?}',
                ['as' => 'admin.file.choose', 'uses' => 'FileChooseController@index']);
            $this->app['router']->get('file-choose/{directory}/add',
                ['as' => 'admin.file.choose.add', 'uses' => 'FileChooseController@add']);
            $this->app['router']->post('files/{directory}/add/',
                ['as' => 'admin.files.add.save', 'uses' => 'FilesController@save']);
            $this->app['router']->post('file-choose/{directory}/add/',
                ['as' => 'admin.file.choose.add.save', 'uses' => 'FileChooseController@save']);
            $this->app['router']->delete('files/{id}/delete',
                ['as' => 'admin.files.delete', 'uses' => 'FilesController@delete']);
        });

        $this->app['router']->group([
            'menu' => 'config',
            'namespace' => 'Slingshot\Admin\Http\Controllers\Manage',
        ], function () {
            $this->app['router']->get('config', ['as' => 'admin.config', 'uses' => 'ConfigController@index']);
            $this->app['router']->post('config', ['as' => 'admin.config.save', 'uses' => 'ConfigController@save']);
        });

    });

    $this->app['router']->group([
        'middleware' => 'Slingshot\Admin\Http\Middleware\RedirectIfAuthenticated',
        'namespace' => 'Slingshot\Admin\Http\Controllers'
    ], function () {
        $this->app['router']->get('login', ['as' => 'admin.login', 'uses' => 'LoginController@index']);
        $this->app['router']->post('login', 'LoginController@login');
        $this->app['router']->get('remind', ['as' => 'admin.remind', 'uses' => 'RemindController@index']);
        $this->app['router']->post('remind', 'RemindController@remind');
        $this->app['router']->get('reset/{token?}', ['as' => 'admin.reset', 'uses' => 'ResetController@index']);
        $this->app['router']->post('reset', ['as' => 'admin.reset.post', 'uses' => 'ResetController@reset']);
    });

    $this->app['router']->group(['middleware' => 'Slingshot\Admin\Http\Middleware\Authenticate'], function () {

        $links = $this->app['admin.menu']->getLinks();

        foreach ($links as $link) {
            if ($link['autoRoutes']) {
                $this->app['router']->group(['menu' => $link['key']], function () use ($link) {

                    $this->app['router']->get($link['key'],
                        ['as' => 'admin.' . $link['key'], 'uses' => $link['controller'] . '@index']);

                    if ($link['edit'] || $link['create']) {
                        $uri = '/edit/{id?}';

                        if (!$link['create']) {
                            $uri = '/edit/{id}';
                        } elseif (!$link['edit']) {
                            $uri = '/edit';
                        }

                        $uri = $link['key'] . $uri;

                        $this->app['router']->get($uri,
                            ['as' => 'admin.' . $link['key'] . '.edit', 'uses' => $link['controller'] . '@edit']);
                        $this->app['router']->post($uri,
                            [
                                'as' => 'admin.' . $link['key'] . '.edit.save',
                                'uses' => $link['controller'] . '@save'
                            ]);
                    }

                    if ($link['delete']) {
                        $this->app['router']->delete($link['key'] . '/{id}/delete',
                            ['as' => 'admin.' . $link['key'] . '.delete', 'uses' => $link['controller'] . '@delete']);
                    }

                });
            }
        }
    });
});
