<?php

namespace Slingshot\Admin\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class ResetPassword extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;
    /**
     * @var
     */
    protected $name;

    /**
     * Create a notification instance.
     *
     * @param  string $token
     * @param $name
     */
    public function __construct($token, $name)
    {
        $this->token = $token;
        $this->name = $name;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->markdown('admin::notifications.email')
            ->subject('Redefinir palavra-passe')
            ->greeting('Caro(a) ' . $this->name . ',')
            ->salutation(new HtmlString('Cumprimentos,<br>'.config('app.name')))
            ->line('Redefina a palavra-passe clicando no botão abaixo:')
            ->action('Redefinir palavra-passe', url(config('app.url') . route('admin.reset', $this->token, false)))
            ;
    }
}
