<?php namespace Slingshot\Admin\Console;

use Illuminate\Console\Command;
use Illuminate\Foundation\Application;

/**
 * Class Install
 * @package Slingshot\Admin\Console
 */
class Install extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Admin package';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (version_compare(Application::VERSION, '5.1.3', '<=')) {
            $this->call('vendor:publish', ['--tag' => 'admin.migrations']);
            $this->call('vendor:publish', ['--tag' => 'admin.assets']);
            $this->call('vendor:publish', ['--tag' => 'admin.config']);
        } else {
            $this->call('vendor:publish', ['--tag' => ['admin.migrations', 'admin.assets', 'admin.config']]);
        }

        if ($this->confirm('Do you want run all database migrations? [yes|no]', true)) {
            $this->call('migrate');
        }

        if ($this->confirm('Do you want run Admin database seeds? [yes|no]', true)) {
            $this->call('db:seed', ['--class' => 'UserTableSeeder']);
            $this->call('db:seed', ['--class' => 'FileTypeTableSeeder']);
        }

        $this->info('Admin installation complete!');
    }

    // old Laravel
    public function fire()
    {
        $this->handle();
    }
}
