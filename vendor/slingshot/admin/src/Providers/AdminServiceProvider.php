<?php namespace Slingshot\Admin\Providers;

use Collective\Html\HtmlBuilder;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Auth\Passwords\DatabaseTokenRepository;
use Illuminate\Auth\Passwords\PasswordBroker;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Slingshot\Admin\Entities\EloquentFile;
use Slingshot\Admin\Entities\EloquentFileType;
use Slingshot\Admin\Entities\EloquentUser;
use Slingshot\Admin\Services\FormBuilder;
use Slingshot\Admin\Services\Menu;


class AdminServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerMigrations();
        $this->registerCommands();
        $this->registerAssets();

        include __DIR__ . '/../Http/routes.php';

        include __DIR__ . '/../Http/viewComposers.php';
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->extend('form', function($service, $app) {
            $form = new FormBuilder($app['html'], $app['url'], $app['view'], $app['session.store']->token());

            return $form->setSessionStore($app['session.store']);
        });

        $this->app->bind('admin.menu', function ($app) {
            return new Menu($app['config'], $app['router']);
        });

        $this->app->bind('Slingshot\Admin\Entities\User', function () {
            return new EloquentUser();
        });

        $this->app->bind('Slingshot\Admin\Entities\FileType', function () {
            return new EloquentFileType();
        });

        $this->app->bind('Slingshot\Admin\Entities\File', function () {
            return new EloquentFile();
        });
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/config.php', 'admin'
        );

        $this->publishes([
            __DIR__ . '/../../config/config.php' => config_path('admin.php'),
        ], 'admin.config');
    }

    /**
     * Register views.
     *
     * @return void
     */
    protected function registerViews()
    {
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'admin');

        $this->publishes([
            __DIR__ . '/../../resources/views' => resource_path('views/vendor/admin')
        ], 'admin.views');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    protected function registerTranslations()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'admin');

        $this->publishes([
            __DIR__ . '/../../resources/lang' => resource_path('lang/vendor/admin'),
        ], 'admin.translations');
    }

    /**
     * Register migrations.
     *
     * @return void
     */
    protected function registerMigrations()
    {
        $this->publishes([
            __DIR__ . '/../../database/migrations/' => database_path('migrations')
        ], 'admin.migrations');
    }

    /**
     * Register commands.
     *
     * @return void
     */
    protected function registerCommands()
    {
        $this->commands([
            \Slingshot\Admin\Console\Install::class
        ]);
    }

    /**
     * Register assets.
     *
     * @return void
     */
    public function registerAssets()
    {
        $this->publishes([
            __DIR__ . '/../../resources/assets/' => public_path('assets/admin')
        ], 'admin.assets');
    }

    /**
     * @return string[]
     */
    public function provides()
    {
        return [];
    }
}
