<?php

namespace Slingshot\Admin\Entities;

interface FileType
{
    public function getId();

    public function getTitle();

    public function getDirectory();

    public function getExtensions();

    public function getArrayExtensions();

    public function getMaxFileSize();

    public static function getFileType($directory);

    public function getOtherFileTypes();

    public function addFile($file);

    public static function getFileTypesDatagrid();

    public function getFilesDatagrid();

    public static function saveFileType($id, $directory, $title, $extensions, $maxFileSize);

    public static function getFileTypeOrCreate($id);

    public static function destroy($id);
}
