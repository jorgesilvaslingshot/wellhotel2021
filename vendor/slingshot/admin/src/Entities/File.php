<?php

namespace Slingshot\Admin\Entities;

interface File
{
    public function getId();

    public function getPath();

    public function getExtension();

    public function getDate();

    public static function registerFile($path, $size);
}
