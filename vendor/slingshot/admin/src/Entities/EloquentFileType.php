<?php

namespace Slingshot\Admin\Entities;

use Slingshot\Admin\Services\EloquentDatagridProvider;
use Illuminate\Database\Eloquent\Model;

class EloquentFileType extends Model implements FileType
{

    protected $table = 'file_types';

    public function files()
    {
        return $this->hasMany('Slingshot\Admin\Entities\EloquentFile', 'file_type_id');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDirectory()
    {
        return $this->directory;
    }

    public function getExtensions()
    {
        return $this->extensions;
    }

    public function getArrayExtensions()
    {
        return explode(',', $this->extensions);
    }

    public function getMaxFileSize()
    {
        return round($this->max_file_size / 1024, 1);
    }

    public static function getFileType($directory)
    {
        if (empty($directory)) {
            return static::getFirstFileType();
        }

        return static::where('directory', $directory)
            ->first();
    }

    public function getOtherFileTypes()
    {
        return static::where('id', '!=', $this->id)
            ->get();
    }

    public function addFile($file)
    {
        $this->files()->save($file);
    }

    public static function getFileTypesDatagrid()
    {
        $query = static::query();

        return new EloquentDatagridProvider($query);
    }

    public function getFilesDatagrid()
    {
        $query = $this->files()->getQuery();

        return new EloquentDatagridProvider($query);
    }

    public static function saveFileType($id, $directory, $title, $extensions, $maxFileSize)
    {
        $model = static::findOrNew($id);

        $model->directory = $directory;
        $model->title = $title;
        $model->extensions = $extensions;
        $model->max_file_size = $maxFileSize;

        $model->save();
    }

    public static function getFileTypeOrCreate($id)
    {
        return static::findOrNew($id);
    }

    protected static function getFirstFileType()
    {
        return static::orderBy('id', 'asc')
            ->first();
    }
}
