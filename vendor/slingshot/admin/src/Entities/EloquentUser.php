<?php

namespace Slingshot\Admin\Entities;

use Slingshot\Admin\Services\EloquentDatagridProvider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class EloquentUser extends Model implements User, AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $table = 'users';

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public static function getUsersDatagrid()
    {
        $query = static::query();

        return new EloquentDatagridProvider($query);
    }

    public static function saveUser($id, $name, $email, $password)
    {
        $model = static::findOrNew($id);

        $model->name = $name;
        $model->email = $email;

        if (!empty($password)) {
            $model->password = $password;
        }

        $model->save();
    }

    public static function getUserOrCreate($id = null)
    {
        return static::findOrNew($id);
    }
}
