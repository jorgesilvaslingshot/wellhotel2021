<?php

namespace Slingshot\Admin\Entities;

use Slingshot\Admin\Services\EloquentDatagridProvider;
use Illuminate\Database\Eloquent\Model;

class EloquentFile extends Model implements File
{

    protected $table = 'files';

    public function fileType()
    {
        return $this->belongsTo('Slingshot\Admin\Entities\EloquentFileType', 'file_type_id');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getExtension()
    {
        return pathinfo($this->getPath(), PATHINFO_EXTENSION);
    }

    public function getDate()
    {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public static function registerFile($path, $size)
    {
        $model = new static;
        $model->path = sprintf('/%s', $path);
        $model->size = $size;

        $extension = pathinfo($path, PATHINFO_EXTENSION);

        if (in_array($extension, ['png', 'jpeg', 'jpg', 'gif'])) {
            list($width, $height) = getimagesize($path);
            $model->width = $width;
            $model->height = $height;
        }

        //$model->save();

        return $model;
    }
}
