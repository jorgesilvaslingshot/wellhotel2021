<?php

namespace Slingshot\Admin\Entities;

interface User
{
    public function getId();

    public function getName();

    public function getEmail();

    public function getUpdatedAt();

    public static function getUsersDatagrid();

    public static function saveUser($id, $name, $email, $new_password);

    public static function getUserOrCreate($id);

    public static function destroy($id);
}
