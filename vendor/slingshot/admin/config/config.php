<?php

return [

    /* Website name */
    'site' => 'Novo Website',
    /* Upload files main folder */
    'files-path' => 'files',
    /*
     * Menu constructor
     *
     * Properties of type header:
     * - header (required)
     *
     * Properties of type link:
     * - key (required)
     * - title (default:null)
     * - controller (required)
     * - icon (default:fa fa-circle)
     * - label (default:null)
     * - create (default:true)
     * - edit (default:true)
     * - delete (default:true)
     * - onlyRoutes (default:false)
     * - autoRoutes (default:true)
     *
     * Properties of type parent
     * - title (default:null)
     * - children (required)
     *
     * */
    'menu' => [
        [
            'header' => 'Administração'
        ],
        [
            'key' => 'users',
            'title' => 'Utilizadores',
            'icon' => 'fa fa-users',
            'controller' => 'Slingshot\Admin\Http\Controllers\Manage\UsersController'
        ],
        [
            'key' => 'files',
            'title' => 'Ficheiros',
            'icon' => 'fa fa-files-o',
            'controller' => 'Slingshot\Admin\Http\Controllers\Manage\FilesController',
            'autoRoutes' => false,
            'edit' => false
        ],
        [
            'key' => 'file-types',
            'title' => 'Tipos de Ficheiros',
            'controller' => 'Slingshot\Admin\Http\Controllers\Manage\FileTypesController',
            'onlyRoutes' => true,
        ],
        [
            'key' => 'config',
            'title' => 'Configurações',
            'icon' => 'fa fa-cogs',
            'controller' => 'Slingshot\Admin\Http\Controllers\Manage\ConfigController',
            'autoRoutes' => false,
        ]
    ],
    /*
     * Config Form View
     * */
    'config-view' => null,
];
