<?php

use Slingshot\Admin\Entities\EloquentFileType as FileType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class FileTypeTableSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();

        FileType::truncate();

        FileType::create([
            'directory' => 'images',
            'title' => 'Imagens',
            'extensions' => 'jpg,jpeg,png,gif',
            'max_file_size' => 2048
        ]);

        FileType::create([
            'directory' => 'documents',
            'title' => 'Documentos',
            'extensions' => 'doc,docx,pdf,xls,xlsx',
            'max_file_size' => 2048
        ]);

    }

}
