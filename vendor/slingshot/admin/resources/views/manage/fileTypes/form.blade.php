{!! Form::boOpen($data) !!}
{!! Form::boText('directory','Directoria') !!}
{!! Form::boText('title','Título') !!}
{!! Form::boTags('extensions','Extensões') !!}
{!! Form::boText('max_file_size','Tamanho Máximo',null,[],['help'=>'Kb']) !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
