<tr>
  <td width="1%">{!! $row->getId() !!}</td>
  <td>{!! $row->getDirectory() !!}</td>
  <td>{!! $row->getTitle() !!}</td>
  <td>{!! $row->getExtensions() !!}</td>
  <td>{!! $row->getMaxFileSize() !!}Mb</td>
  @include('admin::datagrid.actions',['id'=>$row->getId()])
</tr>
