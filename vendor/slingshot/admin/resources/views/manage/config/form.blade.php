{!! Form::boOpen($data) !!}
{!! Form::boText('email','Email') !!}
{!! Form::boText('facebookLink','Link Facebook') !!}
{!! Form::boFile('ogImage','Imagem (Seo)','images') !!}

{!! Form::boTextarea('meta_description','Descrição Meta') !!}

{!! Form::boDateRange('hotidays','Férias') !!}

<div class="form-group form-actions">
  <div class="col-sm-10 col-sm-offset-2">
    <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-check"></i> Guardar</button>
  </div>
</div>

{!! Form::boClose() !!}
