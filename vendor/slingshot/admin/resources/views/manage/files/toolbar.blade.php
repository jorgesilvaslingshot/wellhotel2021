@extends('admin::datagrid.toolbar')

@section('toolbar.content')
  @parent
  <div class="btn-group">
    <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
      Mudar tipo de ficheiro <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
      @foreach($otherFileTypes as $otherFileType)
        <li><a href="{!! sprintf($indexLink,$otherFileType->directory) !!}">{!!
            $otherFileType->getTitle() !!}</a></li>
      @endforeach
    </ul>
  </div>
@stop
