@extends($mainTemplate)

@section('content-header')
  <h1>Upload de Ficheiros</h1>
@stop

@section('content')
  <div class="row">
    <div class="col-md-3">
      <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">{!! $fileType->getTitle() !!}</h3>
        </div>
        <div class="box-body">
          <ul class="list-unstyled">
            <li><b>Tamanho Máximo:</b> {!! $fileType->getMaxFileSize() !!} Mb</li>
            <li><b>Extensões:</b> {!! $fileType->getExtensions() !!}</li>
          </ul>
        </div>
      </div>

      <span class="btn btn-primary btn-block file-button">
        <span>Upload</span>
        <input id="fileupload" type="file" name="files[]" data-url="{!! $addLink !!}"
               data-directory="{!! $fileType->getDirectory() !!}"
               data-on-error="Não é possível mover o ficheiro." data-popup="{!! $popup !!}" multiple>
      </span>
      <a href="{!! $indexLink !!}" class="btn btn-default btn-block margin-bottom">Voltar</a>
    </div>
    <div class="col-md-9">
      <div class="box box-solid">
        <div class="box-body table-responsive no-padding">
          <table class="table" id="uploads-table">
            <tbody></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  @stop

  @section('body.scripts')
  @parent
    <!-- Jquery-File-upload -->
  <script src="{!! asset('assets/admin/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js') !!}"></script>
  <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
  <script src="{!! asset('assets/admin/plugins/blueimp-load-image/js/load-image.all.min.js') !!}"></script>
  <!-- The Canvas to Blob plugin is included for image resizing functionality -->
  <script src="{!! asset('assets/admin/plugins/blueimp-canvas-to-blob/js/canvas-to-blob.min.js') !!}"></script>
  <script src="{!! asset('assets/admin/plugins/jquery-file-upload/js/jquery.iframe-transport.js') !!}"></script>
  <script src="{!! asset('assets/admin/plugins/jquery-file-upload/js/jquery.fileupload.js') !!}"></script>
  <script src="{!! asset('assets/admin/plugins/jquery-file-upload/js/jquery.fileupload-process.js') !!}"></script>
  <script src="{!! asset('assets/admin/plugins/jquery-file-upload/js/jquery.fileupload-image.js') !!}"></script>
  <script src="{!!asset('assets/admin/js/admin-file-upload.js')!!}" type="text/javascript"></script>
@stop
