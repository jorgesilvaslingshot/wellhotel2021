<tr>
  <td class="text-middle" width="1%">
    <a class="file-icon" href="{{ $row->getPath() }}" target="_blank">
      @if(in_array($row->getExtension(),['png','jpeg','jpg','gif']))
        <div class="b-lazy file-thumb" data-src="{!! $row->getPath() !!}"></div>
      @elseif($row->getExtension() == 'pdf')
        <i class="fa fa-file-pdf-o"></i>
      @elseif(in_array($row->getExtension(),['docx','doc']))
        <i class="fa fa-file-word-o"></i>
      @elseif(in_array($row->getExtension(),['xls','xlsx','csv']))
        <i class="fa fa-file-excel-o"></i>
      @else
        <i class="fa fa-file-o"></i>
      @endif
    </a>
  </td>
  <td class="text-middle text-left">
    <a href="{!! $row->getPath() !!}">{!! $row->getPath() !!}</a>
  </td>
  <td class="text-middle">{!! $row->getDate() !!}</td>
  @include('admin::datagrid.actions',['id'=>$row->getId()])
</tr>
