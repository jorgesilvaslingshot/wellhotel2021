{!! Form::boOpen($data) !!}

{!! Form::boTopicGroup('Dados da Conta') !!}

{!! Form::boText('name','Nome') !!}

{!! Form::boText('email','Email') !!}

{!! Form::boCloseTopicGroup() !!}

{!! Form::boTopicGroup('Alterar Palavra-passe') !!}

{!! Form::boBooleanGroup('update_password','Alterar?') !!}

{!! Form::boPassword('new_password','Nova Palavra-passe') !!}

{!! Form::boPassword('new_password_confirmation','Confirmar Palavra-passe') !!}

{!! Form::boCloseBooleanGroup() !!}

{!! Form::boCloseTopicGroup() !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
