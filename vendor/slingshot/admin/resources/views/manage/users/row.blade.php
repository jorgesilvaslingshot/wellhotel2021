<tr>
  <td width="1%">{!! $row->getId() !!}</td>
  <td>{!! $row->getName() !!}</td>
  <td>{!! $row->getEmail() !!}</td>
  <td>{!! $row->getUpdatedAt() !!}</td>
  @include('admin::datagrid.actions',['id'=>$row->getId()])
</tr>
