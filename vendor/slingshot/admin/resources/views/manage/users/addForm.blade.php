{!! Form::boOpen() !!}

{!! Form::boText('name','Nome') !!}

{!! Form::boText('email','Email') !!}

{!! Form::boPassword('password','Palavra-passe') !!}

{!! Form::boPassword('password_confirmation','Confirmação Palavra-passe') !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
