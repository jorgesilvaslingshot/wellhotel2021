@extends('admin::unlock-template')

@section('content')
  <h4 class="text-center">Redifinir a palavra-passe</h4>
  <br/>
  <form method="post" action="{!! route('admin.reset.post') !!}">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
    <input type="hidden" name="token" value="{{ $token }}">

    <div class="form-group has-feedback">
      <input type="text" name="email" value="{!! old('email') !!}" placeholder="Email" class="form-control"/>
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
      <input type="password" name="password" placeholder="Paravra-passe" class="form-control"/>
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
      <input type="password" name="password_confirmation" placeholder="Confirmar Paravra-passe" class="form-control"/>
      <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
    </div>
    <div class="row">
      <div class="col-xs-12 text-right">
        <button type="submit" class="btn btn-primary btn-flat">Guardar</button>
        <a href="{!! route('admin.login') !!}" class="btn btn-primary btn-flat">Cancelar</a>
      </div>
    </div>
  </form>
@stop
