@extends('admin::template')

@section('body-class','skin-blue')

@section('body')
  <!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <a href="{!! route('admin.dashboard') !!}" class="logo">{!! config('admin.site') !!}</a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li>
            <a href="/" target="_blank">
              <i class="glyphicon glyphicon-home"></i>
              <span class="hidden-xs">Ir para Website</span>
            </a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <i class="glyphicon glyphicon-user"></i>
              <span class="hidden-xs">{!! $user !!}</span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="{!! route('admin.preferences') !!}" class=""><i class="fa fa-wrench"></i> Preferências</a>
              </li>
              <li><a href="{!! route('admin.logout') !!}" class=""><i class="fa fa-sign-out"></i> Fechar sessão</a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  @include('admin::menu.navbar')

    <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      @yield('content-header')
    </section>

    <!-- Main content -->
    <section class="content">
      @include('admin::messages')
      @yield('content')
      {!! $content ?? '' !!}
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" style="background-color:#ecf0f5">

  </footer>
</div><!-- ./wrapper -->
@stop
