@extends($mainTemplate)

@section('content-header')
  <h1>{!! $title ?? '' !!}
    <small>{!! $subtitle ?? '' !!}</small>
  </h1>
@stop

@section('content')
  {!! $form ?? '' !!}
@stop
