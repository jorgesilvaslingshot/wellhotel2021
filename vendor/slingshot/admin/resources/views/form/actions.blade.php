<div class="form-group form-actions">
  <div class="col-sm-10 col-sm-offset-2">
    <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-check"></i> Guardar</button>
    <a href="{!! $url !!}" class="btn btn-default btn-flat"><i class="fa fa-times"></i> Cancelar</a>
  </div>
</div>
