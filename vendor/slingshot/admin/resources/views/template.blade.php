<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>{!! config('admin.site') !!}</title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  @section('header.scripts')
    <!-- Bootstrap 3.3.4 -->
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <!-- Font Awesome Icons -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"
        type="text/css"/>
  <!-- Ionicons -->
  <link href="https://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
  <!-- Selectize -->
  <link href="{!!asset('assets/admin/plugins/selectize/dist/css/selectize.bootstrap3.css')!!}" rel="stylesheet"
        type="text/css"/>
  <!-- Bootstrap Color Picker -->
  <link href="{!!asset('assets/admin//plugins/bootstrap-color-picker/css/bootstrap-colorpicker.min.css')!!}" rel="stylesheet"
            type="text/css"/>
  <!-- Datepicker  -->
  <link href="{!!asset('assets/admin/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css')!!}"
        rel="stylesheet" type="text/css"/>
  <!-- Theme style -->
  <link href="{!!asset('assets/admin/css/Adminlte.min.css')!!}" rel="stylesheet" type="text/css"/>
  <!-- AdminLTE Skin. -->
  <link href="{!!asset('assets/admin/css/skin-blue.min.css')!!}" rel="stylesheet" type="text/css"/>
  <!-- Custom Style -->
  <link href="{!!asset('assets/admin/css/admin.css')!!}" rel="stylesheet" type="text/css"/>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
  @show
</head>
<body class="@yield('body-class')">
@yield('body')
@section('body.scripts')
  <script>
    window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
    ]); ?>
  </script>
  <!-- jQuery 2.1.3 -->
<script src="{!!asset('assets/admin/plugins/jQuery/jQuery-2.1.3.min.js')!!}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Selectize -->
<script src="{!!asset('assets/admin/plugins/selectize/dist/js/standalone/selectize.min.js')!!}"
        type="text/javascript"></script>
<!-- Bootstrap Color Picker -->
<script src="{!!asset('assets/admin/plugins/bootstrap-color-picker/js/bootstrap-colorpicker.min.js')!!}"></script>
<!-- Tinymce -->
<script src="{!!asset('assets/admin/plugins/tinymce/tinymce.min.js')!!}" type="text/javascript"></script>
<script src="{!!asset('assets/admin/plugins/tinymce/langs/pt_PT.js')!!}" type="text/javascript"></script>
<!-- Datepicker -->
<script src="{!!asset('assets/admin/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')!!}"
        type="text/javascript"></script>
<script src="{!!asset('assets/admin/plugins/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt.min.js')!!}"
        type="text/javascript"></script>
<!-- bLazy -->
<script src="{!! asset('assets/admin/plugins/blazy/blazy.min.js') !!}"></script>
<!-- Bootbox -->
<script src="{!! asset('assets/admin/plugins/bootbox/bootbox.js') !!}"></script>
  <!-- InputMask -->
  <script src="{!! asset('assets/admin/plugins/input-mask/jquery.inputmask.js') !!}"></script>
  <script src="{!! asset('assets/admin/plugins/input-mask/jquery.inputmask.date.extensions.js') !!}"></script>
  <script src="{!! asset('assets/admin/plugins/input-mask/jquery.inputmask.extensions.js') !!}"></script>
<!-- AdminLTE App -->
<script src="{!!asset('assets/admin/js/app.min.js')!!}" type="text/javascript"></script>
<!-- admin js -->
<script src="{!!asset('assets/admin/js/admin.js')!!}" type="text/javascript"></script>
@show
</body>
</html>
