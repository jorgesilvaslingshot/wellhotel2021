@extends('admin::template')

@section('body-class','sidebar-collapse')

@section('body')
  <!-- Site wrapper -->
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      @yield('content-header')
    </section>

    <!-- Main content -->
    <section class="content">
      @include('admin::messages')
      @yield('content')
      {!! $content ?? '' !!}
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" style="background-color:#ecf0f5">

  </footer>
</div><!-- ./wrapper -->
@stop
