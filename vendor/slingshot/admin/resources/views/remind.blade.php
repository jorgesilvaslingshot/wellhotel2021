@extends('admin::unlock-template')

@section('content')
  <h4 class="text-center">Recuperar a palavra-passe</h4>
  <p class="login-box-msg">Indique o email da sua conta para receber o email para a recuperação da palavra-passe.</p>
  <form method="post" action="{!! route('admin.remind') !!}">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>

    <div class="form-group has-feedback">
      <input type="text" name="email" placeholder="Email" class="form-control"/>
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="row">
      <div class="col-xs-12 text-right">
        <button type="submit" class="btn btn-primary btn-flat">Enviar</button>
        <a href="{!! route('admin.login') !!}" class="btn btn-primary btn-flat">Cancelar</a>
      </div>
    </div>
  </form>
@stop
