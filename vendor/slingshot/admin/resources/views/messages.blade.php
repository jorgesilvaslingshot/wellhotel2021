@if (count($errors) > 0)
  <div class="alert alert-danger">
    @foreach ($errors->all() as $error)
      {!! $error !!}<br/>
    @endforeach
  </div>
@endif
<?php
$types = ['success', 'warning', 'info']
?>
@foreach($types as $type)
  @if (!is_null(session($type)) && count(session($type)) > 0)
    <div class="alert alert-{!! $type !!}">
      @foreach (session()->pull($type) as $msg)
        {!! $msg !!}<br/>
      @endforeach
    </div>
  @endif
@endforeach
