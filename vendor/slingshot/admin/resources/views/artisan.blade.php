@extends('admin::lock-template')

@section('content-header')
  <h1>Comandos artisan</h1>
@stop

@section('content')
  {!! Form::boOpen() !!}

  {!! Form::boSelect('command', 'Comando', $commands) !!}
  {!! Form::boText('arguments', 'Argumentos') !!}

  <div class="row form-actions">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-check"></i> Executar</button>
    </div>
  </div>

  {!! Form::boClose() !!}
@stop
