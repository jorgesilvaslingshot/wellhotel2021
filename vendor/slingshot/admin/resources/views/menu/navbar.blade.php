<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      @foreach($menus as $menu)
        @if($menu['isHeader'])
          <li class="header text-uppercase">{!! $menu['header'] !!}</li>
    @elseif($menu['isLink'] || $menu['isDropdown'])
      @include('admin::menu.navbar-item')
    @endif
    @endforeach
  </section>
  <!-- /.sidebar -->
</aside>
