<li class="{!! $menu['isDropdown']?'treeview':'' !!} {!! $menu['active']?'active':'' !!}">
  <a href="{!! $menu['isDropdown']?'#':action($menu['routeAction'].'@index') !!}">
    <i class="{!! $menu['icon'] !!}"></i>
    <span>{!! $menu['title'] !!}</span>
    @if($menu['isDropdown'])
      <i class="fa fa-angle-left pull-right"></i>
    @endif
    @if(!empty($menu['label']))
      <small class="label pull-right {!! $menu['labelClass'] !!}">{!! $menu['label'] !!}</small>
    @endif
  </a>

  @if($menu['isDropdown'])
    <ul class="treeview-menu">
      @foreach($menu['children'] as $children)
        @if($menu['isLink'] || $menu['isDropdown'])
          @include('admin::menu.navbar-item',['menu'=>$children])
        @endif
      @endforeach
    </ul>
  @endif
</li>

