@extends('admin::lock-template')

@section('content-header')
  <h1>Perfil</h1>
@stop

@section('content')
  {!! Form::boOpen($user) !!}

  {!! Form::boTopicGroup('Dados da Conta') !!}

  {!! Form::boPassword('password','Palavra-passe') !!}

  {!! Form::boText('name','Nome') !!}

  {!! Form::boText('email','Email') !!}

  {!! Form::boCloseTopicGroup() !!}

  {!! Form::boTopicGroup('Alterar Palavra-passe') !!}

  {!! Form::boBooleanGroup('update_password','Alterar?') !!}

  {!! Form::boPassword('new_password','Nova Palavra-passe') !!}

  {!! Form::boPassword('new_password_confirmation','Confirmar Palavra-passe') !!}

  {!! Form::boCloseBooleanGroup() !!}

  {!! Form::boCloseTopicGroup() !!}

  <div class="row form-actions">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-check"></i> Guardar</button>
    </div>
  </div>

  {!! Form::boClose() !!}
@stop
