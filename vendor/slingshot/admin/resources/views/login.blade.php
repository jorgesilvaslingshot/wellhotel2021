@extends('admin::unlock-template')

@section('content')
  <h4 class="text-center">Login</h4>
  <br/>
  <form method="post" action="{!! route('admin.login') !!}">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>

    <div class="form-group has-feedback">
      <input type="text" name="email" value="{!! old('email') !!}" placeholder="Email" class="form-control"/>
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
      <input type="password" name="password" placeholder="Palavra-passe" class="form-control"/>
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="row">
      <div class="col-xs-8 form-control-static">
        <a href="{!! route('admin.remind') !!}">Esqueceu-se da Palavra Passe?</a>
      </div>
      <div class="col-xs-4">
        <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
      </div>
    </div>
  </form>
@stop
