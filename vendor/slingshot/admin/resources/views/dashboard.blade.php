@extends('admin::lock-template')

@section('content-header')
  <h1>Bem-vindo(a)</h1>
@stop

@section('content')
  <p class="text-center lead">Bem vindo(a) ao Backoffice do website {!! config('admin.site') !!}.<br>Utilize o menu
    lateral esquerdo para administrar os vários conteúdos do site.</p>
@stop
