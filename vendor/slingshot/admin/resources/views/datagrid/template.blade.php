@extends($mainTemplate)

@section('content-header')
  <h1>{!! $title ?? '' !!}
    <small>{!! $subtitle ?? '' !!}</small>
  </h1>
@stop

@section('content')
  <div class="row">
    <div class="col-md-8 margin-bottom">
      @section('datagrid.toolbar')
        @include($datagridToolbarTemplate)
      @show
    </div>
    <div class="col-md-4 margin-bottom">
      @section('datagrid.search')
        @include('admin::datagrid.search')
      @show
    </div>
  </div>

  <div class="box box-solid">
    <div class="table-responsive">
      <table class="table table-striped text-center">
        @section('datagrid.header')
          @include('admin::datagrid.header')
        @show
        <tbody>
        @section('datagrid.content')
          @if(!empty($datagridRowTemplate))
            @each($datagridRowTemplate,$data,'row','admin::datagrid.empty')
          @endif
        @show
        </tbody>
      </table>
    </div>
  </div>

  @if(!empty($pagination))
    <nav class="text-center">
      {!! $pagination !!}
    </nav>
  @endif
@stop
