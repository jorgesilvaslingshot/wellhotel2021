<td class="text-nowrap text-middle" width="1%">
  @if($editLink)
    <a href="{!! sprintf($editLink,$id) !!}" class="btn btn-xs btn-primary" data-toggle="tooltip"
       data-placement="top" data-original-title="Editar">
      <i class="fa fa-pencil"></i>
    </a>
  @endif
  @if($deleteLink)
    <form method="POST" action="{!! sprintf($deleteLink,$id) !!}" accept-charset="UTF-8" style="display:inline"
          class="delete-form">
      <input name="_method" type="hidden" value="DELETE">
      <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
      <button type="submit" class="btn btn-danger btn-xs"
              data-toggle="tooltip" data-placement="top" data-original-title="Eliminar">
        <i class="fa fa-times"></i>
      </button>
    </form>
  @endif
</td>
