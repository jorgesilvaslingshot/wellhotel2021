@if(!empty($search_columns) && !empty($search_by))
  <form>
    <div class="input-group">
      <div class="input-group-btn">
        <div class="btn-group dropdown-select">
          <a href="#" type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown"
             aria-expanded="false">
            <span class="dropdown-text">{{ $columns[$search_by] }}</span>
            <input class="dropdown-value" type="hidden" name="search_by" value="{!! $search_by !!}"/>
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            @foreach($search_columns as $column)
              <li><a href="#" data-value="{{ $column }}">{{ $columns[$column] }}</a></li>
            @endforeach
          </ul>
        </div>
      </div>

      <div class="form-group {!! !empty($search)?'has-feedback':'' !!}">
        <input type="text" name="search" class="form-control" value="{!! $search !!}"/>
        @if(!empty($search))
          <a class="form-control-feedback" type="button" href="{{ $url_path }}">
            <small class="glyphicon glyphicon-remove text-muted"></small>
          </a>
        @endif
      </div>
      <div class="input-group-btn">
        <button class="btn btn-default btn-flat"><i class="glyphicon glyphicon-search"></i></button>
      </div>
    </div>
  </form>
@endif
