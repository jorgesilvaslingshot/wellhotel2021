@if($createLink)
  <a href="{{ $createLink }}" class="btn btn-primary btn-flat">
    <i class="fa fa-plus"></i> Adicionar
  </a>
@endif
@yield('toolbar.content')
