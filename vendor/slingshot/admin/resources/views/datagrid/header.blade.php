<thead>
<tr>
  @foreach($columns as $key => $column)
    <th class="text-nowrap">
      @if(!in_array($key,$sort_columns))
        <span>{!! $column !!}</span>
      @else
        <a
          href="?{!! http_build_query(array_merge($query,['sort_by'=>$key,'sort'=>($key==$sort_by && $sort=='asc'?'desc':'asc')])) !!}">
          {!! $column !!}
          @if($key==$sort_by && $sort=='asc')
            <i class="fa fa-sort-asc"></i>
          @elseif($key==$sort_by && $sort=='desc')
            <i class="fa fa-sort-desc"></i>
          @else
            <i class="fa fa-sort"></i>
          @endif
        </a>
      @endif
    </th>
  @endforeach
  <th></th>
</tr>
</thead>
