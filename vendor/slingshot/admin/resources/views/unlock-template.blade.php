@extends('admin::template')

@section('body-class','login-page')

@section('body')
  <div class="login-box">
    <div class="login-logo">
      <span>Backoffice</span>
    </div>
    <!-- /.login-logo -->
    @if($error = Session::get('error'))
      <div class="alert alert-danger">
        {!! $error !!}
      </div>
    @endif
    @if($success = Session::get('success'))
      <div class="alert alert-success">
        {!! $success !!}
      </div>
    @endif
    <div class="login-box-body">
      <br>
      @yield('content')
      <br>
    </div>
    <!-- /.login-box-body -->
  </div><!-- /.login-box -->
@stop
