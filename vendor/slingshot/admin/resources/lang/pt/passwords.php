<?php

return [
    "password" => "A palavra-passe deverá conter pelo menos seis carateres e ser igual à confirmação.",
    "user" => "Não existe nenhum utilizador com o email indicado.",
    "token" => "Este código de recuperação da palavra-passe é inválido.",
    "sent" => "O lembrete para a palavra-passe foi enviado!",
    "reset" => "A palavra-passe foi redefinida!",
];
