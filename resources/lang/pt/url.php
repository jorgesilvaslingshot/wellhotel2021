<?php

return [
    'contacts' => '/pt/contactos',
    'privacy-policy' => '/pt/politica-de-privacidade',
];
