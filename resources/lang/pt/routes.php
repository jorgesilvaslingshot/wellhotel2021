<?php

return [
    'welfare'=> 'bem-estar',
    'about' => 'hotel',
    'spaces.detail' => 'espacos/detalhe/{space}',
    'rooms.list' => 'quartos-e-suites',
    'rooms.detail' => 'quartos-e-suites/detalhe/{room}',
    'experiences.list' => 'packs-experiencia',
    'experiences.detail' => 'packs-experiencia/detalhe/{experience}',
    'packs.list' => 'packs-experiencias',
    'packs.detail' => 'packs-experiencias/detalhe/{pack}',
    'amenities' => 'comodidades',
    'contacts' => 'contactos',
    'privacy-policy' => 'politica-de-privacidade',
    'booking' => 'reservar',
    'hotel-restaurant' => 'hotel-restaurante',
    'hotel-bar' => 'hotel-bar',
    'eco'=> 'eco',
    'event'=> 'eventos',
    'event-proposal'=>'pedido-proposta-evento',
    'booking-voucher'=>'pedido-info-voucher',
    'sustainability'=>'sustentabilidade'
];
