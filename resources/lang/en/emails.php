<?php

return [
    'subjects' => [
        'availability-request-auto-response' => 'Well Hotel & Spa - Pedido de disponibilidade',
        'proposal-request-auto-response' => 'Well Hotel & Spa - Pedido de Proposta',
        'voucher-request-auto-response' => 'Well Hotel & Spa - Pedido de Informação de Voucher'
    ]
];
