<?php

return [
    'welfare'=> 'welfare',
    'about' => 'hotel',
    'spaces.detail' => 'spaces/detail/{space}',
    'rooms.list' => 'rooms-and-suites',
    'rooms.detail' => 'rooms-and-suites/detail/{room}',
    'experiences.list' => 'packs-experience',
    'experiences.detail' => 'packs-experience/detail/{experience}',
    'packs.list' => 'packs-experiences',
    'packs.detail' => 'packs-experiences/detail/{pack}',
    'amenities' => 'amenities',
    'contacts' => 'contacts',
    'privacy-policy' => 'privacy-policy',
    'hotel-restaurant' => 'hotel-restaurant',
    'hotel-bar' => 'hotel-bar',
    'eco'=> 'eco',
    'event'=> 'events',
    'event-proposal'=>'event-proposal',
    'booking-voucher'=>'voucher-info-request',
    'sustainability'=>'sustainability'
];
