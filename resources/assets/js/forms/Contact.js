import Form from 'form-backend-validation';

export default {
    data() {
        return {
            form: new Form({
                name: '',
                email: '',
                message: '',
            }),
            message: '',
            messageClass: '',
            placeholderSupported: true
        };
    },

    props: ['method', 'action'],

    methods: {
        onSubmit() {
            this.clearMessage();

            this.form[this.method](this.action)
                .then(response => this.displaySuccessMessage(response.message));
        },

        displaySuccessMessage(message) {
            this.messageClass = 'alert-success';
            this.message = message;
            this.$el.reset();
            this.form.reset();
        },

        clearMessage() {
            this.message = '';
        },
    },

    mounted() {
        let test = document.createElement('input');
        this.placeholderSupported = 'placeholder' in test;
    }
};
