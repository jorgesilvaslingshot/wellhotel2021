import Form from 'form-backend-validation';
import moment from 'moment';
import axios from 'axios';
import {Portuguese} from 'flatpickr/dist/l10n/pt.js';

//console.log(vacationData);

export default {
    data() {
        return {
            form: new Form({
                event_type: '',
                voucher_type: '',
                label_name: '',
                title: '',
                name: '',
                email: '',
                phone: '',
                arrival: moment().format('YYYY-MM-DD'),
                departure: moment().add(1, 'days').format('YYYY-MM-DD'),
                adults: 2,
                start_time: moment().format('H:i'),
                end_time: moment().add(1, 'hours').format('H:i'),
                children: 0,
                product_type: 'room',
                product_id: '',
                product_name: '',
                description: '',
                product_max_guests: 0,
                price: '',
            }),
            voucher_types: [],
            event_types: [],
            products: [],
            message: false,
            flatpickrConfigArrival: {
                minDate: 'today',
                maxDate: moment().add(1, 'year').format('YYYY-MM-DD'),
                static: true,
                wrap: true,
                position: 'below',
                locale: Portuguese,
                disable: this.vacationData
            },
            flatpickrConfigDeparture: {
                minDate: moment().add(1, 'days').format('YYYY-MM-DD'),
                maxDate: moment().add(1, 'year').format('YYYY-MM-DD'),
                static: true,
                wrap: true,
                position: 'below',
                locale: Portuguese,
                disable: this.vacationData
            },
//moment().add(1, 'hours').format('H:i')
            flatpickrConfigStart: {
                time_24hr: true,
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
                //minTime: moment().format('H:i'),
                //maxTime: "00:00",
            },
            flatpickrConfigEnd: {
                time_24hr: true,
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",

                //minTime: moment().add(1, 'hours').format('H:i'),
                // maxTime: "00:00",
            },
            placeholderSupported: true

        };
    },

    props: ['method', 'action', 'productsUrl', 'priceUrl', 'initialId', 'initialType', 'vacationData', 'voucherId'],

    computed: {
        currentProducts() {
            return this.products[this.form.product_type] || [];
        },
        currentEventTypes() {
            return this.event_types || [];
        },
        currentVoucherTypes() {
            return this.voucher_types || [];
        },

        displayPrice() {
            if (!this.form.price) {
                return 'S/N';
            }

            return `${this.form.price}<sup>€</sup>`;
        },
        formWatchable() {
            this.form.product_name;
            this.form.product_type;
            this.form.arrival;
            this.form.date;
            this.form.departure;

            return Date.now();
        },
        maxNumbers() {

            let options = {};

            for (let i = 1; i <= 20; i++) {
                let option = {};

                if(i<10)
                option[i] = `0${i}`;
                else
                option[i] = `${i}`;

                options = Object.assign({}, options, option);
            }

            return options;
        },
        adultNumbers() {
            let max = this.form.product_max_guests - this.form.children;
            let options = {};

            if (max < 1) {
                max = 1;
            }

            for (let i = 1; i <= max; i++) {
                let option = {};

                option[i] = `0${i}`;

                options = Object.assign({}, options, option);
            }

            return options;
        },
        childrenNumbers() {
            let max = this.form.product_max_guests - this.form.adults;
            let options = {};

            if (max < 0) {
                max = 0;
            }

            for (let i = 0; i <= max; i++) {
                let option = {};

                option[i] = `0${i}`;

                options = Object.assign({}, options, option);
            }

            return options;
        },
    },

    methods: {
        fetchVoucherTypes() {
            axios.get('/api/voucher-type')
                .then(response => {

                    // console.log('voucherId id: '+this.voucherId)
                    // console.log(response.data[0][this.voucherId]);

                    this.voucher_types = response.data[0];

                    //this.form.voucher_type = response.data[0][this.voucherId]['label'];

                    // this.voucher_types.forEach((product) => {
                    //     console.log(JSON.stringify(product))
                    // });

                    // console.log('vouvher id value: '+this.voucherId);

                    this.form.voucher_type = this.voucher_types.filter(voucher => voucher.voucher_id == this.voucherId)[0].label;

                    this.form.label_name = this.voucher_types.filter(voucher => voucher.voucher_id == this.voucherId)[0].name;

                })
                .catch(e => {
                    console.log(e);
                });
        },

        fetchEventTypes() {
            axios.get('/api/event-type')
                .then(response => {

                    this.event_types = response.data[0];

                    this.selectDefaultProduct();
                })
                .catch(e => {
                    console.log(e);
                });
        },

        fetchProducts() {
            axios.get(this.productsUrl)
                .then(response => {
                    this.products = response.data;

                    this.selectDefaultProduct();
                })
                .catch(e => {
                    console.log(e);
                });
        },

        onSubmit() {
            this.clearMessage();

            this.form[this.method](this.action)
                .then(response => this.displaySuccessMessage(response.message));
        },

        displaySuccessMessage() {
            this.message = true;
            this.$el.reset();
            this.form.reset();
        },

        clearMessage() {
            this.message = false;
        },

        selectDefaultProduct() {
            let id = this.initialId || null;

            if (!id || !this.products[this.form.product_type]) return;

            this.products[this.form.product_type].forEach((product) => {
                if (product.id === id) {
                    this.form.product_id = product.id;
                }
            });
        },

        clearProduct() {
            this.form.product_id = '';
        },

        calculatePrice() {
            if (!this.form.product_type || !this.form.product_id) {
                this.form.price = null;
                return;
            }

            axios.get(this.priceUrl, {
                params: {
                    product_type: this.form.product_type,
                    product_id: this.form.product_id,
                    arrival: this.form.arrival,
                    departure: this.form.departure
                }
            })
                .then(response => {
                    this.form.price = response.data;
                })
                .catch(e => {
                    console.log(e);
                });
        },

        clearPrice() {
            this.form.price = null;
        }
    },

    watch: {
        'form.product_type': function () {
            this.clearProduct();
        },
        'form.product_id': function (id) {
            if (!id) {
                this.form.product_name = '';
                this.form.product_max_guests = 0;
                return;
            }

            this.form.adults = 1;
            this.form.children = 0;

            this.form.product_name = this.currentProducts.filter(product => product.id === id)[0].name;
            this.form.product_max_guests = this.currentProducts.filter(product => product.id === id)[0].max_guests;
        },
        'form.arrival': function (arrival) {
            if (moment(arrival).isSameOrAfter(this.form.departure, 'day')) {
                this.form.departure = moment(arrival).add(1, 'days').format('YYYY-MM-DD');
            }
        },
        'form.departure': function (departure) {
            if (moment(departure).isSameOrBefore(this.form.arrival, 'day')) {
                this.form.arrival = moment(departure).subtract(1, 'days').format('YYYY-MM-DD');
            }
        },
        'form.start_time': function (start_time) {
            this.form.start_time = moment(start_time, "HH:mm").format("HH:mm");
        },
        'form.end_time': function (end_time) {
            this.form.end_time = moment(end_time, "HH:mm").format("HH:mm");
        },
        'form.voucher_type': function (voucher_type) {
            this.form.label_name = this.currentVoucherTypes.filter(voucher => voucher.label === voucher_type)[0].name;
        },
        formWatchable: function () {
            this.calculatePrice();
        },
    },

    filters: {
        formatDate: function (value) {
            if (!value) return '';
            return moment(value).format('DD.MM.YYYY');
        },

        formatHour: function (value) {
            if (!value) return '';

            return moment(value, "HH:mm").format("HH:mm")
            //return moment(value).format('H:i');
        },

        limitOverflow: function (value, limit = 35) {

            if (value.length < limit) {
                return value;
            }

            return value.substring(0, limit) + '...';
        },
        pad: function(value) {

            if(value < 10)
            return `0${value}`;
            else
            return `${value}`;
        }
    },

    created() {
        this.form.product_type = this.initialType || 'room';

        this.fetchProducts();

        this.fetchEventTypes();

        this.fetchVoucherTypes();
    },

    mounted() {
        let test = document.createElement('input');
        this.placeholderSupported = 'placeholder' in test;
    }
};
