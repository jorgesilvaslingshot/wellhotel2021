/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

console.log(document.getElementById('#modal-covid19'))

import Blazy from 'blazy';

Vue.prototype.$blazy = new Blazy({
    breakpoints: [
        {width: 768, src: 'data-src-sm'},
    ],
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import gallery from './plugins/gallery';

Vue.use(gallery);
Vue.component('modal-covid', require('./components/ModalCovid.vue'));
Vue.component('my-top-slider', require('./components/TopSlider.vue'));
Vue.component('my-promotion', require('./components/Promotion.vue'));
Vue.component('my-bottom-slider', require('./components/BottomSlider.vue'));
Vue.component('my-comment-slider', require('./components/CommentSlider.vue'));
Vue.component('my-gallery-slider', require('./components/GallerySlider.vue'));
Vue.component('my-quotes-slider', require('./components/QuotesSlider.vue'));
Vue.component('my-contacts-map', require('./components/ContactsMap.vue'));
Vue.component('my-gallery-link', require('./components/GalleryLink.vue'));
Vue.component('my-scroll-to', require('./components/ScrollTo.vue'));
Vue.component('my-top-nav', require('./components/TopNav.vue'));
Vue.component('my-submenu', require('./components/Submenu.vue'));
Vue.component('my-drag-content', require('./components/DragContent.vue'));
Vue.component('flat-pickr', require('./components/FlatPickr.vue'));
Vue.component('my-booking', require('./components/Booking.vue'));

Vue.component('my-contact-form', require('./forms/Contact').default);
Vue.component('my-booking-form', require('./forms/Booking').default);

var bodyScrollTop;

function measureScrollbar() {
    let scrollDiv = document.createElement('div');
    scrollDiv.className = 'scrollbar-measure';
    document.body.appendChild(scrollDiv);

    let scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;

    document.body.removeChild(scrollDiv);

    return scrollbarWidth;
}

function setBodyUnscrollable(value) {
    let scrollbarWidth = measureScrollbar();

    if (value) bodyScrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    document.body.style.overflow = (value) ? 'hidden' : '';
    document.body.style.position = (value) ? 'fixed' : '';
    document.body.style.left = (value) ? '0' : '';
    document.body.style.right = (value) ? '0' : '';
    document.body.style.top = (value) ? -bodyScrollTop + 'px' : '';
    document.body.style.paddingRight = (value) ? scrollbarWidth + 'px' : '';

    $('.top-nav, .submenu.stuck').css('margin-right', (value) ? scrollbarWidth + 'px' : '');

    if(value) {
        Waypoint.disableAll();
    } else {
        Waypoint.enableAll();
    }

    if (!value) window.scrollTo(0, bodyScrollTop);
}

// eslint-disable-next-line no-unused-vars
const app = new Vue({
    el: '#app',
    data() {
        return {
            scrollbarWidth: 0,
            bodyScroll: true,
            showMenuSection: false,
            showBookingSection: false,
            productId: null,
            productType: null,
            showModalCovidß: '',
            pageY: 0,
        };
    },
    computed: {
        fixedElementStyle() {
            return {
                'margin-right': !this.bodyScroll ? this.scrollbarWidth + 'px' : ''
            };
        },
    },
    methods: {
        toggleMenu() {
            if (this.showMenuSection) {
                this.hideMenu();
            } else {
                this.showMenu();
            }

        },
        showMenu() {
            this.showMenuSection = true;

            this.setBodyUnscrollable(true);
        },
        hideMenu() {
            this.showMenuSection = false;

            window.setTimeout(() => {
                this.setBodyUnscrollable(false);
            }, 500);
        },
        openBooking(id = null, type = null) {
            this.showBookingSection = true;
            this.productId = id;
            this.productType = type;

            this.setBodyUnscrollable(true);
        },

        openBookingRoom(id) {
            this.openBooking(id, 'room');
        },

        openBookingPack(id) {
            this.openBooking(id, 'pack');
        },

        closeBooking() {
            this.showBookingSection = false;

            window.setTimeout(() => {
                this.setBodyUnscrollable(false);
            }, 500);
        },

        setBodyUnscrollable(value) {
            this.bodyScroll = !value;

            setBodyUnscrollable(value);
        },
        scrollToTopMethod(){
            console.log('method scroll to top work');

            //this.scrollToTop(true);
            window.scrollTo(0, 0);
        },
        handleScroll(){
            document.getElementById('scrollToTopBtn').style.display = "display";
        }
    },
    mounted() {
        this.scrollbarWidth = measureScrollbar();
        this.scrollToTopMethod();

        window.addEventListener('scroll', function(e){
            var scrollPos = window.scrollY

            if(scrollPos > 500){
                document.getElementById('scrollToTopBtn').style.display = "";
            }else{
                document.getElementById('scrollToTopBtn').style.display = "none";
            }

          });
    }
});
