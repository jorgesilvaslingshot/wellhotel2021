import GalleryComponent from '../components/GalleryModal.vue';


let $vm;

export default {
    install (Vue) {
        const Gallery = Vue.extend(GalleryComponent);

        if (!$vm) {
            $vm = new Gallery({el: document.createElement('div')});
            document.body.appendChild($vm.$el);
        }

        Vue.$gallery = {
            open (images, title, index) {
                $vm.open(images, title, index);
            },
            close () {
                $vm.close();
            }
        };

        Vue.mixin({
            created () {
                this.$gallery = Vue.$gallery;
            }
        });
    }
};
