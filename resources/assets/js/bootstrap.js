/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap-sass');
} catch (e) {} // eslint-disable-line no-empty

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Promises
 */

require('es6-promise').polyfill();

require('raf').polyfill();

/**
 * Load fonts asynchronously using https://github.com/typekit/webfontloader
 */

import WebFont from 'webfontloader';

WebFont.load({
    google: {
        families: ['Raleway:300,400,500,600,700', 'Merriweather:300,300i,400', 'Libre+Baskerville:400,400i']
    },
    // Para carregar uma fonte costumizada, criar outro css com essa fonte
    // e fazer referência nas variáveis globais
    // custom: {
    //     families: ['icomoon'],
    //     urls: [window.Fonts.icons]
    // },
    active() {
        // Loaded once, fonts should be cached
        localStorage.fonts = true;
    }
});

/**
 * Svg
 */
import 'svgxuse';
import '../icomoon/symbol-defs.svg';

/**
 *
 * Waypoints
 */

import 'waypoints/lib/noframework.waypoints.js';

import 'classlist-polyfill';

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(search, pos) {
        return this.substr(!pos || pos < 0 ? 0 : +pos, search.length) === search;
    };
}

/**
 *
 * Addthis
 */

const addThisScript = document.createElement('SCRIPT');
addThisScript.setAttribute('src', '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5acc7f73026d13ee');
addThisScript.setAttribute('async', '');
addThisScript.setAttribute('defer', '');
document.body.appendChild(addThisScript);

window.addthis_config = {
    services_expanded: 'facebook,twitter,print,google_plusone_share,email,whatsapp,linkedin,favorites,pinterest_share,blogger',
};


