<!doctype html>
<html>

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <meta content="width=device-width, initial-scale=1" name="viewport" />

    <style>
        html,
        body {
            background-color: black;
            color: #B4985A !important;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 5px;


        }

        .full-height {
            height: 100vh;
        }

        .icon {

            fill: #B4985A !important;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 10px;
        }

        .m-b-md {
            margin-bottom: 30px;
        }


        @media only screen and (min-width: 601px) and (max-width: 2900px) {

            .mobile {
                visibility: hidden !important;
            }

            .desktop {
                visibility: visible !important;
            }
        }


        @media screen and (max-width: 600px) {

            .desktop {
                visibility: hidden !important;
                margin-top: -20px !important;
            }

            .mobile {
                visibility: visible !important;
            }

        }
    </style>
</head>

<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            <svg class="icon icon-logo-well">
                <use xmlns:xlink="http://www.w3.org/1999/xlink"
                     xlink:href="/assets/img/symbol-defs.svg#icon-logo-well"></use>
            </svg>

            <div class="desktop" style="font-size: 34px; margin: 65px 0px 30px 0px;">
                Website em manutenção | Website under maintenance
            </div>

            <div class="desktop" style="font-size: 18px;">
                O nosso hotel encontra-se encerrado para modernização. Dia 27 de março voltamos a abrir portas. |
                Our hotel is now closed for modernization. March 27th we open doors again.

            </div>

            <div class="mobile"
                 style="font-size: 22pt; font-family: Raleway, semibold; margin: -190px 0px 50px 0px !important;">
                <p>Website em manutenção</p>
                <p style="margin-top: -10px;">Website under maintenance</p>
            </div>


            <div class="mobile" style="font-size: 14pt; font-family: Raleway, regular;">

                <p>
                    O nosso hotel encontra-se encerrado para modernização. Dia 27 de março voltamos a abrir portas 222.
                </p>
                <p>
                    Our hotel is now closed for modernization. March 27th we open doors again.

                </p>

            </div>

        </div>

    </div>
</div>
</body>

</html>