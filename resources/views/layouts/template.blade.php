<!doctype html>
<html lang="{{ $locale }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="preconnect" href="//fonts.gstatic.com">

    {{ $seo }}

    @section('site.assets.head')
        <link href="{{ mix('assets/css/app.css') }}" rel="stylesheet">
        <!--[if IE 9]>
        <link rel="stylesheet" type="text/css" href="https://npmcdn.com/flatpickr/dist/ie.css">
        <![endif]-->
        <script>
            if (localStorage.fonts) {
                document.documentElement.className += ' wf-active';
            }
        </script>
    @show
</head>
<body>
<div id="app">
    @include('partials.header')

    @yield('site.content')

    @include('partials.footer')

    @include('partials.covid-19')

</div>

@section('site.assets.body')
    <script src="{{ mix('assets/js/manifest.js') }}" defer></script>
    <script src="{{ mix('assets/js/app.js') }}" defer></script>
    @include('partials.analytics')
@show
</body>
</html>
