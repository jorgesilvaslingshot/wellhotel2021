@component('mail::message')
# Pedido de Informação de Voucher

Nome: {{ $voucherRequest->getName() }}  
Email: {{ $voucherRequest->getEmail() }}  
Telefone: {{ $voucherRequest->getPhone() }}  

Tipo de Voucher: {{$voucherRequest->getVoucherType()}}  
Adultos: {{ $voucherRequest->getAdults() }}  
Crianças: {{ $voucherRequest->getChildren() }}  
Descrição: {{ $voucherRequest->getDescription() }}
<br>
Idioma: {{ $voucherRequest->getLocale() }}
@endcomponent
