@component('mail::message')
# Pedido de Disponibilidade

Nome: {{ $availabilityRequest->getName() }}  
Email: {{ $availabilityRequest->getEmail() }}  
Telefone: {{ $availabilityRequest->getPhone() }}  

{{ $availabilityRequest->getProductType() == 'room' ? 'Quarto' : 'Pack' }}: {{ $availabilityRequest->getProductName() }}      
Chegada: {{ $availabilityRequest->getArrival()->format('d-m-Y') }}  
Partida: {{ $availabilityRequest->getDeparture()->format('d-m-Y') }}  
Adultos: {{ $availabilityRequest->getAdults() }}  
Crianças: {{ $availabilityRequest->getChildren() }}  
Preço (com IVA): {{ $availabilityRequest->getPrice().(!empty($availabilityRequest->getPrice()) ? '€' : '') }}  
Idioma: {{ $availabilityRequest->getLocale() }}
@endcomponent
