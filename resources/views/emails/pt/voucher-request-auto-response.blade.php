@component('mail::message')
Após verificar o seu pedido de informação de voucher entraremos em contacto consigo. <br>

Para mais esclarecimentos não hesite em contactar para {{setting('contact_phone')}} ou {{setting('availability-request-emails')}}.
<br>
Muito obrigado pela sua escolha!

# Resumo do pedido
Nome: {{ $voucherRequest->getName() }}<br>
Email: {{ $voucherRequest->getEmail() }}<br>
Telefone: {{ $voucherRequest->getPhone() }}<br>

Tipo de Voucher: {{$voucherRequest->getVoucherType()}}<br>
Adultos: {{ $voucherRequest->getAdults() }}<br>
Crianças: {{ $voucherRequest->getChildren() }}<br>
Descrição: {{ $voucherRequest->getDescription() }}<br>
Idioma: {{ $voucherRequest->getLocale() }}
@endcomponent
