@component('mail::message')
Após verificar o seu pedido de proposta entraremos em contacto consigo. <br>

Para mais esclarecimentos não hesite em contactar para {{setting('contact_phone')}} ou {{setting('availability-request-emails')}}.
<br>
Muito obrigado pela sua escolha!

# Resumo do pedido
Nome: {{ $proposalRequest->getName() }}<br>
Email: {{ $proposalRequest->getEmail() }}<br>
Telefone: {{ $proposalRequest->getPhone() }}<br>

Tipo de Evento: {{$proposalRequest->getEventType()}}<br>
Data Pedido:  {{$proposalRequest->getArrival()}}<br>
Horário de Início: {{ $proposalRequest->getStartTime() }}<br>
Horário de Fim: {{ $proposalRequest->getEndTime() }}<br>
Adultos: {{ $proposalRequest->getAdults() }}<br>
Crianças: {{ $proposalRequest->getChildren() }}<br>
Descrição: {{ $proposalRequest->getDescription() }}<br>
Idioma: {{ $proposalRequest->getLocale() }}
@endcomponent
