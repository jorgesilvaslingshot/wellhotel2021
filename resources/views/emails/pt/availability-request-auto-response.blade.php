@component('mail::message')
Após verificar a disponibildade do seu pedido de reserva entraremos em contacto consigo. <br>

Para mais esclarecimentos não hesite em contactar para {{setting('contact_phone')}} ou {{setting('availability-request-emails')}}.
<br>
Muito obrigado pela sua escolha!

# Resumo do pedido
Nome: {{ $availabilityRequest->getName() }} <br>
Email: {{ $availabilityRequest->getEmail() }} <br>
Telefone: {{ $availabilityRequest->getPhone() }} <br>

{{ $availabilityRequest->getProductType() == 'room' ? 'Quarto' : 'Pack' }}: {{ $availabilityRequest->getProductName() }} <br>
Chegada: {{ $availabilityRequest->getArrival()->format('d-m-Y') }} <br>
Partida: {{ $availabilityRequest->getDeparture()->format('d-m-Y') }} <br>
Adultos: {{ $availabilityRequest->getAdults() }} <br>
Crianças: {{ $availabilityRequest->getChildren() }} <br>
Preço (com IVA): {{ $availabilityRequest->getPrice().(!empty($availabilityRequest->getPrice()) ? '€' : '') }} <br>
Idioma: {{ $availabilityRequest->getLocale() }}
@endcomponent
