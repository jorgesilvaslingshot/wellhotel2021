@component('mail::message')
# Pedido de Proposta

Nome: {{ $proposalRequest->getName() }}  
Email: {{ $proposalRequest->getEmail() }}  
Telefone: {{ $proposalRequest->getPhone() }}  

Tipo de Evento: {{$proposalRequest->getEventType()}}  
Data Pedido:  {{$proposalRequest->getArrival()}}  
Horário de Início: {{ $proposalRequest->getStartTime() }}  
Horário de Fim: {{ $proposalRequest->getEndTime() }}  
Adultos: {{ $proposalRequest->getAdults() }}  
Crianças: {{ $proposalRequest->getChildren() }}  
Descrição: {{ $proposalRequest->getDescription() }}
<br>
Idioma: {{ $proposalRequest->getLocale() }}
@endcomponent
