@component('mail::message')
# Pedido de Contacto

Nome: {{ $contactRequest->getName() }}  
Email: {{ $contactRequest->getEmail() }}  
Messagem: {{ $contactRequest->getMessage() }}  
Idioma: {{ $contactRequest->getLocale() }}  
@endcomponent
