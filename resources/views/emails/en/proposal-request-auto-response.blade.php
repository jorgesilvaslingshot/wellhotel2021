@component('mail::message')
After verifying your proposal request we will contact you.
For further questions please contact us on the {{setting('contact_phone')}} or at {{setting('availability-request-emails')}}.
<br>
Thank you very much for your preference!

# Your request

Name: {{ $proposalRequest->getName() }}
Email: {{ $proposalRequest->getEmail() }}
Telephone: {{ $proposalRequest->getPhone() }}

<br>

Event Type: {{$proposalRequest->getEventType()}}
Request Date:  {{$proposalRequest->getArrival()}}
Start Time: {{ $proposalRequest->getStartTime() }}
End Time: {{ $proposalRequest->getEndTime() }}
Adults: {{ $proposalRequest->getAdults() }}
Childrens: {{ $proposalRequest->getChildren() }}
Description: {{ $proposalRequest->getDescription() }}
<br>
Idiom: {{ $proposalRequest->getLocale() }}
@endcomponent
