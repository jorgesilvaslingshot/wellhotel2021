@component('mail::message')
After verifying your reservation request availability we will contact you.
For further questions please contact us on the {{setting('contact_phone')}} or at {{setting('availability-request-emails')}}.
<br>
Thank you very much for your preference!

# Your request
Name: {{ $availabilityRequest->getName() }}<br>
Email: {{ $availabilityRequest->getEmail() }}<br>
Telephone: {{ $availabilityRequest->getPhone() }}<br>

{{ $availabilityRequest->getProductType() == 'room' ? 'Room' : 'Pack' }}: {{ $availabilityRequest->getProductName() }}<br>
Arrival: {{ $availabilityRequest->getArrival()->format('d-m-Y') }}<br>
Departure: {{ $availabilityRequest->getDeparture()->format('d-m-Y') }}<br>
Adults: {{ $availabilityRequest->getAdults() }}<br>
Children: {{ $availabilityRequest->getChildren() }}<br>
Rate (with taxes and fees): {{ $availabilityRequest->getPrice().(!empty($availabilityRequest->getPrice()) ? '€' : '') }}<br>
Idiom: {{ $availabilityRequest->getLocale() }}
@endcomponent
