@component('mail::message')
After verifying your voucher information request we will contact you.
For further questions please contact us on the {{setting('contact_phone')}} or at {{setting('availability-request-emails')}}.
<br>
Thank you very much for your preference!

# Your request

Name: {{ $voucherRequest->getName() }}
Email: {{ $voucherRequest->getEmail() }}
Telephone: {{ $voucherRequest->getPhone() }}

<br>

Voucher Type: {{$voucherRequest->getVoucherType()}}
Adults: {{ $voucherRequest->getAdults() }}
Childrens: {{ $voucherRequest->getChildren() }}
Description: {{ $voucherRequest->getDescription() }}
<br>
Idiom: {{ $voucherRequest->getLocale() }}
@endcomponent
