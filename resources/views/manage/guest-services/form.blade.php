{!! Form::boOpen($data) !!}


@foreach ($languages as $language)
    <?php $translation = $data->translateOrNew($language) ?>
    {!! Form::boTopicGroup(strtoupper($language)) !!}
    {!! Form::boTextarea("languages[$language][title]", 'Título', $translation->title, ['rows' => 2]) !!}
    {!! Form::boTexteditor("languages[$language][description]", 'Descrição', $translation->description) !!}
    {!! Form::boCloseTopicGroup() !!}
@endforeach

{!! Form::boText('priority','Prioridade', $data->priority, array() ,array('sizes' => 'col-sm-3 col-md-1')) !!}
{!! Form::boBoolean('publish', 'Publicado') !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
