{!! Form::boOpen($data) !!}

{!! Form::boTopicGroup('Contactos') !!}

{!! Form::boTextarea('address', 'Morada', null, ['rows' => 3]) !!}

{!! Form::boText('facebook_link', 'Link Facebook') !!}

{!! Form::boText('instagram_link', 'Link Instagram') !!}

{!! Form::boText('contact_email', 'Email contacto') !!}

{!! Form::boText('contact_phone', 'Telefone contacto') !!}

{!! Form::boText('contact_mobile_phone', 'Telemóvel contacto') !!}

{!! Form::boText('directions_link', 'Link Direcções') !!}

{!! Form::boCloseTopicGroup() !!}

{!! Form::boTopicGroup('Emails') !!}

{!! Form::boTags('contact-request-emails', 'Pedido de Contacto') !!}
{!! Form::boTags('availability-request-emails', 'Pedido de disponibilidade') !!}

{!! Form::boCloseTopicGroup() !!}

{!! Form::boTopicGroup('Descontos') !!}

{!! Form::boText('room_discount', 'Desconto Quartos', null, [], ['help' => '%', 'sizes' => 'col-sm-3 col-md-1']) !!}

{!! Form::boText('pack_discount', 'Desconto Packs', null, [], ['help' => '%', 'sizes' => 'col-sm-3 col-md-1']) !!}

{!! Form::boCloseTopicGroup() !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
