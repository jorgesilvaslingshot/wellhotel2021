{!! Form::boOpen($data) !!}

{!! Form::boText('slug', '*Slug') !!}

{!! Form::boText('name', '*Nome', null, ['readonly_']) !!}

@foreach ($languages as $language)
  {!! Form::boTopicGroup(strtoupper($language)) !!}

  {!! Form::boTextarea('locales['.$language.'][title]', '*Título', $data->translateOrNew($language)->title, ['rows' => 2]) !!}

  {!! Form::boTexteditor('locales['.$language.'][content]', 'Corpo', $data->translateOrNew($language)->content) !!}

  {!! Form::boCloseTopicGroup() !!}
@endforeach

@include('admin::form.actions')

{!! Form::boClose() !!}
