{!! Form::boOpen($data) !!}

{!! Form::boText('name', '*Nome') !!}

{!! Form::boDateRange('date', 'Intervalo', !empty($data) ? $data->getStartDate():'', !empty($data) ? $data->getEndDate():'') !!}

{!! Form::boText('price', '*Preço', !empty($data) ? $data->getPrice():null, [], ['help' => '€', 'sizes' => 'col-sm-3 col-md-1']) !!}

{!! Form::boInput('priority', 'number', 'Prioridade', null, [], ['sizes' => 'col-sm-3 col-md-1']) !!}

{!! Form::hidden('room_id') !!}

{!! Form::hidden('id') !!}

<div class="form-group form-actions">
  <div class="col-sm-10 col-sm-offset-2">
    <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-check"></i> Guardar</button>
    <a href="{!! route('admin.rooms.edit', $data['room_id']) !!}" class="btn btn-default btn-flat"><i class="fa fa-times"></i> Cancelar</a>
  </div>
</div>

{!! Form::boClose() !!}
