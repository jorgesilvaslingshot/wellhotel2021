<tr>
  <td width="1%">{!! $row->id !!}</td>
  <td>{!! $row->name !!}</td>
  <td>{!! $row->locale !!}</td>
  @include('admin::datagrid.actions', ['id' => $row->id])
</tr>
