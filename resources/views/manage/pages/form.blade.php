{!! Form::boOpen($data) !!}

{!! Form::boTopicGroup('Páginas') !!}

{!! Form::boTextarea('title', '*Título', null, ['rows' => 2]) !!}

{!! Form::boText('name', 'Nome', null, [], ['help' => 'Nota: Assume valor do título caso vazio.']) !!}

{!! Form::boTexteditor('content', 'Corpo') !!}

{!! Form::boCloseTopicGroup() !!}

{!! Form::boTopicGroup('Endereço') !!}

<div class="form-group">
    <label for="url" class="col-sm-2 control-label">Url</label>
    <div class="col-sm-6 col-md-4">
        <div class="input-group">
            <span class="input-group-addon">{{ url('/') }}/</span>
            {{ Form::text('url', null, ['class' =>'form-control']) }}
        </div>
    </div>
    <div class="pull-left">
        <div class="form-control-static">Nota: é gerado automaticamente através do título caso vazio.</div>
    </div>
</div>

@if(!empty($data['id']))
    {!! Form::boTextStatic('Endereço atual', '<a href="'.url($data['url']).'" target="_blank">' . url($data['url']) . '</a>') !!}
@endif

{!! Form::boCloseTopicGroup() !!}

{!! Form::boTopicGroup('Banner') !!}

{!! Form::boFile('masthead_image', 'Imagem', 'mastheads') !!}

{!! Form::boBoolean('masthead_big', 'Grande?') !!}

{!! Form::boTexteditor('masthead_description', 'Descrição') !!}

{!! Form::boCloseTopicGroup() !!}

{!! Form::boTopicGroup('SEO') !!}

{!! Form::boText('seo_title', 'Título', null, ['maxlength' => 70], ['help' => 'máx. 70 caracteres']) !!}

{!! Form::boText('seo_description', 'Descrição', null, ['maxlength' => 156], ['help' => 'máx. 156 caracteres']) !!}

{!! Form::boFile('seo_image', 'Imagem', 'images') !!}

{!! Form::boCloseTopicGroup() !!}

{!! Form::boTopicGroup('Outras opções') !!}

{!! Form::boSelect('locale', '*Idioma', $languages, null,[],['size' => 'col-sm-4']) !!}

{!! Form::boBoolean('publish', '*Publicar') !!}

{!! Form::boCloseTopicGroup() !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
