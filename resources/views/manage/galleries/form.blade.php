{!! Form::boOpen($data) !!}


@foreach ($languages as $language)
    {!! Form::boTopicGroup(strtoupper($language)) !!}
    {!! Form::boText('languages['.$language.'][name]', 'Nome', $data->translateOrNew($language)->name) !!}
    {!! Form::boText('languages['.$language.'][description]', 'Descrição', $data->translateOrNew($language)->description) !!}
    {!! Form::boCloseTopicGroup() !!}
@endforeach

{!! Form::boTopicGroup('Imagens') !!}
@if(empty($data['id']))
    <div class="row">
        <div class="col-sm-offset-2 col-sm-10 col-md-8">
            <div class="alert alert-warning" role="alert">Tem que guardar primeiro a galeria para adicionar imagens.</div>
        </div>
    </div>
@else
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <div class="box box-solid">
                <div class="table-responsive">
                    <table class="table table-striped text-center">
                        <thead>
                        <tr>
                            <td><strong>Image</strong></td>
                            <td><strong>Rótulo</strong></td>
                            <td><strong>Priority</strong></td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($items as $item)
                            <tr>
                                <td>
                                    <div class="file-icon">
                                        <img src="{!! $item->image !!}" width="100"/>
                                    </div>
                                </td>
                                <td>{!! $item->label !!}</td>
                                <td>{!! $item->priority !!}</td>
                                <td class="text-nowrap text-middle" width="1%">
                                    <a href="{!! route('admin.gallery-items.edit', $item->id) !!}?gallery_id={!! $data['id'] !!}" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="#" data-provide="delete-ajax" data-token="{!! csrf_token() !!}" data-href="{!! route('admin.gallery-items.delete', $item->id) !!}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Delete">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-center text-info" colspan="100%">Não existem imagens associadas.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <a href="{!! route('admin.gallery-items.edit') !!}?gallery_id={!! $data['id'] !!}" class="btn btn-default btn-flat"><i class="fa fa-plus"></i> Add Image</a>
            <br/>
        </div>
    </div>
@endif
{!! Form::boCloseTopicGroup() !!}


@include('admin::form.actions')

{!! Form::boClose() !!}
