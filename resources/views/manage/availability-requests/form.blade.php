{!! Form::boOpen($data) !!}

{!! Form::boText('nome', 'Nome', null, ['readonly']) !!}
{!! Form::boText('email', 'E-mail', null, ['readonly']) !!}
{!! Form::boText('phone', 'Telefone', null, ['readonly']) !!}
{!! Form::boText('arrival', 'Data de chegada', null, ['readonly']) !!}
{!! Form::boText('departure', 'Data de saída', null, ['readonly']) !!}
{!! Form::boText('adults', 'Nº de adultos', null, ['readonly']) !!}
{!! Form::boText('children', 'Nº de crianças', null, ['readonly']) !!}
{!! Form::boText('product_name', 'Serviço', null, ['readonly']) !!}
{!! Form::boText('created_at', 'Submetido em', null, ['readonly']) !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
