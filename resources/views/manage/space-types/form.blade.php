{!! Form::boOpen($data) !!}

@foreach ($languages as $language)
  {!! Form::boTopicGroup(strtoupper($language)) !!}

  {!! Form::boText('locales['.$language.'][name]', '*Nome', $data->translateOrNew($language)->name) !!}

  {!! Form::boCloseTopicGroup() !!}
@endforeach

{!! Form::boText('priority','Prioridade', $data->priority, array() ,array('sizes' => 'col-sm-3 col-md-1')) !!}
{!! Form::boBoolean('publish', 'Publicado') !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
