{!! Form::boOpen($data) !!}

@foreach ($languages as $language)
    <?php $translation = $data->translateOrNew($language) ?>
    {!! Form::boTopicGroup(strtoupper($language)) !!}
    {!! Form::boText("languages[$language][name]", '*Nome', $translation->name) !!}
    {!! Form::boTexteditor("languages[$language][description]", 'Descrição', $translation->description) !!}
    {!! Form::boTextarea("languages[$language][excerpt]", 'Excerto', $translation->excerpt) !!}
    {!! Form::boText("languages[$language][url]", 'Outro Url', $translation->url) !!}
    {!! Form::boCloseTopicGroup() !!}
@endforeach

{!! Form::boFile('image', 'Imagem', 'spaces') !!}
{!! Form::boFile('thumbnail', 'Miniatura', 'spaces') !!}

{!! Form::boSelect('space_type_id', '*Tipo', $spaceTypes) !!}
{!! Form::boSelect('gallery_id', 'Galeria', $galleries) !!}

{!! Form::boText('priority','Prioridade', $data->priority, array() ,array('sizes' => 'col-sm-3 col-md-1')) !!}
{!! Form::boBoolean('publish', 'Publicado') !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
