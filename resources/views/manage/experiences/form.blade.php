{!! Form::boOpen($data) !!}


@foreach ($languages as $language)
    <?php $translation = $data->translateOrNew($language) ?>
    {!! Form::boTopicGroup(strtoupper($language)) !!}
    {!! Form::boText("languages[$language][name]", 'Nome', $translation->name) !!}
    {!! Form::boText("languages[$language][label]", 'Etiqueta', $translation->label) !!}
    {!! Form::boText("languages[$language][subtitle]", 'Subtítulo', $translation->subtitle) !!}
    {!! Form::boTexteditor("languages[$language][description]", 'Descrição', $translation->description) !!}
    {!! Form::boTextarea("languages[$language][excerpt]", 'Excerto', $translation->excerpt) !!}
    {{--{!! Form::boTextarea("languages[$language][includes]", 'Inclui', $translation->includes) !!}--}}
    {!! Form::boCloseTopicGroup() !!}
@endforeach

{!! Form::boFile('cover_image', 'Imagem (Topo)', 'cover-images-experiences') !!}
{!! Form::boFile('cover_image_2', 'Miniatura', 'cover-images-experiences') !!}

{!! Form::boSelect('gallery_id', 'Galeria', $galleries) !!}

{!! Form::boText('priority','Prioridade', $data->priority, array() ,array('sizes' => 'col-sm-3 col-md-1')) !!}
{!! Form::boBoolean('publish', 'Publicado') !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
