{!! Form::boOpen($data) !!}

{!! Form::boText('description', 'Descrição', $data->description) !!}

{!! Form::boDateRange('date', 'Intervalo', $data->date_start, $data->date_end) !!}


<div class="form-group form-actions">
    <div class="col-sm-10 col-sm-offset-2">
        <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-check"></i> Guardar</button>
        <a href="{!! route('admin.vacations') !!}" class="btn btn-default btn-flat"><i
                class="fa fa-times"></i> Cancelar</a>
    </div>
</div>

{!! Form::boClose() !!}
