<tr>
  <td width="1%">{!! $row->id !!}</td>
  <td>{!! $row->description !!}</td>
  <td>{!! $row->date_start !!}</td>
  <td>{!! $row->date_end !!}</td>
  @include('admin::datagrid.actions', ['id' => $row->id])
</tr>
