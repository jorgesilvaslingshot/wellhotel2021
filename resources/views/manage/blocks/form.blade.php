{!! Form::boOpen($data) !!}

@foreach ($languages as $language)
  {!! Form::boTopicGroup(strtoupper($language)) !!}

  {!! Form::boTextarea('locales['.$language.'][title]', '*Título', $data->translateOrNew($language)->title, ['rows' => 2]) !!}

  {!! Form::boText('locales['.$language.'][label]', 'Etiqueta', $data->translateOrNew($language)->label) !!}

  {!! Form::boTexteditor('locales['.$language.'][description]', 'Descrição', $data->translateOrNew($language)->description) !!}

  {!! Form::boCloseTopicGroup() !!}
@endforeach

{!! Form::boTopicGroup('Outras Opções') !!}

{!! Form::boFile('image', 'Imagem', 'blocks') !!}

{!! Form::boFile('thumbnail', 'Miniatura', 'blocks') !!}

{!! Form::boSelect('gallery_id', 'Galeria', $galleries) !!}

{!! Form::boCloseTopicGroup() !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
