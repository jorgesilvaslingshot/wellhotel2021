{!! Form::boOpen($data) !!}

<?php
$icons = [
    'icon-circle-sound' => 'Som',
    'icon-circle-tv' => 'TV',
    'icon-circle-safe-box' => 'Cofre',
    'icon-circle-bar' => 'Bar',
    'icon-circle-wifi' => 'Wi-fi',
    'icon-circle-children-space' => 'Espaço para crianças',
    'icon-circle-spa' => 'SPA',
    'icon-circle-living-room' => 'Sala de estar',
    'icon-circle-pool' => 'Piscina',
    'icon-circle-fridge' => 'Frigorífico',
    'icon-circle-kitchen' => 'Cozinha Partilhada',
    'icon-circle-gym' => 'Ginásio',
    'icon-icon-transfers'=>'Transfers',
    'icon-icon-recepcao-24h' => 'Recepção 24h',
    'icon-icon-recepcao-24h'=> 'Novo Recepção 24h',
    'icon-icon-elevador-panoramico'=> 'Elevador',
    'icon-icon-restaurante' => 'Restaurante'
]
?>

{!! Form::boSelect('icon', 'Ícone', $icons) !!}

@foreach ($languages as $language)
    {!! Form::boTopicGroup(strtoupper($language)) !!}
    {!! Form::boText('languages['.$language.'][name]', 'Nome', $data->translateOrNew($language)->name) !!}
    {!! Form::boCloseTopicGroup() !!}
@endforeach

{!! Form::boBoolean('is_main', 'É principal?') !!}
{!! Form::boText('priority','Prioridade', $data->priority, array() ,array('sizes' => 'col-sm-3 col-md-1')) !!}
{!! Form::boBoolean('publish', 'Publicado') !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
