<tr>
  <td width="1%">{!! $row->id !!}</td>
  <td>{!! $row->title !!}</td>
  <td>{!! $row->locale !!}</td>
  <td>{!! $row->priority !!}</td>
    <td><i class="fa fa-{!! $row->publish ? 'check' : 'times' !!}"></i></td>
  @include('admin::datagrid.actions', ['id' => $row->id])
</tr>
