{!! Form::boOpen($data) !!}

{!! Form::boTopicGroup('Informação básica') !!}

{!! Form::boTextarea('title', '*Título', null, ['rows' => 2]) !!}

{!! Form::boTextarea('description', 'Descrição', null, ['rows' => 2]) !!}

{!! Form::boFile('image', '*Imagem', 'slides') !!}

{!! Form::boText('action_title', 'Título (botão)') !!}

{!! Form::boText('action_link', 'Link (botão)') !!}

{!! Form::boCloseTopicGroup() !!}

{!! Form::boTopicGroup('Outras opções') !!}

{!! Form::boSelect('locale', '*Idioma', $languages, null,[],['size' => 'col-sm-4']) !!}

{!! Form::boText('priority','Prioridade', $data->priority, array() ,array('sizes' => 'col-sm-3 col-md-1')) !!}

{!! Form::boBoolean('publish', '*Publicar') !!}

{!! Form::boCloseTopicGroup() !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
