{!! Form::boOpen($data) !!}

{!! Form::boText('name', 'Nome', null, ['readonly']) !!}
{!! Form::boText('email', 'E-mail', null, ['readonly']) !!}
{!! Form::boText('phone', 'Telefone', null, ['readonly']) !!}

{!! Form::boText('adults', 'Nº de adultos', null, ['readonly']) !!}
{!! Form::boText('children', 'Nº de crianças', null, ['readonly']) !!}
{!! Form::boText('voucher_type', 'Tipo de Voucher', null, ['readonly']) !!}
{!! Form::boTextArea('description', 'Descrição', null, ['readonly']) !!}
{!! Form::boText('created_at', 'Submetido em', null, ['readonly']) !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
