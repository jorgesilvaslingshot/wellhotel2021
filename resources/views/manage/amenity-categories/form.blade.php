{!! Form::boOpen($data) !!}


@foreach ($languages as $language)
  {!! Form::boTopicGroup(strtoupper($language)) !!}
  {!! Form::boText('languages['.$language.'][name]', 'Nome', $data->translateOrNew($language)->name) !!}

  <div class="form-group"><label for="languages[{{ $language }}][amenities_list]" class="col-sm-2 control-label">Comodidades</label>
      <div class="col-sm-10 col-md-8">
          {!! Form::textarea("languages[$language][amenities_list]", $data->translateOrNew($language)->amenities_list, ['class' => 'form-control','rows' => 10]) !!}

          <p>Nota: inserir uma comodidade por linha.</p>
      </div>
  </div>

  {!! Form::boCloseTopicGroup() !!}
@endforeach

{!! Form::boText('priority','Prioridade', $data->priority, array() ,array('sizes' => 'col-sm-3 col-md-1')) !!}
{!! Form::boBoolean('publish', 'Publicado') !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
