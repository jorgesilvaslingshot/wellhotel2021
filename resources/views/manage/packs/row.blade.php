<tr>
  <td width="1%">{!! $row->id !!}</td>
  <td>{!! $row->name !!}</td>
  <td>{!! $row->priority !!}</td>
    <td><i class="fa fa-{!! $row->publish ? 'check' : 'times' !!}"></i></td>
  @include('admin::datagrid.actions', ['id' => $row->id])
</tr>
