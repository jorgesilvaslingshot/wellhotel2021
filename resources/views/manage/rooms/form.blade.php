{!! Form::boOpen($data) !!}


@foreach ($languages as $language)
    <?php $translation = $data->translateOrNew($language) ?>
    {!! Form::boTopicGroup(strtoupper($language)) !!}
    {!! Form::boText("languages[$language][name]", '*Nome', $translation->name) !!}
    {!! Form::boText("languages[$language][label]", 'Etiqueta', $translation->label) !!}
    {!! Form::boTextarea("languages[$language][title]", 'Título', $translation->title, ['rows' => 2]) !!}
    {!! Form::boTexteditor("languages[$language][description]", 'Descrição', $translation->description) !!}
    {!! Form::boTextarea("languages[$language][excerpt]", 'Excerto',$translation->excerpt, ['rows' => 2]) !!}
    {!! Form::boTextarea("languages[$language][additional_title]", 'Título adicional',$translation->additional_title, ['rows' => 2]) !!}
    {!! Form::boTexteditor("languages[$language][additional_description]", 'Descrição adicional',
        $translation->additional_description, ['rows' => 3]) !!}
    {!! Form::boTextarea("languages[$language][room_size]", 'Tamanho do quarto', $translation->room_size, ['rows' => 2]) !!}
    {!! Form::boTextarea("languages[$language][bed_size]", 'Tamanho da(s) cama(s)', $translation->bed_size, ['rows' => 2]) !!}
    {!! Form::boTextarea("languages[$language][max_guests]", 'Ocupação máxima (texto)', $translation->max_guests, ['rows' => 2]) !!}

    <div class="form-group"><label for="languages[{{ $language }}][other_amenities]" class="col-sm-2 control-label">Comodidades
            do
            quarto</label>
        <div class="col-sm-10 col-md-8">
            {!! Form::textarea("languages[$language][other_amenities]", $translation->other_amenities, ['class' => 'form-control','rows' => 10]) !!}

            <p>Nota: inserir uma comodidade por linha.</p>
        </div>
    </div>

    {!! Form::boCloseTopicGroup() !!}
@endforeach

{!! Form::boSelect('gallery_id', 'Galeria', $galleries) !!}
{!! Form::boSelect('room_type_id', 'Tipo de quarto', $roomTypes) !!}
{!! Form::boSelectMultiple('amenities', 'Comodidades gerais', $amenities) !!}
{!! Form::boFile('masthead_image', '*Imagem (topo)', 'mastheads') !!}
{!! Form::boFile('cover_image', 'Imagem de capa 1', 'cover-images') !!}
{!! Form::boFile('cover_image_2', 'Imagem de capa 2', 'cover-images') !!}

{!! Form::boTopicGroup('Reservas') !!}

{!! Form::boText('price', 'Preço', null, [], ['help' => '€', 'sizes' => 'col-sm-3 col-md-1']) !!}

{!! Form::boText('max_guests_numeric', 'Ocupação máxima', null, [], ['sizes' => 'col-sm-3 col-md-1']) !!}

{!! Form::boCloseTopicGroup() !!}

{!! Form::boTopicGroup('Outros Preços') !!}
@if(empty($data['id']))
    <div class="row">
        <div class="col-sm-offset-2 col-sm-10 col-md-8">
            <div class="alert alert-warning" role="alert">Tem que guardar primeiro o quarto para adicionar outros preços.</div>
        </div>
    </div>
@else
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <div class="box box-solid">
                <div class="table-responsive">
                    <table class="table table-striped text-center">
                        <thead>
                        <tr>
                            <td><strong>Nome</strong></td>
                            <td><strong>De</strong></td>
                            <td><strong>A</strong></td>
                            <td><strong>Preço (€)</strong></td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($data->getOrderedPriceIntervals() as $price)
                            <tr>
                                <td>{!! $price->getName() !!}</td>
                                <td>{!! $price->getStartDate() !!}</td>
                                <td>{!! $price->getEndDate() !!}</td>
                                <td>{!! $price->getPrice() !!}</td>
                                <td class="text-nowrap text-middle" width="1%">
                                    <a href="{!! route('admin.room-price-intervals.edit', $price->id) !!}?room_id={!! $data['id'] !!}" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="#" data-provide="delete-ajax" data-token="{!! csrf_token() !!}" data-href="{!! route('admin.room-price-intervals.delete', $price->getId()) !!}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Delete">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-center text-info" colspan="100%">Não existem outros preços.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <a href="{!! route('admin.room-price-intervals.edit') !!}?room_id={!! $data['id'] !!}" class="btn btn-default btn-flat"><i class="fa fa-plus"></i> Adicionar Preço</a>
            <br/>
        </div>
    </div>
@endif
{!! Form::boCloseTopicGroup() !!}


{!! Form::boText('priority','Prioridade', $data->priority, array() ,array('sizes' => 'col-sm-3 col-md-1')) !!}
{!! Form::boBoolean('publish', '*Publicar') !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
