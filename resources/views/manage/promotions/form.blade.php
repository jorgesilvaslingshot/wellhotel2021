{!! Form::boOpen($data) !!}

@foreach ($languages as $language)
  {!! Form::boTopicGroup(strtoupper($language)) !!}

  {!! Form::boText('locales['.$language.'][label]', 'Etiqueta', $data->translateOrNew($language)->label) !!}

  {!! Form::boTextarea('locales['.$language.'][title]', '*Título', $data->translateOrNew($language)->title, ['rows' => 2]) !!}

  {!! Form::boCloseTopicGroup() !!}
@endforeach

{!! Form::boTopicGroup('Outras opções') !!}

{!! Form::boDate('start_date', '*Data início') !!}

{!! Form::boDate('end_date', '*Data fim') !!}

{!! Form::boText('link', 'Link') !!}

{!! Form::boFile('image', '*Image', 'promotions') !!}

{!! Form::boBoolean('publish', 'Publicado') !!}

{!! Form::boCloseTopicGroup() !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
