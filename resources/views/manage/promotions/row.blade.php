<tr>
  <td width="1%">{!! $row->id !!}</td>
  <td>{!! $row->title !!}</td>
  <td>{!! $row->start_date !!}</td>
  <td>{!! $row->end_date !!}</td>
  <td><i class="fa fa-{!! $row->publish ? 'check' : 'times' !!}"></i></td>
  @include('admin::datagrid.actions', ['id' => $row->id])
</tr>
