{!! Form::boOpen($data) !!}

{!! Form::boTopicGroup('Informação básica') !!}

{!! Form::boText('author', '*Autor') !!}

{!! Form::boTextarea('message', '*Mensagem', null, ['rows' => 3]) !!}

{!! Form::boDate('date', '*Data') !!}

{!! Form::boCloseTopicGroup() !!}

{!! Form::boTopicGroup('Outras opções') !!}

{!! Form::boSelect('locale', '*Idioma', $languages, null,[],['size' => 'col-sm-4']) !!}

{!! Form::boText('priority','Prioridade', $data->priority, array() ,array('sizes' => 'col-sm-3 col-md-1')) !!}

{!! Form::boBoolean('publish', '*Publicar') !!}

{!! Form::boCloseTopicGroup() !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
