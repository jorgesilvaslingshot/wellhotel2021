<tr>
    <td width="1%">{!! $row->id !!}</td>
    <td>{!! $row->author !!}</td>
    <td>{!! str_limit($row->message) !!}</td>
    <td>{!! $row->date->format('Y-m-d') !!}</td>
    <td>{!! $row->locale !!}</td>
    <td>{!! $row->priority !!}</td>
    <td>{!! $row->publish !!}</td>
    @include('admin::datagrid.actions', ['id' => $row->id])
</tr>
