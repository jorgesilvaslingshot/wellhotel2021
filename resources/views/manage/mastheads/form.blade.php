{!! Form::boOpen($data) !!}

{!! Form::boText('slug', '*Slug') !!}

@foreach ($languages as $language)
    <?php $translation = $data->translateOrNew($language) ?>
    {!! Form::boTopicGroup(strtoupper($language)) !!}
    {!! Form::boTextarea("locales[$language][title]", 'Título', $translation->title, ['rows' => 2]) !!}
    {!! Form::boTexteditor("locales[$language][description]", 'Descrição', $translation->description) !!}
    {!! Form::boCloseTopicGroup() !!}
@endforeach

{!! Form::boTopicGroup('Outras informações') !!}

{!! Form::boFile('image', '*Imagem', 'mastheads') !!}

{!! Form::boCloseTopicGroup() !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
