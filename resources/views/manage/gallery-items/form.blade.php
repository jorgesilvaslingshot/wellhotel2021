{!! Form::boOpen($data) !!}

@foreach ($languages as $language)
    {!! Form::boTopicGroup(strtoupper($language)) !!}
    {!! Form::boText('languages['.$language.'][label]', 'Rótulo', $data->translateOrNew($language)->label) !!}
    {!! Form::boCloseTopicGroup() !!}
@endforeach

{!! Form::boInput('priority', 'number', 'Prioridade') !!}
{!! Form::boFile('image', 'Ficheiro', 'gallery-image') !!}
{!! Form::hidden('gallery_id') !!}

<div class="form-group form-actions">
  <div class="col-sm-10 col-sm-offset-2">
    <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-check"></i> Guardar</button>
    <a href="{!! route('admin.galleries.edit', $data['gallery_id']) !!}" class="btn btn-default btn-flat"><i class="fa fa-times"></i> Cancelar</a>
  </div>
</div>

{!! Form::boClose() !!}
