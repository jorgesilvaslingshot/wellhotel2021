{!! Form::boOpen($data) !!}

{!! Form::boText('locale', 'Idioma', null, ['readonly']) !!}
{!! Form::boText('email', 'E-mail', null, ['readonly']) !!}
{!! Form::boTextarea('message', 'Mensagem', null, ['class' => 'simple form-control', 'readonly']) !!}
{!! Form::boText('created_at', 'Submetido em', null, ['readonly']) !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
