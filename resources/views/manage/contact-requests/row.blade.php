<tr>
  <td width="1%">{!! $row->id !!}</td>
  <td>{!! $row->locale !!}</td>
  <td>{!! $row->email !!}</td>
  <td>{!! $row->created_at !!}</td>
  @include('admin::datagrid.actions', ['id' => $row->id])
</tr>
