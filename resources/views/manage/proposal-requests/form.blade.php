{!! Form::boOpen($data) !!}

{!! Form::boText('name', 'Nome', null, ['readonly']) !!}
{!! Form::boText('email', 'E-mail', null, ['readonly']) !!}
{!! Form::boText('phone', 'Telefone', null, ['readonly']) !!}
{!! Form::boText('arrival', 'Data', null, ['readonly']) !!}
{!! Form::boText('start_time', 'Data de Início', null, ['readonly']) !!}
{!! Form::boText('end_time', 'Data de Fim', null, ['readonly']) !!}
{!! Form::boText('adults', 'Nº de adultos', null, ['readonly']) !!}
{!! Form::boText('children', 'Nº de crianças', null, ['readonly']) !!}
{!! Form::boText('event_type', 'Tipo de Evento', null, ['readonly']) !!}
{!! Form::boTextArea('description', 'Descrição', null, ['readonly']) !!}
{!! Form::boText('created_at', 'Submetido em', null, ['readonly']) !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
