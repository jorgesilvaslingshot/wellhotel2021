{!! Form::boOpen($data) !!}

{!! Form::boText('name', 'Nome', null, ['readonly']) !!}

@foreach ($languages as $language)
  {!! Form::boTopicGroup(strtoupper($language)) !!}
  @if ($data['allow_html'])
    {!! Form::boTextarea('locales['.$language.'][content]', 'Conteúdo', $data->translateOrNew($language)->content) !!}
  @else
  {!! Form::boTextarea('locales['.$language.'][content]', 'Conteúdo', $data->translateOrNew($language)->content,
   ['class' => 'simple form-control']) !!}
  @endif
  {!! Form::boCloseTopicGroup() !!}
@endforeach

@include('admin::form.actions')

{!! Form::boClose() !!}
