{!! Form::boOpen($data) !!}

@foreach ($languages as $language)
    <?php $translation = $data->translateOrNew($language) ?>
    {!! Form::boTopicGroup(strtoupper($language)) !!}
    {!! Form::boText("languages[$language][name]", '*Nome', $translation->name) !!}
    {!! Form::boTexteditor("languages[$language][description]", 'Descrição', $translation->description) !!}
    {!! Form::boTextarea("languages[$language][description_right]", 'Descrição à Direita', $translation->description_right) !!}

    {!! Form::boText("languages[$language][title_right]", 'Título à Direita', $translation->title_right) !!}
    {!! Form::boCloseTopicGroup() !!}
@endforeach

{!! Form::boFile('image', 'Imagem', 'spaces') !!}

{!! Form::boSelect('gallery_id', 'Galeria', $galleries) !!}

{!! Form::boText('priority','Prioridade', $data->priority, array() ,array('sizes' => 'col-sm-3 col-md-1')) !!}
{!! Form::boBoolean('publish', 'Publicado') !!}

@include('admin::form.actions')

{!! Form::boClose() !!}
