@extends('layouts.template')

@section('site.content')
    <div class="masthead masthead--full o-vertical-middle c-white">
        <div class="masthead__background fill bg-cover bg-center"
             style="background-image: url('{{ $space->getImage() }}');"></div>

        <div class="masthead__content o-vertical-middle__item">

            <h1 class="masthead__title ls50">
                <span class="line-title hidden-xs"></span>

                {{ $space->getName() }}
            </h1>

            <p class="masthead__subtitle font-libre-baskerville italic ls150 mb-0">
                {{ $space->getType()->getName() }}
            </p>
            <p class="gallery-above-image">
                @if($space->hasGallery())
                <my-gallery-link class="introduction__image-group"
                                 :images="{{ $space->getGallery()->getItemsToModal() }}"
                                 name="{{ $space->getGallery()->getName() }}" description="{{ $space->getGallery()->getDescription() }}">
                    <span class="introduction__image-group__link ls300 text-lowercase">
                        @icon('icon-pictures', ['class' => 'icon icon-pictures'])

                        <span class="hidden-xs">@lang('site.see-gallery')</span>
                    </span>
                </my-gallery-link>
                @endif
            </p>
        </div>

    </div>

    <div class="container main-wrapper">
        <div class="introduction detalhes-space">

            <h3 class="masthead__title ls50 text-center">

                {!! $space->getName() !!}
            </h3>

            <div class="introduction__description ls25 text-center">
                {!! $space->getDescription() !!}
            </div>

            <div class="actions-group text-center text-left-sm-up descricao-botoes">

                <a href="{{ route('booking', ['id' => $space->getId(), 'type' => 'room']) }}"
                    class="btn btn--main-inverted btn--animated ls300 fw-600 book-button">
                     <span class="p-relative">@lang('site.book-now')</span>
                 </a>

                <a class="addthis_button_more share-button" href="#">
                    <button href="#" class="btn btn--main btn--animated ls300 fw-600 m-0">
                        <span class="p-relative">@lang('site.share')</span>
                    </button>
                </a>
            </div>

        </div>
    </div>
@endsection
