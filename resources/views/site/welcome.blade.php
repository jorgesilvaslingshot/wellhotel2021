@extends('layouts.template')

@section('site.content')
    <my-top-slider inline-template>
        <div class="top-slider">
            <div class="top-slider__backgrounds fill slick" ref="backgroundSlick">
                @foreach($slides as $slide)
                    <div class="slick-slide">
                        <div class="top-slider__image fill bg-cover bg-center bg-no-repeat b-lazy"
                             data-src="{{ $slide->getImage() }}"></div>
                    </div>
                @endforeach
            </div>

            <div class="slick slick--default" ref="slick">
                @foreach($slides as $slide)
                    <div class="slick-slide">
                        <div class="top-slider__slide o-vertical-middle c-white">
                            <div class="top-slider__slide__content o-vertical-middle__item">
                                <div class="fraction-pagination font-libre-baskerville italic ls60">
                                    <span>{{ $loop->iteration }}</span>
                                    <span>{{ $loop->count }}</span>
                                </div>

                                <h1 class="top-slider__slide__title font-libre-baskerville italic ls200 fw-400 mb-3">
                                    <span class="line-title"></span>

                                    <span>{{ $slide->getTitle() }}</span>
                                </h1>

                                @if(!empty($slide->getDescription()))
                                <div class="top-slider__slide__description font-libre-baskerville italic ls150 mb-3 mb-md-5">
                                    {!! $slide->getDescription() !!}
                                </div>
                                @endif

                                @if($slide->hasAction())
                                    <a href="{{ $slide->getActionLink() }}"
                                       class="btn btn--white btn--animated ls300 fw-600"
                                    >
                                        <span class="p-relative">{{ $slide->getActionTitle() }}</span>
                                    </a>
                                @endif
                            </div>

                            <div class="top-slider__side top-slider__side--left hidden-xs hidden-sm"></div>
                            <div class="top-slider__side top-slider__side--right hidden-xs hidden-sm"></div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="wifi wifi--top-slider font-libre-baskerville c-white italic ls300 text-right-sm-up">
                @lang('site.free-wi-fi')

                @icon('icon-wifi')
            </div>
        </div>
    </my-top-slider>

    @include('partials.promotion')

    <div class="container main-wrapper">
        <div class="introduction">
            <p class="introduction__label h6 ls100 p-relative text-primary">
                <span class="line-title"></span>

                {{ $aboutUsBlock->getLabel() }}
            </p>

            <h1 class="introduction__title ls50 c-gray-dark">{{ $aboutUsBlock->getTitle() }}</h1>

            @if($aboutUsBlock->hasGallery())
                <my-gallery-link class="introduction__image-group"
                                 :images="{{ $aboutUsBlock->getGallery()->getItemsToModal() }}"
                                 name="{{ $aboutUsBlock->getGallery()->getName() }}">
                    <img class="img-responsive b-lazy" src="{{ img_placeholder() }}"
                         data-src="{{ img_cache($aboutUsBlock->getImage(), 'introduction-cover-item') }}"
                         width="460"
                         height="300">
                    <img class="b-lazy"
                         src="{{ img_placeholder() }}"
                         data-src="{{ img_cache($aboutUsBlock->getThumbnail(), 'introduction-cover-item-2') }}"
                         width="265"
                         height="170">


                    <span class="introduction__image-group__link ls300 text-lowercase">
                        @icon('icon-pictures', ['class' => 'icon icon-pictures'])

                        <span class="hidden-xs">@lang('site.see-gallery')</span>
                    </span>
                </my-gallery-link>
            @else
                <div class="introduction__image-group">
                    <img class="img-responsive b-lazy" src="{{ img_placeholder() }}"
                         data-src="{{ img_cache($aboutUsBlock->getImage(), 'introduction-cover-item') }}"
                         width="460"
                         height="300">
                    <img class="b-lazy"
                         src="{{ img_placeholder() }}"
                         data-src="{{ img_cache($aboutUsBlock->getThumbnail(), 'introduction-cover-item-2') }}"
                         width="265"
                         height="170">
                </div>
            @endif

            <div class="introduction__description ls25">{!! $aboutUsBlock->getDescription() !!}</div>

            <div class="actions-group text-center text-left-sm-up">
                <a href="{{ route('about') }}" class="btn btn--main btn--animated ls300 fw-600">
                    <span class="p-relative">@lang('site.read-more')</span>
                </a>
            </div>
        </div>
    </div>

    {{-- SUSTENTABIULIDADE --}}
    @include('site.sustentabilidade')

    {{-- VOUCHER AQUI --}}

    @foreach($vouchers as $voucher)
        <div class="pack-list-item{{ $loop->index % 2 != 0 ? ' pack-list-item--inverted' : '' }}">
            <a class="pack anchor-text parent-btn" href="{{ route('booking-voucher', ['id' => $voucher->getId()]) }}">
                <div class="pack__body">
                    <p class="pack__label h6 ls100 p-relative text-primary">
                        <span class="line-title"></span>

                        {{ $voucher->getLabel() }}
                    </p>

                    <h4 class="pack__title ls50 c-gray-dark">
                        {{ $voucher->getName() }}
                    </h4>

                    <div class="pack__actions hidden-xs">
                        <span class="btn btn--main btn--animated ls300 fw-600">
                            <span class="p-relative">@lang('site.request-info')</span>
                        </span>
                    </div>
                </div>

                <div class="pack__image">

                    <img class="img-responsive b-lazy" src="{{ img_placeholder() }}"
                         data-src="{{ img_cache($voucher->getThumbnail(), 'pack-item') }}">
                </div>
            </a>
        </div>
    @endforeach

    {{-- FIM PACKAGE --}}

    {{-- SLIDER COMENTARIOS --}}

    @include('partials.comment-slider')


    @include('partials.bottom-slider')
@stop
