@extends('layouts.template')

@section('site.content')
    <div class="booking" id="booking-section">
        <my-booking-form inline-template action="{{ route('booking-request') }}" method="post"
                         products-url="{{ route('booking-products') }}"
                         price-url="{{ route('booking-price') }}"
                         :initial-id="{{ !empty($id) ? "$id": 'null' }}"
                         :initial-type="{{ !empty($type) ? "'$type'": 'null' }}"
                         :vacation-data="{{$data}}"
        >
            <transition name="fade" mode="out-in">
                <form class="booking__form"
                      v-on:submit.prevent="onSubmit"
                      v-on:keydown="form.errors.clear($event.target.name);"
                      v-if="!message"
                      key="form"
                >
                    <div class="booking__form__header">
                        <div class="container main-wrapper">
                            <p class="booking__top__label h6 ls100 p-relative c-white">
                                <span class="line-title"></span>

                                {{ $phrases['booking-online'] }}
                            </p>

                            <div class="booking__row text-center ls25">
                                {!! nl2br($phrases['booking-text-header']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="booking__form__products">
                        <div class="container main-wrapper">
                            <div class="booking__row">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="booking__form__types">
                                                <label class="radio-inline ">
                                                    <input type="radio"
                                                           name="product_type"
                                                           id="booking-form-product-type-room"
                                                           value="room"
                                                           v-model="form.product_type">
                                                    <span
                                                        class="font-libre-baskerville italic ls200">@lang('site.room')</span>
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio"
                                                           name="product_type"
                                                           id="booking-form-product-type-pack"
                                                           value="pack"
                                                           v-model="form.product_type">
                                                    <span
                                                        class="font-libre-baskerville italic ls200">@lang('site.pack')</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-8">
                                        <div class="booking__form__product">
                                            <div class="form-group" v-cloak="none">
                                                <label for="booking-form-product-name" class="sr-only"
                                                       v-text="form.product_type"></label>

                                                <div class="booking-select booking-select--top ls50 fw-300">
                                                    <select tabindex="-1"
                                                            name="product_name"
                                                            id="booking-form-product-name"
                                                            v-model="form.product_id"
                                                    >
                                                        <option value="" disabled>@lang('site.select')</option>

                                                        <option v-for="product in currentProducts" :value="product.id"
                                                                v-text="product.name"></option>
                                                    </select>

                                                    <span tabindex="0">
                                                        @{{ form.product_name || '@lang('site.select')' | limitOverflow }}
                                                    </span>
                                                </div>

                                                <span class="form-error"
                                                      v-if="form.errors.has('product_name')"
                                                      v-text="form.getError('product_name')"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container main-wrapper">
                        <div class="booking__row">
                            <div class="row">
                                <div class="booking__form__dates col-xs-12">
                                    <div class="form-group">
                                        <label for="booking-form-arrival"
                                               class="booking-label font-libre-baskerville italic ls200">
                                            @lang('site.arrival')
                                        </label>

                                        <div class="booking-select booking-select--date ls50 fw-300" v-cloak="none">
                                            <flat-pickr class="form-control"
                                                        name="arrival"
                                                        id="booking-form-arrival"
                                                        :config="flatpickrConfigArrival"
                                                        v-model="form.arrival"></flat-pickr>

                                            <span tabindex="0">@{{ form.arrival | formatDate }}</span>
                                        </div>

                                        <span class="form-error"
                                              v-if="form.errors.has('arrival')"
                                              v-text="form.getError('arrival')"></span>
                                    </div>

                                    <div class="booking__form__dates__arrow hidden-xs hidden-sm">
                                        <span class="line-title"></span>

                                        @icon('icon-arrow-right', ['class' => 'icon icon-arrow-right'])
                                    </div>

                                    <div class="form-group">
                                        <label for="booking-form-departure"
                                               class="booking-label font-libre-baskerville italic ls200">
                                            @lang('site.departure')
                                        </label>

                                        <div class="booking-select booking-select--date ls50 fw-300" v-cloak="none">
                                            <flat-pickr class="form-control"
                                                        name="departure"
                                                        id="booking-form-departure"
                                                        :config="flatpickrConfigDeparture"
                                                        v-model="form.departure"></flat-pickr>

                                            <span tabindex="0">@{{ form.departure | formatDate }}</span>
                                        </div>

                                        <span class="form-error"
                                              v-if="form.errors.has('departure')"
                                              v-text="form.getError('departure')"></span>
                                    </div>
                                </div>

                                <div class="booking__form__people col-xs-12">
                                    <div class="form-group">
                                        <label for="booking-form-adults"
                                               class="booking-label font-libre-baskerville italic ls200">
                                            @lang('site.adults')
                                        </label>


                                        <div class="booking-select ls50 fw-300" v-cloak="none">
                                            <select tabindex="-1"
                                                    name="adults"
                                                    id="booking-form-adults"
                                                    v-model="form.adults"
                                            >
                                                <option v-for="(value,index) in adultNumbers" :value="index"
                                                        v-text="value"></option>
                                            </select>

                                            <span tabindex="0">@{{ form.adults | pad }}</span>
                                        </div>
                                        <span class="form-error"
                                              v-if="form.errors.has('adults')"
                                              v-text="form.getError('adults')"></span>
                                    </div>

                                    <div class="form-group">
                                        <label for="booking-form-children"
                                               class="booking-label font-libre-baskerville italic ls200">
                                            @lang('site.children')
                                        </label>

                                        <div class="booking-select ls50 fw-300" v-cloak="none">
                                            <select tabindex="-1"
                                                    name="children"
                                                    id="booking-form-children"
                                                    v-model="form.children"
                                            >
                                                <option v-for="(value,index) in childrenNumbers" :value="index"
                                                        v-text="value"></option>
                                            </select>

                                            <span tabindex="0">@{{ form.children | pad }}</span>
                                        </div>
                                        <span class="form-error"
                                              v-if="form.errors.has('children')"
                                              v-text="form.getError('children')"></span>
                                    </div>
                                </div>

                                <div class="booking__form__price col-xs-12">
                                    <div class="form-group">
                                        <label class="booking-label font-libre-baskerville italic ls200">
                                            @lang('site.price-with-tax')
                                        </label>

                                        <div class="form-control-static" v-html="displayPrice"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="booking__client">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="booking-label font-libre-baskerville italic ls200"
                                                   for="booking-form-name"
                                                   :class="{'sr-only':placeholderSupported}">
                                                @lang('site.name')
                                            </label>
                                            <input type="text"
                                                   class="form-control ls25"
                                                   id="booking-form-name"
                                                   placeholder="@lang('site.name')"
                                                   v-model="form.name">
                                            <span class="form-error"
                                                  v-if="form.errors.has('name')"
                                                  v-text="form.getError('name')"></span>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="booking-label font-libre-baskerville italic ls200"
                                                   for="booking-form-email"
                                                   :class="{'sr-only':placeholderSupported}">
                                                @lang('site.email')
                                            </label>
                                            <input type="email"
                                                   class="form-control ls25"
                                                   id="booking-form-email"
                                                   placeholder="@lang('site.email')"
                                                   v-model="form.email">
                                            <span class="form-error"
                                                  v-if="form.errors.has('email')"
                                                  v-text="form.getError('email')"></span>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="booking-label font-libre-baskerville italic ls200"
                                                   for="booking-form-phone"
                                                   :class="{'sr-only':placeholderSupported}">
                                                @lang('site.phone-mobile-phone')
                                            </label>
                                            <input type="tel"
                                                   class="form-control ls25"
                                                   id="booking-form-phone"
                                                   placeholder="@lang('site.phone')"
                                                   v-model="form.phone">
                                            <span class="form-error"
                                                  v-if="form.errors.has('phone')"
                                                  v-text="form.getError('phone')"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="booking__actions">
                                <button type="submit" class="btn btn-block btn-primary text-uppercase fw-700 ls550">
                                    <span v-if="!form.processing">@lang('site.check-availability')</span>
                                    <span v-else>@lang('site.wait')</span>
                                </button>
                            </div>

                            <div class="booking__footer text-center text-left-sm-up ls-25">
                                <div class="row">
                                    <div class="u-row-table">
                                        <div class="co-sm-6 col-md-7 u-row-table__cell-sm">
                                            <p>
                                                {{ $phrases['booking-text'] }}
                                            </p>
                                        </div>
                                        <div
                                            class="col-sm-6 col-md-5 u-row-table__cell-sm text-right-sm-up vertical-bottom">
                                            <p>
                                                <a href="tel:{{ str_replace(' ', '', setting('contact_phone')) }}"><span>{{ setting('contact_phone') }}</span></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="booking__message o-vertical-middle" v-else key="message">
                    <div class="container-fluid o-vertical-middle__item">
                        <h3 class="booking__message__title ls50">{{ $phrases['availability-request-successful-title'] }}</h3>

                        <div class="booking__message__content ls25">
                            {!! $phrases['availability-request-successful-content'] !!}
                        </div>
                    </div>
                </div>
            </transition>
        </my-booking-form>

        <a class="booking__close" href="{{ $close }}">
            @icon('icon-close', ['class' => 'icon icon-close'])
        </a>
    </div>
@endsection
