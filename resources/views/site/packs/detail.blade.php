@extends('layouts.template')

@section('site.content')
    <div class="masthead masthead--full o-vertical-middle c-white">
        <div class="masthead__background fill bg-cover bg-center b-lazy"
             data-src="{{ $pack->getImage() }}"></div>

        <div class="masthead__content o-vertical-middle__item">
            <div class="fraction-pagination font-libre-baskerville italic ls60">
                <span>{{ $packNumber }}</span>
                <span>{{ $totalPacks }}</span>
            </div>

            <h1 class="masthead__title ls50">
                <span class="line-title hidden-xs"></span>

                {{ $pack->getLabel() }}
            </h1>

            <p class="masthead__subtitle font-libre-baskerville italic ls150 mb-0">

                {{ $pack->getName() }}
            </p>

            <p class="gallery-above-image">
                @if($pack->hasGallery())
                <my-gallery-link class="introduction__image-group"
                                 :images="{{ $pack->getGallery()->getItemsToModal() }}"
                                 name="{{ $pack->getGallery()->getName() }}" description="{{ $pack->getGallery()->getDescription() }}">
                    <span class="introduction__image-group__link ls300 text-lowercase">
                        @icon('icon-pictures', ['class' => 'icon icon-pictures'])

                        <span class="hidden-xs">@lang('site.see-gallery')</span>
                    </span>
                </my-gallery-link>
                @endif
            </p>
        </div>

    </div>

    <my-submenu class="submenu detalhes-space-pagination">
        <div class="submenu__content">

            <div class="submenu__col hidden-xs">
                <ul class="submenu__arrows">
                    <li>
                        <a href="{{ route('packs.detail', $prevPack->getSlug()) }}" class="arrow">
                            @icon('icon-slider-left', ['class' => 'icon icon-slider-left'])
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('packs.detail', $nextPack->getSlug()) }}" class="arrow">
                            @icon('icon-slider-right', ['class' => 'icon icon-slider-right'])
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </my-submenu>

    <div class="container main-wrapper {{$pack->getId() == 5 ? 'adega-mae-container': ''}}">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 {{$pack->getId() == 5 ? 'adega-mae-box-esquerda': ''}}">
                <div class="conclusion text-center pt-4">

                    @if(!empty($pack->getSubtitle()))
                    <h3 class="masthead__title ls50 text-center">

                        {!! $pack->getSubtitle() !!}
                    </h3>
                    @endif


                    <div class="page__body__description static-text ls25">
                        {!! $pack->getDescription() !!}
                    </div>

                    <div class="actions-group">
                        <a href="{{ route('booking', ['id' => $pack->getId(), 'type' => 'pack']) }}"
                           class="btn btn--main-inverted btn--animated ls300 fw-600">
                            <span class="p-relative">@lang('site.book-now')</span>
                        </a>

                        <a class="addthis_button_more" href="#">
                            <button href="#" class="btn btn--main btn--animated ls300 fw-600 ">
                                <span class="p-relative">@lang('site.share')</span>
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            {{-- somente para pag degustação --}}
            <div class="{{$pack->getId() == 5 ? 'adega-mae-box-direita ': ''}}">
                <div class="adega-mae"></div>
            </div>

        </div>
    </div>
@endsection
