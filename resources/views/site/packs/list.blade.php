@extends('layouts.template')

@section('site.content')
    <div class="masthead o-vertical-middle c-white">
        <div class="masthead__background fill bg-cover bg-center b-lazy"
             data-src="{{ $masthead->getImage() }}"></div>

        <div class="masthead__content o-vertical-middle__item">
            <h1 class="masthead__title ls50">
                <span class="line-title"></span>

                {{ $masthead->getTitle() }}
            </h1>

            <div class="masthead__subtitle font-libre-baskerville italic ls150 mb-0">
                {!! $masthead->getDescription() !!}
            </div>
        </div>

    </div>

    @php
        $index = 0;
    @endphp

    <div class="pack-experience-title">
        <p class="block-type-2__label h6 ls100 p-relative text-primary titulo-linha">
            @lang('site.think-about')
        </p>
        <h4 class="block-type-2__title ls50 c-gray-dark">
            @lang('site.packs')
        </h4>
    </div>

    @foreach($packs as $pack)
        <div id="s-{{ $pack->getSlug() }}">
            <div class="pack-list-item{{ $index % 2 != 0 ? ' pack-list-item--inverted' : '' }}">
                <div class="pack anchor-text parent-btn">
                    <div class="pack__body">
                        <p class="pack__label h6 ls100 p-relative text-primary">
                            <span class="line-title"></span>

                            {{ $pack->getLabel() }}
                        </p>

                        <h4 class="pack__title ls50 c-gray-dark">
                            {{ $pack->getName() }}
                        </h4>


                        <div class="pack-experience-buttons actions-group text-center text-left-sm-up">
                            <a href="{{ route('booking', ['id' => $pack->getId(), 'type' => 'pack']) }}"
                               class="btn btn--main-inverted btn--animated ls300 fw-600">
                                <span class="p-relative">@lang('site.book-now')</span>
                            </a>

                            <a href="{{ route('packs.detail', $pack->getSlug()) }}"
                                class="btn btn--main btn--animated ls300 fw-600">
                                 <span class="p-relative">@lang('site.learn-more')</span>
                             </a>
                        </div>
                    </div>

                    <div class="pack__image">

                        <img class="img-responsive b-lazy" src="{{ img_placeholder() }}"
                             data-src="{{ img_cache($pack->getThumbnail(), 'pack-item') }}">
                    </div>
                </div>
            </div>

            @php
                $index++;
            @endphp
        </div>
    @endforeach

    <div class="pack-experience-title">
        <p class="block-type-2__label h6 ls100 p-relative text-primary titulo-linha">
            @lang('site.think-about')
        </p>
        <h4 class="block-type-2__title ls50 c-gray-dark">
            @lang('site.experiences')
        </h4>
    </div>

    @foreach($experiences as $experience)
        <div id="s-{{ $experience->getSlug() }}">
            <div class="pack-list-item{{ $index % 2 != 0 ? ' pack-list-item--inverted' : '' }}">
                <div class="pack anchor-text parent-btn">
                    <div class="pack__body">
                        <p class="pack__label h6 ls100 p-relative text-primary">
                            <span class="line-title"></span>

                            {{ $experience->getLabel() }}
                        </p>

                        <h4 class="pack__title ls50 c-gray-dark">
                            {{ $experience->getName() }}
                        </h4>


                        <div class="pack-experience-buttons actions-group text-center text-left-sm-up">
                            <a href="{{ route('booking', ['id' => $experience->getId(), 'type' => 'experience']) }}"
                               class="btn btn--main-inverted btn--animated ls300 fw-600">
                                <span class="p-relative">@lang('site.book-now')</span>
                            </a>

                            <a href="{{ route('experiences.detail', $experience->getSlug()) }}"
                                class="btn btn--main btn--animated ls300 fw-600">
                                 <span class="p-relative">@lang('site.learn-more')</span>
                             </a>
                        </div>
                    </div>

                    <div class="pack__image">

                        <img class="img-responsive b-lazy" src="{{ img_placeholder() }}"
                             data-src="{{ img_cache($experience->getThumbnail(), 'pack-item') }}">
                    </div>
                </div>
            </div>

            @php
                $index++;
            @endphp
        </div>
    @endforeach
@endsection
