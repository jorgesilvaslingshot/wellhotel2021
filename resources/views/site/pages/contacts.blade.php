@extends('layouts.template')

@section('site.content')
    <div class="contacts-map">
        <div class="contacts-map__object">
            <my-contacts-map></my-contacts-map>
        </div>

        <div class="contacts-map__body c-white">
            <p class="contacts-map__coordinates font-libre-baskerville italic ls150 p-relative">
                <span class="line-title text-primary"></span>

                N 39.177326 · W -9.355299
            </p>

            <a href="{{ setting('directions_link') }}" target="_blank"
               class="btn btn--main btn--animated ls300 fw-600 c-white">
                <span class="p-relative">@lang('site.get-directions')</span>
            </a>
        </div>
    </div>

    <div class="page">
        <div class="row u-row-no-padding">
            <div class="col-sm-6">
                <div class="page__body">
                    <div class="page__body__description static-text ls25">
                        {!! $page->getContent() !!}
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="page__sidebar">
                    <p class="page__sidebar__label h6 ls100 p-relative text-primary">
                        <span class="line-title"></span>

                        {{ $phrases['get-in-touch-with-us'] }}
                    </p>

                    <my-contact-form inline-template action="{{ route('contact-request') }}" method="post">
                        <form class="contact-form" v-on:submit.prevent="onSubmit"
                              v-on:keydown="form.errors.clear($event.target.name);"
                        >
                            <transition name="fade">
                                <div class="alert alert-dismissible" :class="messageClass" role="alert" v-if="message"
                                     v-cloak="none">
                                    <button type="button" class="alert-close" aria-label="Close"
                                            v-on:click="clearMessage">
                                        @icon('icon-close', ['class' => 'icon icon-close'])
                                    </button>

                                    @{{ message }}
                                </div>
                            </transition>

                            <div class="form-group">
                                <label class="font-libre-baskerville italic ls200"
                                       for="contact-form-name"
                                       :class="{'sr-only':placeholderSupported}">
                                    @lang('site.name')
                                </label>
                                <input type="text"
                                       class="form-control"
                                       id="contact-form-name"
                                       placeholder="@lang('site.name')"
                                       v-model="form.name"
                                       required>
                                <span class="form-error"
                                      v-if="form.errors.has('name')"
                                      v-text="form.getError('name')"></span>
                            </div>

                            <div class="form-group">
                                <label class="font-libre-baskerville italic ls200"
                                       for="contact-form-email"
                                       :class="{'sr-only':placeholderSupported}">
                                    @lang('site.email')
                                </label>
                                <input type="email"
                                       class="form-control"
                                       id="contact-form-email"
                                       placeholder="@lang('site.email')"
                                       v-model="form.email"
                                       required>
                                <span class="form-error"
                                      v-if="form.errors.has('email')"
                                      v-text="form.getError('email')"></span>
                            </div>

                            <div class="form-group">
                                <label class="font-libre-baskerville italic ls200"
                                       for="contact-form-message"
                                       :class="{'sr-only':placeholderSupported}">
                                    @lang('site.message')
                                </label>
                                <textarea name="message"
                                          class="form-control"
                                          placeholder="@lang('site.message')"
                                          id="contact-form-message"
                                          rows="3"
                                          v-model="form.message"
                                          required
                                ></textarea>
                                <span class="form-error"
                                      v-if="form.errors.has('message')"
                                      v-text="form.getError('message')"></span>
                            </div>

                            <div>
                                <button class="btn btn--gray btn-block ls300" type="submit">@lang('site.send')</button>
                            </div>
                        </form>
                    </my-contact-form>
                </div>
            </div>
        </div>
    </div>
@endsection
