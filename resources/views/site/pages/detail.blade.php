@extends('layouts.template')

@section('site.content')
    <div class="masthead{{ $page->getMastheadBig() ? ' masthead--full': '' }} o-vertical-middle c-white">
        <div class="masthead__background fill bg-cover bg-center b-lazy"
             data-src="{{ $page->getMastheadImage() }}"></div>

        <div class="masthead__content o-vertical-middle__item">
            <h1 class="masthead__title ls50">
                <span class="line-title"></span>

                {{ $page->getTitle() }}
            </h1>

            <div class="masthead__subtitle font-libre-baskerville italic ls150 mb-0">
                {!! $page->getMastheadDescription() !!}
            </div>
        </div>
    </div>

    <div class="page">
        <div class="row u-row-no-padding">
            <div class="col-sm-6">
                <div class="page__body">
                    <div class="page__body__description static-text ls25">
                        {!! $page->getContent() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
