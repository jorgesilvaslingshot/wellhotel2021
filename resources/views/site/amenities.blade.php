@extends('layouts.template')

@section('site.content')
    <div class="masthead o-vertical-middle c-white">
        <div class="masthead__background fill bg-cover bg-center b-lazy"
             data-src="{{ $masthead->getImage() }}"></div>

        <div class="masthead__content o-vertical-middle__item">
            <h1 class="masthead__title ls50">
                <span class="line-title"></span>

                {{ $masthead->getTitle() }}
            </h1>

            <div class="masthead__subtitle font-libre-baskerville italic ls150 mb-0">
                {!! $masthead->getDescription() !!}
            </div>
        </div>

        <div class="wifi wifi--masthead font-libre-baskerville c-white italic ls300">
            @lang('site.free-wi-fi')

            @icon('icon-wifi')
        </div>
    </div>

    <my-submenu class="submenu" :scrollspy="true">
        <div class="submenu__content">
            <div class="submenu__col w-100">
                <div class="submenu__drag">
                    <my-drag-content>
                        <ul class="submenu__nav text-center text-nowrap ls25">
                            @foreach($amenityCategories as $amenityCategory)
                                <li data-target="#s-{{ $amenityCategory->getSlug() }}">
                                    <my-scroll-to class="submenu__nav__link"
                                                  target="#s-{{ $amenityCategory->getSlug() }}">
                                        {{ $amenityCategory->getName() }}
                                    </my-scroll-to>
                                </li>
                            @endforeach
                        </ul>
                    </my-drag-content>
                </div>
            </div>
        </div>
    </my-submenu>

    <div class="container main-wrapper float-block float-block--amenities">
        <div class="row">
            <div class="col-lg-4">
                <p class="float-block__label h6 ls100 p-relative text-primary">
                    <span class="line-title"></span>

                    {{ $phrases['main-hotel-amenities'] }}
                </p>
            </div>

            <div class="col-lg-8">
                <ul class="amenities-list amenities-list--main mb-md-0">
                    @foreach($mainAmenities as $amenity)
                        <li class="amenities-list__item text-center ls25">
                            @icon($amenity->getIcon(), ['class'=> 'text-primary icon '.$amenity->getIcon()])

                            {{ $amenity->getName() }}
                        </li>

                        @if($loop->iteration % 2 == 0)
                            <li class="clearfix hidden-md hidden-lg"></li>
                        @endif

                        @if($loop->iteration % 4 == 0)
                            <li class="clearfix"></li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <div class="bg-gray-lighter">
        <div class="container main-wrapper float-block">
            <div class="row">
                <div class="col-lg-4">
                    <p class="float-block__label h6 ls100 p-relative text-primary">
                        <span class="line-title"></span>

                        {{ $phrases['all-amenities'] }}
                    </p>
                </div>

                <div class="col-lg-8">
                    <ul class="services-list">
                        @foreach($amenityCategories as $amenityCategory)
                            <li class="services-list__item" id="s-{{ $amenityCategory->getSlug() }}">
                                <div class="services-list__item__header vertical-top ls50 c-gray-dark">
                                    {{ $amenityCategory->getName() }}
                                </div>

                                <div class="services-list__item__body ls25">
                                    <ul class="unordered-list">
                                        @foreach($amenityCategory->getAmenitiesList() as $amenity)
                                            <li>{{ $amenity }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
