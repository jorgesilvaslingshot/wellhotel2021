@extends('layouts.template')

@section('site.content')
    <div class="masthead masthead--full o-vertical-middle c-white">
        <div class="masthead__background fill bg-cover bg-center"
             style="background-image: url('{{ $masthead->getImage() }}');"></div>

             <div class="masthead__content o-vertical-middle__item">
                <h1 class="masthead__title ls50">
                    <span class="line-title"></span>

                    {{ $masthead->getTitle() }}
                </h1>

                <div class="masthead__subtitle font-libre-baskerville italic ls150 mb-0">
                    {!! $masthead->getDescription() !!}
                </div>
                <p class="gallery-above-image">
                    @if($event->hasGallery())
                    <my-gallery-link class="introduction__image-group"
                                     :images="{{ $event->getGallery()->getItemsToModal() }}"
                                     name="{{ $event->getGallery()->getName() }}" description="{{ $event->getGallery()->getDescription() }}">
                        <span class="introduction__image-group__link ls300 text-lowercase">
                            @icon('icon-pictures', ['class' => 'icon icon-pictures'])

                            <span class="hidden-xs">@lang('site.see-gallery')</span>
                        </span>
                    </my-gallery-link>
                    @endif
                </p>
            </div>




    </div>


    <div class="container main-wrapper adega-mae-container mt-0">
        <div class="row">
            <div class="eventos-esquerda">
                <div class="conclusion text-left">
                    @if(!empty($event->getName()))
                        <h3 class="page__body__title c-gray-dark ls50">{{ $event->getName() }}</h3>
                    @endif

                    <div class="page__body__description static-text ls25">
                        {!! $event->getDescription() !!}
                    </div>

                    <a href="{{ route('event-proposal') }}"
                        class="btn btn--main btn--animated ls300 fw-600">
                         <span class="p-relative">@lang('site.request-proposal')</span>
                     </a>

                </div>
            </div>

            {{-- somente para pag degustação --}}
            <div class="eventos-direita">

                    <p class="titulo-direito h6 ls100 p-relative text-primary titulo-linha">
                        {{$event->getTitleRight()}}
                    </p>

                    <ul class="ls25 mb-0 m-0">
                        @foreach($events_type as $event_type)
                        <li>{{ $event_type }}</li>
                        @endforeach
                    </ul>
            </div>

        </div>
    </div>
@endsection
