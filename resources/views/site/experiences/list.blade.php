@extends('layouts.template')

@section('site.content')
    <div class="masthead o-vertical-middle c-white">
        <div class="masthead__background fill bg-cover bg-center b-lazy"
             data-src="{{ $masthead->getImage() }}"></div>

        <div class="masthead__content o-vertical-middle__item">
            <h1 class="masthead__title ls50">
                <span class="line-title"></span>

                {{ $masthead->getTitle() }}
            </h1>

            <div class="masthead__subtitle font-libre-baskerville italic ls150 mb-0">
                {!! $masthead->getDescription() !!}
            </div>
        </div>

        <div class="wifi wifi--masthead font-libre-baskerville c-white italic ls300">
            @lang('site.free-wi-fi')

            @icon('icon-wifi')
        </div>
    </div>

    <my-submenu class="submenu" :scrollspy="true">
        <div class="submenu__content">
            <div class="submenu__col w-100">
                <div class="submenu__drag">
                    <my-drag-content>
                        <ul class="submenu__nav text-center text-nowrap ls25">
                            @foreach($experiences as $experience)
                                <li data-target="#s-{{ $experience->getSlug() }}">
                                    <my-scroll-to class="submenu__nav__link"
                                                  target="#s-{{ $experience->getSlug() }}">
                                        {{ $experience->getName() }}
                                    </my-scroll-to>
                                </li>
                            @endforeach
                        </ul>
                    </my-drag-content>
                </div>
            </div>
        </div>
    </my-submenu>

    @php
        $index = 0;
    @endphp

    @foreach($experiences as $experience)
        <div id="s-{{ $experience->getSlug() }}">
            <div class="block-type-2-item{{ $index % 2 == 0 ? ' block-type-2-item--inverted' : ''  }}">
                <a class="block-type-2 anchor-text parent-btn"
                   href="{{ route('experiences.detail', $experience->getSlug()) }}">
                    <div class="block-type-2__body o-vertical-middle">
                        <div class="o-vertical-middle__item">
                            <p class="block-type-2__label h6 ls100 p-relative text-primary">
                                <span class="line-title"></span>

                                {{ $experience->getLabel() }}
                            </p>

                            <h4 class="block-type-2__title ls50 c-gray-dark">
                                {{ $experience->getName() }}
                            </h4>

                            <div
                                class="block-type-2__description block-type-2__description--indented ls25 hidden-xs">
                                {!! $experience->getExcerpt() !!}
                            </div>

                            <div class="block-type-2__actions block-type-2__actions--indented hidden-xs">
                                    <span class="btn btn--main btn--animated ls300 fw-600">
                                        <span class="p-relative">@lang('site.learn-more')</span>
                                    </span>
                            </div>
                        </div>
                    </div>

                    <div class="block-type-2__object">
                        <div class="block-type-2__object__image b-lazy"
                             data-src="{{ img_cache($experience->getThumbnail(),'block-type-2-item') }}">
                        </div>
                    </div>
                </a>
            </div>

            @php
                $index++;
            @endphp
        </div>
    @endforeach
@endsection
