@extends('layouts.template')

@section('site.content')
    <div class="booking" id="booking-section">
        <my-booking-form inline-template action="{{ route('voucher-request') }}" method="post"
                         products-url="{{ route('booking-products') }}"
                         price-url="{{ route('booking-price') }}"
                         :initial-id="{{ !empty($id) ? "$id": 'null' }}"
                         :initial-type="{{ !empty($type) ? "'$type'": 'null' }}"
                         :vacation-data="{{$data}}"
                         :voucher-id="{{$id}}"
        >
            <transition name="fade" mode="out-in">
                <form class="booking__form"
                      v-on:submit.prevent="onSubmit"
                      v-on:keydown="form.errors.clear($event.target.name);"
                      v-if="!message"
                      key="form"
                >
                    <div class="booking__form__header">
                        <div class="container main-wrapper container-events">
                            <p class="booking__top__label h6 ls100 p-relative c-white booking-title-rental">
                                @lang('site.voucher-request')
                            </p>
                        </div>
                    </div>


                    <div class="container main-wrapper">
                        <div class="booking__row">
                            <div class="row">
                                <div class="col-sm-12 coluna-select--eventtype">
                                    <div class="booking__form__product">
                                        <div class="form-group" v-cloak="none">
                                            <label for="booking-form-product-name" class="sr-only"
                                                   v-text="form.voucher_type"></label>

                                            <div class="booking-select booking-select--top ls50 fw-300">
                                                <select tabindex="-1"
                                                        name="voucher_type"
                                                        id="booking-form-product-name"
                                                        v-model="form.voucher_type"
                                                >
                                                    <option value="" disabled>@lang('site.select-voucher')</option>

                                                    <option v-for="(voucher_type, index) in currentVoucherTypes" :key="index" :value="voucher_type.label"
                                                            v-text="voucher_type.label"></option>
                                                </select>

                                                <span tabindex="0">
                                                    @{{ form.voucher_type || '@lang('site.select-voucher')' | limitOverflow }}
                                                </span>
                                            </div>

                                            <span class="form-error"
                                                  v-if="form.errors.has('voucher_type')"
                                                  v-text="form.getError('voucher_type')"></span>
                                        </div>
                                    </div>
                                </div>



                                <div class="booking__form__dates col-xs-12 pull-left">
                                    <div class="form-group">
                                        <label for="booking-form-arrival"
                                               class="booking-label font-libre-baskerville italic ls200 booking-voucher-label">
                                               @{{ form.label_name}}
                                        </label>
                                    </div>
                                </div>

                                <div class="booking__form__people col-xs-12 pull-right">
                                    <div class="form-group">
                                        <label for="booking-form-adults"
                                               class="booking-label font-libre-baskerville italic ls200">
                                            @lang('site.adults')
                                        </label>


                                        <div class="booking-select ls50 fw-300" v-cloak="none">
                                            <select tabindex="-1"
                                                    name="adults"
                                                    id="booking-form-adults"
                                                    v-model="form.adults"
                                            >
                                                <option v-for="(value,index) in maxNumbers" :value="index"
                                                        v-text="value"></option>
                                            </select>

                                            <span tabindex="0">@{{ form.adults | pad }}</span>
                                        </div>
                                        <span class="form-error"
                                              v-if="form.errors.has('adults')"
                                              v-text="form.getError('adults')"></span>
                                    </div>

                                    <div class="form-group">
                                        <label for="booking-form-children"
                                               class="booking-label font-libre-baskerville italic ls200">
                                            @lang('site.children')
                                        </label>

                                        <div class="booking-select ls50 fw-300" v-cloak="none">
                                            <select tabindex="-1"
                                                    name="children"
                                                    id="booking-form-children"
                                                    v-model="form.children"
                                            >
                                                <option v-for="(value,index) in maxNumbers" :value="index"
                                                        v-text="value"></option>
                                            </select>

                                            <span tabindex="0">@{{ form.children | pad }}</span>
                                        </div>
                                        <span class="form-error"
                                              v-if="form.errors.has('children')"
                                              v-text="form.getError('children')"></span>
                                    </div>
                                </div>

                            </div>

                            <div class="booking__client">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="booking-label font-libre-baskerville italic ls200"
                                                   for="booking-form-name"
                                                   :class="{'sr-only':placeholderSupported}">
                                                @lang('site.name')
                                            </label>
                                            <input type="text"
                                                   class="form-control ls25"
                                                   id="booking-form-name"
                                                   placeholder="@lang('site.name')"
                                                   v-model="form.name">
                                            <span class="form-error"
                                                  v-if="form.errors.has('name')"
                                                  v-text="form.getError('name')"></span>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="booking-label font-libre-baskerville italic ls200"
                                                   for="booking-form-email"
                                                   :class="{'sr-only':placeholderSupported}">
                                                @lang('site.email')
                                            </label>
                                            <input type="email"
                                                   class="form-control ls25"
                                                   id="booking-form-email"
                                                   placeholder="@lang('site.email')"
                                                   v-model="form.email">
                                            <span class="form-error"
                                                  v-if="form.errors.has('email')"
                                                  v-text="form.getError('email')"></span>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="booking-label font-libre-baskerville italic ls200"
                                                   for="booking-form-phone"
                                                   :class="{'sr-only':placeholderSupported}">
                                                @lang('site.phone-mobile-phone')
                                            </label>
                                            <input type="tel"
                                                   class="form-control ls25"
                                                   id="booking-form-phone"
                                                   placeholder="@lang('site.phone')"
                                                   v-model="form.phone">

                                            <span class="form-error"
                                                  v-if="form.errors.has('phone')"
                                                  v-text="form.getError('phone')"></span>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 mt-4 coluna-event-description-form">
                                        <textarea name="" required id="event-description-form" cols="100" rows="4" v-model="form.description">

                                        </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="booking__actions">
                                <button type="submit" class="btn btn-block btn-primary text-uppercase fw-700 ls550">
                                    <span v-if="!form.processing">@lang('site.send')</span>
                                    <span v-else>@lang('site.wait')</span>
                                </button>
                            </div>

                            <div class="booking__footer text-center text-left-sm-up ls-25">
                                <div class="row">
                                    <div class="u-row-table">
                                        <div class="co-sm-6 col-md-7 u-row-table__cell-sm">
                                            <p>
                                                {{ $phrases['voucher-text'] }}
                                            </p>
                                        </div>
                                        <div
                                            class="col-sm-6 col-md-5 u-row-table__cell-sm text-right-sm-up vertical-bottom">
                                            <p>
                                                <a href="tel:{{ str_replace(' ', '', setting('contact_phone')) }}"><span>{{ setting('contact_phone') }}</span></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="booking__message o-vertical-middle" v-else key="message">
                    <div class="container-fluid o-vertical-middle__item">
                        <h3 class="booking__message__title ls50">{{ $phrases['voucher-request-successful-title'] }}</h3>

                        <div class="booking__message__content ls25">
                            {!! $phrases['voucher-request-successful-content'] !!}
                        </div>
                    </div>
                </div>
            </transition>
        </my-booking-form>

        <a class="booking__close" href="{{ $close }}">
            @icon('icon-close', ['class' => 'icon icon-close'])
        </a>
    </div>
@endsection
