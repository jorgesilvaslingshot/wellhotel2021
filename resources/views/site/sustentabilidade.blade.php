<div class="row coluna1-sustentabilidade">
        <p class="float-block__label h6 ls100 p-relative text-primary c-white" id="titulo-with-line">
            {{$phrases['sustainability-title'] }}
        </p>

    <div class="col-sm-12 u-row-table__cell-sm col-12-titulo-normal">
        <a href="https://youriguide.com/vZO061HNC8IGD1" target="_target" class="bottom-slider__slide__title h3 ls50 c-white">
            {{-- ECO – Well Hotel & Spa <br><span>Uma experiência com</span>                                              <span class="font-libre-baskerville italic ls200 fw-400 text-background-underline">responsabilidade ambiental</span> --}}
            {!! $phrases['sustainability-header-text'] !!}
        </a>
    </div>
</div>
<div class="row sustentabilida-descricao">
    <div class="col-sm-12">
        <h1 class="titulo-descricao text-center">
            {{$phrases['sustainability-title'] }}
        </h1>
        <p class="descricao-descricao text-center">
            {{$phrases['sustainability-homepage'] }}
        </p>

        <div class="actions-group text-center text-left-sm-up botao-mais-centrado"><a href="{{route('sustainability')}}" class="btn btn--main btn--animated ls300 fw-600"><span class="p-relative">{{trans('site.learn-more')}}</span></a></div>
    </div>
</div>
