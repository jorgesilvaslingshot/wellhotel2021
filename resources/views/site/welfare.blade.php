@extends('layouts.template')

@section('site.content')
    <div class="masthead masthead--full o-vertical-middle c-white" xmlns:v-on="http://www.w3.org/1999/xhtml">
        <div class="masthead__background fill bg-cover bg-center b-lazy"
             data-src="{{ $masthead->getImage() }}"></div>

        <div class="masthead__content o-vertical-middle__item">
            <h1 class="masthead__title ls50">
                <span class="line-title"></span>

                {{ $masthead->getTitle() }}
            </h1>

            <div class="masthead__subtitle font-libre-baskerville italic ls150 mb-0">
                {!! $masthead->getDescription() !!}
            </div>
        </div>

    </div>

    {{-- listagem --}}

    @php
        $index = 0;
    @endphp

    <div class="new-two-colomun">

        @php
        $index=0;
        @endphp
        @foreach($spaces as $space)

        @if ($index % 2 == 1)
        <div class="two-column">
            <a class="anchor-text parent-btn" href="{{ !empty($space->getUrl()) ? url($space->getUrl()) : route('spaces.detail', $space->getSlug()) }}">

                <div class="right-column cinza-bem-estar">

                    <p class="block-type-2__label h6 ls100 p-relative text-primary titulo-linha">

                        {{ $space->getType()->getName() }}
                    </p>

                    <h4 class="block-type-2__title ls50 c-gray-dark">
                        {{ $space->getName() }}
                    </h4>

                    <div
                        class="block-type-2__description block-type-2__description--indented ls25 hidden-xs">
                        {!! $space->getExcerpt() !!}
                    </div>

                    <div class="block-type-2__actions block-type-2__actions--indented hidden-xs">
                            <span class="btn btn--main btn--animated ls300 fw-600">
                                <span class="p-relative">@lang('site.learn-more')</span>
                            </span>
                    </div>
                </div>
                <div class="left-column bg-cover bg-center bg-no-repeat b-lazy"
                data-src="{{ $space->getImage() }}">

                </div>
            </a>
        </div>
        @else
        <div class="two-column">
            <a class="anchor-text parent-btn" href="{{ !empty($space->getUrl()) ? url($space->getUrl()) : route('spaces.detail', $space->getSlug()) }}">

                <div class="left-column bg-cover bg-center bg-no-repeat b-lazy"
                data-src="{{ $space->getImage() }}">

                </div>
                <div class="right-column" >
                    <p class="block-type-2__label h6 ls100 p-relative text-primary titulo-linha">

                        {{ $space->getType()->getName() }}
                    </p>

                    <h4 class="block-type-2__title ls50 c-gray-dark">
                        {{ $space->getName() }}
                    </h4>

                    <div
                        class="block-type-2__description block-type-2__description--indented ls25 hidden-xs">
                        {!! $space->getExcerpt() !!}
                    </div>

                    <div class="block-type-2__actions block-type-2__actions--indented hidden-xs">
                            <span class="btn btn--main btn--animated ls300 fw-600">
                                <span class="p-relative">@lang('site.learn-more')</span>
                            </span>
                    </div>
                </div>
            </a>
        </div>
        @endif
        @php
        $index++;
        @endphp
        @endforeach

    </div>

@endsection
