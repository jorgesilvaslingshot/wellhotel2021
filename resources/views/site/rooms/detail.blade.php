@extends('layouts.template')

@section('site.content')
    <div class="masthead masthead--full o-vertical-middle c-white">
        <div class="masthead__background fill bg-cover bg-center"
             style="background-image: url('{{ $room->getMastheadImage() }}');"></div>

        <div class="masthead__content o-vertical-middle__item">
            <div class="fraction-pagination font-libre-baskerville italic ls60">
                <span>{{ $roomNumber }}</span>
                <span>{{ $totalRooms }}</span>
            </div>

            <h1 class="masthead__title ls50">
                <span class="line-title hidden-xs"></span>

                {{ $room->getName() }}
            </h1>

            <p class="masthead__subtitle font-libre-baskerville italic ls150 mb-0">
                {{ $room->getLabel() }}
            </p>
            <p class="gallery-above-image">
                @if($room->hasGallery())
                <my-gallery-link class="introduction__image-group"
                                 :images="{{ $room->getGallery()->getItemsToModal() }}"
                                 name="{{ $room->getGallery()->getName() }}" description="{{ $room->getGallery()->getDescription() }}">
                    <span class="introduction__image-group__link ls300 text-lowercase">
                        @icon('icon-pictures', ['class' => 'icon icon-pictures'])

                        <span class="hidden-xs">@lang('site.see-gallery')</span>
                    </span>
                </my-gallery-link>
                @endif
            </p>
        </div>

    </div>

    <my-submenu class="submenu detalhes-space-pagination">
        <div class="submenu__content">

            <div class="submenu__col hidden-xs">
                <ul class="submenu__arrows">
                    <li>
                        <a href="{{ route('rooms.detail', $prevRoom->getSlug()) }}" class="arrow">
                            @icon('icon-slider-left', ['class' => 'icon icon-slider-left'])
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('rooms.detail', $nextRoom->getSlug()) }}" class="arrow">
                            @icon('icon-slider-right', ['class' => 'icon icon-slider-right'])
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </my-submenu>

    <div class="room-attributes">
        <div class="container main-wrapper">
            <div class="room-attributes__content p-relative">
                <div class="room-specifications">
                    <dl class="room-specifications__list p-relative mb-0">
                        @if(!empty($room->getMaxGuests()))
                            <dt class="h6 ls100 fw-500">@lang('site.max-guests')</dt>
                            <dd class="h5 ls30 c-gray-dark">{!! $room->getMaxGuests() !!}</dd>
                        @endif
                        @if(!empty($room->getRoomSize()))
                            <dt class="h6 ls100 fw-500">@lang('site.room-size')</dt>
                            <dd class="h5 ls30 c-gray-dark">{!! $room->getRoomSize() !!}</dd>
                        @endif
                        @if(!empty($room->getBedSize()))
                            <dt class="h6 ls100 fw-500">@lang('site.bed-size')</dt>
                            <dd class="h5 ls30 c-gray-dark mb-0">{!! nl2br($room->getBedSize()) !!}</dd>
                        @endif
                        @if(!empty($room->getLandscape()))
                            <dt class="h6 ls100 fw-500 mt-4">@lang('site.landscape')</dt>
                            <dd class="h5 ls30 c-gray-dark mb-0">{!! nl2br($room->getLandscape()) !!}</dd>
                        @endif
                    </dl>
                </div>

                <div class="room-amenities">
                    <p class="room-amenities__label h6 ls100 p-relative text-primary">
                        <span class="line-title"></span>

                        @lang('site.amenities')
                    </p>

                    <ul class="room-amenities__list unordered-list ls25 mb-0">
                        @foreach($room->getOtherAmenities() as $amenity)
                            <li>{{ $amenity }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @if($room->hasAmenities())
        <div class="container main-wrapper">
            <ul class="amenities-list amenities-list--room">
                @foreach($room->getAmenities() as $amenity)
                    <li class="amenities-list__item text-center ls25{{ $loop->iteration == 5 ? ' hidden-xs hidden-sm' : '' }}">
                        @icon($amenity->getIcon(), ['class'=> 'text-primary icon '.$amenity->getIcon()])

                        {{ $amenity->getName() }}
                    </li>

                    @if($loop->iteration % 2 == 0)
                        <li class="clearfix hidden-md hidden-lg"></li>
                    @endif

                    @if($loop->iteration % 5 == 0)
                        <li class="clearfix hidden-xs hidden-sm"></li>
                    @endif
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container main-wrapper">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <div class="conclusion text-center">
                    <h3 class="conclusion__title ls50 c-gray-dark">{{ $room->getAdditionalTitle() }}</h3>

                    <div class="conclusion__description">
                        {!! $room->getAdditionalDescription() !!}
                    </div>

                    <div class="actions-group">
                        <a href="{{ route('booking', ['id' => $room->getId(), 'type' => 'room']) }}"
                           class="btn btn--main-inverted btn--animated ls300 fw-600">
                            <span class="p-relative">@lang('site.book-now')</span>
                        </a>

                        <a class="addthis_button_more" href="#">
                            <button href="#" class="btn btn--main btn--animated ls300 fw-600 ">
                                <span class="p-relative">@lang('site.share')</span>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
