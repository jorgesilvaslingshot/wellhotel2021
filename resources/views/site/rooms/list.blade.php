@extends('layouts.template')

@section('site.content')
    <div class="masthead o-vertical-middle c-white">
        <div class="masthead__background fill bg-cover bg-center b-lazy"
             data-src="{{ $masthead->getImage() }}"></div>

        <div class="masthead__content o-vertical-middle__item">
            <h1 class="masthead__title ls50">
                <span class="line-title"></span>

                {{ $masthead->getTitle() }}
            </h1>

            <div class="masthead__subtitle font-libre-baskerville italic ls150 mb-0">
                {!! $masthead->getDescription() !!}
            </div>
        </div>
    </div>
    @php
        $index = 0;
    @endphp

    @foreach($roomTypes as $roomType)
        <div id="s-{{ $roomType->getSlug() }}">
            @foreach($roomType->getRooms() as $room)
                <div class="introduction-list-item{{ $index % 2 == 0 ? ' introduction-list-item--inverted' : '' }}">
                    <div class="container main-wrapper">
                        <div class="introduction">
                            <p class="introduction__label h6 ls100 p-relative text-primary">
                                <span class="line-title"></span>

                                {{ $room->getLabel() }}
                            </p>

                            <h2 class="introduction__title ls50 c-gray-dark">
                                {{ $room->getName() }}
                            </h2>

                                <div class="introduction__image-group">
                                    <img class="img-responsive b-lazy" src="{{ img_placeholder() }}"
                                         data-src="{{ img_cache($room->getCoverImage(), 'introduction-cover-item') }}"
                                         width="460" height="300" alt="">
                                    </div>


                            <div class="actions-group text-center text-left-sm-up">
                                <a href="{{ route('booking', ['id' => $room->getId(), 'type' => 'room']) }}"
                                   class="btn btn--main-inverted btn--animated ls300 fw-600">
                                    <span class="p-relative">@lang('site.book-now')</span>
                                </a>

                                <a href="{{ route('rooms.detail', $room->getSlug()) }}"
                                   class="btn btn--main btn--animated ls300 fw-600">
                                    <span class="p-relative">@lang('site.more-info')</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                @php
                    $index++;
                @endphp
            @endforeach
        </div>
    @endforeach

    @include('partials.bottom-slider')
@endsection
