@extends('layouts.template')

@section('site.content')
    <div class="masthead masthead--full o-vertical-middle c-white" xmlns:v-on="http://www.w3.org/1999/xhtml">
        <div class="masthead__background fill bg-cover bg-center b-lazy"
             data-src="{{ $masthead->getImage() }}"></div>

        <div class="masthead__content o-vertical-middle__item pl-4">
            <h1 class="masthead__title ls50">
                <span class="line-title"></span>

                {{ $masthead->getTitle() }}
            </h1>

            <div class="masthead__subtitle font-libre-baskerville italic ls150 mb-0">
                {!! $masthead->getDescription() !!}
            </div>
        </div>

    </div>

    <div class="container main-wrapper p-relative" id="s-{{ str_slug($locationBlock->getLabel()) }}">
        <div class="introduction" id="{{ str_slug($locationBlock->getLabel()) }}">
            <p class="introduction__label h6 ls100 p-relative text-primary">
                <span class="line-title"></span>

                {{ $locationBlock->getLabel() }}
            </p>

            <h3 class="introduction__title ls50 c-gray-dark">
                {{ $locationBlock->getTitle() }}
            </h3>

            @if($locationBlock->hasGallery())
                <my-gallery-link class="introduction__image-group"
                                 :images="{{ $locationBlock->getGallery()->getItemsToModal() }}"
                                 name="{{ $locationBlock->getGallery()->getName() }}">
                    <img class="img-responsive b-lazy" src="{{ img_placeholder() }}"
                         data-src="{{ img_cache($locationBlock->getImage(), 'introduction-cover-item') }}" width="460"
                         height="300">
                    <img class="b-lazy" src="{{ img_placeholder() }}"
                         data-src="{{ img_cache($locationBlock->getThumbnail(), 'introduction-cover-item-2') }}"
                         width="265" height="170">

                    <span class="introduction__image-group__link ls300 text-lowercase">
                        @icon('icon-pictures', ['class' => 'icon icon-pictures'])

                        <span class="hidden-xs">@lang('site.see-gallery')</span>
                    </span>
                </my-gallery-link>
            @else
                <div class="introduction__image-group">
                    <img class="img-responsive b-lazy" src="{{ img_placeholder() }}"
                         data-src="{{ img_cache($locationBlock->getImage(), 'introduction-cover-item') }}" width="460"
                         height="300">
                    <img class="b-lazy" src="{{ img_placeholder() }}"
                         data-src="{{ img_cache($locationBlock->getThumbnail(), 'introduction-cover-item-2') }}"
                         width="265" height="170">
                </div>
            @endif

            <div class="introduction__description ls25">
                {!! $locationBlock->getDescription() !!}
            </div>

            <div class="actions-group">
                <a class="addthis_button_more" href="#">
                    <button href="#" class="btn btn--main btn--animated ls300 fw-600 ">
                        <span class="p-relative">@lang('site.share')</span>
                    </button>
                </a>
            </div>
        </div>
    </div>

@endsection
