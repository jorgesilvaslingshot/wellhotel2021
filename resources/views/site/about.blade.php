@extends('layouts.template')

@section('site.content')
    <div class="masthead masthead--full o-vertical-middle c-white" xmlns:v-on="http://www.w3.org/1999/xhtml">
        <div class="masthead__background fill bg-cover bg-center b-lazy"
             data-src="{{ $masthead->getImage() }}"></div>

        <div class="masthead__content o-vertical-middle__item">
            <h1 class="masthead__title ls50">
                <span class="line-title"></span>

                {{ $masthead->getTitle() }}
            </h1>

            <div class="masthead__subtitle font-libre-baskerville italic ls150 mb-0">
                {!! $masthead->getDescription() !!}
            </div>
        </div>

        <div class="wifi wifi--masthead font-libre-baskerville c-white italic ls300">
            @lang('site.free-wi-fi')

            @icon('icon-wifi')
        </div>
    </div>

    <div class="container main-wrapper p-relative" id="s-{{ str_slug($locationBlock->getLabel()) }}">
        <div class="introduction" id="{{ str_slug($locationBlock->getLabel()) }}">
            <p class="introduction__label h6 ls100 p-relative text-primary">
                <span class="line-title"></span>

                {{ $locationBlock->getLabel() }}
            </p>

            <h3 class="introduction__title ls50 c-gray-dark">
                {{ $locationBlock->getTitle() }}
            </h3>

            @if($locationBlock->hasGallery())
                <my-gallery-link class="introduction__image-group"
                                 :images="{{ $locationBlock->getGallery()->getItemsToModal() }}"
                                 name="{{ $locationBlock->getGallery()->getName() }}">
                    <img class="img-responsive b-lazy" src="{{ img_placeholder() }}"
                         data-src="{{ img_cache($locationBlock->getImage(), 'introduction-cover-item') }}" width="460"
                         height="300">
                    <img class="b-lazy" src="{{ img_placeholder() }}"
                         data-src="{{ img_cache($locationBlock->getThumbnail(), 'introduction-cover-item-2') }}"
                         width="265" height="170">

                    <span class="introduction__image-group__link ls300 text-lowercase">
                        @icon('icon-pictures', ['class' => 'icon icon-pictures'])

                        <span class="hidden-xs">@lang('site.see-gallery')</span>
                    </span>
                </my-gallery-link>
            @else
                <div class="introduction__image-group">
                    <img class="img-responsive b-lazy" src="{{ img_placeholder() }}"
                         data-src="{{ img_cache($locationBlock->getImage(), 'introduction-cover-item') }}" width="460"
                         height="300">
                    <img class="b-lazy" src="{{ img_placeholder() }}"
                         data-src="{{ img_cache($locationBlock->getThumbnail(), 'introduction-cover-item-2') }}"
                         width="265" height="170">
                </div>
            @endif

            <div class="introduction__description ls25">
                {!! $locationBlock->getDescription() !!}
            </div>
        </div>
    </div>

    @php
        $index = 0;
    @endphp

    <div class="new-two-colomun">

        @php
        $index=0;
        @endphp
        @foreach($spaces as $space)

        @if ($index % 2 != 1)
        <div class="two-column hotel-cinza-claro">
            <a class="anchor-text parent-btn" href="{{ !empty($space->getUrl()) ? url($space->getUrl()) : route('spaces.detail', $space->getSlug()) }}">

                <div class="right-column">

                    <p class="block-type-2__label h6 ls100 p-relative text-primary titulo-linha">

                        {{ $space->getType()->getName() }}
                    </p>

                    <h4 class="block-type-2__title ls50 c-gray-dark">
                        {{ $space->getName() }}
                    </h4>

                    <div
                        class="block-type-2__description block-type-2__description--indented ls25 hidden-xs">
                        {!! $space->getExcerpt() !!}
                    </div>

                    <div class="block-type-2__actions block-type-2__actions--indented hidden-xs">
                            <span class="btn btn--main btn--animated ls300 fw-600">
                                <span class="p-relative">@lang('site.learn-more')</span>
                            </span>
                    </div>
                </div>
                <div class="left-column bg-cover bg-center bg-no-repeat b-lazy"
                data-src="{{ $space->getImage() }}">

                </div>
            </a>
        </div>
        @else
        <div class="two-column hotel-cinza-escuro">
            <a href="{{ !empty($space->getUrl()) ? url($space->getUrl()) : route('spaces.detail', $space->getSlug()) }}" class="anchor-text parent-btn">

                <div class="left-column bg-cover bg-center bg-no-repeat b-lazy"
                data-src="{{ $space->getImage() }}">

                </div>
                <div class="right-column" >
                    <p class="block-type-2__label h6 ls100 p-relative text-primary titulo-linha">

                        {{ $space->getType()->getName() }}
                    </p>

                    <h4 class="block-type-2__title ls50 c-gray-dark">
                        {{ $space->getName() }}
                    </h4>

                    <div
                        class="block-type-2__description block-type-2__description--indented ls25 hidden-xs">
                        {!! $space->getExcerpt() !!}
                    </div>

                    <div class="block-type-2__actions block-type-2__actions--indented hidden-xs">
                            <span class="btn btn--main btn--animated ls300 fw-600">
                                <span class="p-relative">@lang('site.learn-more')</span>
                            </span>
                    </div>
                </div>
            </a>
        </div>
        @endif
        @php
        $index++;
        @endphp
        @endforeach

    </div>

    <div class="container main-wrapper float-block float-block--amenities" id="s-hotel-amenities">
        <div class="row">
            <div class="col-lg-3">
                <p class="float-block__label h6 ls100 p-relative text-primary titulo-linha">
                    <span class="line-title"></span>

                    {{ $phrases['main-hotel-amenities'] }}
                </p>
            </div>

            <div class="col-lg-9">
                <ul class="amenities-list amenities-list--main mb-md-0">
                    @foreach($mainAmenities as $amenity)
                        <li class="amenities-list__item text-center ls25">
                            @icon($amenity->getIcon(), ['class'=> 'text-primary icon '.$amenity->getIcon()])

                            {{ $amenity->getName() }}
                        </li>
                    @endforeach
                </ul>

            </div>
        </div>
    </div>

    <div class="bg-gray-lighter">
        <div class="container main-wrapper float-block" id="s-guest-services">
            <div class="row">
                <div class="col-lg-4">
                    <p class="float-block__label h6 ls100 p-relative text-primary">
                        <span class="line-title"></span>

                        {{ $phrases['guest-services'] }}
                    </p>
                </div>

                <div class="col-lg-8">
                    <ul class="services-list">
                        @foreach($guestServices as $guestService)
                            <li class="services-list__item">
                                <div class="services-list__item__header ls50 c-gray-dark">
                                    {{ $guestService->getTitle() }}
                                </div>

                                <div class="services-list__item__body vertical-middle ls25">
                                    {!! $guestService->getDescription() !!}
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @if($locationBlock->hasGallery())
        <my-gallery-slider :images="{{ $locationBlock->getGallery()->getItemsToModal() }}"
                           name="{{ $locationBlock->getGallery()->getName() }}" inline-template>
            <div class="gallery-slider slick slick--default">
                @foreach($locationBlock->getGallery()->getItems() as $item)
                    <div class="slick-slide">
                        <div class="gallery-slider__image fill bg-cover bg-center bg-no-repeat b-lazy"
                             data-src="{{ $item->getImage() }}"></div>
                    </div>
                @endforeach
            </div>
        </my-gallery-slider>
    @endif

    <div class="container main-wrapper">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <div class="conclusion text-center">
                    {{--<h3 class="conclusion__title ls50 c-gray-dark">{{ $location2Block->getTitle() }}</h3>--}}

                    <div class="conclusion__description">
                        {!! $location2Block->getDescription() !!}
                    </div>

                    <div class="actions-group">
                        <a href="{{ route('booking') }}"
                           class="btn btn--main-inverted btn--animated ls300 fw-600">
                            <span class="p-relative">@lang('site.book-now')</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
