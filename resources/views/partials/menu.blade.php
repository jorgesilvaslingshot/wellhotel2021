<transition name="fade">
    <div class="menu" v-show="showMenuSection" v-cloak="sr-only">
        <div class="menu__body">
            <a class="site-logo text-primary" href="/">
                @icon('icon-logo-well', ['class' => 'icon icon-logo-well'])
            </a>

            <div class="menu__content o-vertical-middle text-center">
                <div class="o-vertical-middle__item text-left">
                    <div class="container-fluid ls50">
                        <ol class="menu-list">
                            <li class="{{ Route::currentRouteNamed('about*') ? 'active' : '' }}" data-index="1">
                                <span class="line-title"></span>
                                <a href="{{ route('about') }}">@lang('site.the-hotel')</a>
                            </li>
                            <li class="{{ Route::currentRouteNamed('rooms*') ? 'active' : '' }}" data-index="2">
                                <span class="line-title"></span>
                                <a href="{{ route('rooms.list') }}">@lang('site.rooms-and-suites')</a>
                            </li>
                            <li class="{{ Route::currentRouteNamed('welfare*') ? 'active' : '' }}" data-index="3">
                                <span class="line-title"></span>
                                <a href="{{ route('welfare') }}">@lang('site.welfare')</a>
                            </li>

                        </ol>
                        <ol class="menu-list">
                            <li class="{{ Route::currentRouteNamed('experiences*') ? 'active' : '' }}" data-index="5">
                                <span class="line-title"></span>
                                <a href="{{ route('sustainability') }}">@lang('site.eco')</a>
                            </li>
                            <li class="{{ Route::currentRouteNamed('packs*') ? 'active' : '' }}" data-index="6">
                                <span class="line-title"></span>
                                <a href="{{ route('packs.list') }}">@lang('site.packs-experience')</a>
                            </li>
                            <li class="{{ Route::currentRouteNamed('event*') ? 'active' : '' }}" data-index="7">
                                <span class="line-title"></span>
                                <a href="{{ route('event') }}">@lang('site.events')</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>

            <div class="menu__footer">
                <div class="menu-separator hidden-xs"></div>

                <div class="menu-info-container container-fluid hidden-xs">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="menu-info text-center">
                                <p class="menu-info__title ls100 fw-500">
                                    {{ $phrases['transfers-lisbon'] }}
                                </p>

                                <div class="ls70">
                                    {!! nl2br($phrases['transfers-lisbon-description']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="menu-info text-center">
                                <p class="menu-info__title ls100 fw-500">
                                    {{ $phrases['you-will-find-us-here'] }}
                                </p>

                                <address class="ls70 mb-0">
                                    {!! nl2br(setting('address')) !!}
                                </address>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="menu-info text-center">
                                <p class="menu-info__title ls100 fw-500">
                                    {{ $phrases['contact-us'] }}
                                </p>

                                <p class="menu-info__email font-merriweather italic ls60 fw-300 mb-0">
                                    <a href="mailto:{{ setting('contact_email') }}">{{ setting('contact_email') }}</a>
                                </p>

                                <p class="menu-info__phone ls130 mb-0">
                                    <a href="tel:{{ str_replace(' ', '', setting('contact_phone')) }}"><span>{{ setting('contact_phone') }}</span></a>
                                </p>

                                <p class="menu-info__phone ls130 mb-0">
                                    <a href="tel:{{ str_replace(' ', '', setting('contact_mobile_phone')) }}"><span>{{ setting('contact_mobile_phone') }}</span></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="menu-separator"></div>

                <div class="menu-footer">
                    <div class="menu-footer__col hidden-xs">
                        <div class="menu-footer__tripadvisor">
                            <a href="https://www.tripadvisor.pt/Hotel_Review-g656858-d1053397-Reviews-Well_Hotel_Spa-Torres_Vedras_Lisbon_District_Central_Portugal.html" target="_blank">
                                @icon('icon-tripadvisor', ['class' => 'icon icon-tripadvisor'])
                            </a>
                        </div>
                    </div>

                    <div class="menu-footer__col text-center-sm-up">
                        <a class="menu-footer__location fw-700 ls600" href="{{ setting('directions_link') }}" target="_blank">
                            @icon('icon-location', ['class' => 'icon icon-location'])

                            <span class="text-uppercase">@lang('site.location')</span>
                        </a>
                    </div>

                    <div class="menu-footer__col text-right">
                        <ul class="menu-footer__social list-inline mb-0">
                            @if(!empty(setting('facebook_link')))
                                <li>
                                    <a target="_blank" href="{{ setting('facebook_link') }}">
                                        @icon('icon-facebook-novo', ['class' => 'icon icon-facebook-novo'])
                                    </a>
                                </li>
                            @endif
                            @if(!empty(setting('instagram_link')))
                                <li class="ml-2">
                                    <a target="_blank"
                                       href="{{ setting('instagram_link') }}">
                                        @icon('icon-instagram-novo', ['class' => 'icon icon-instagram-novo'])
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</transition>
