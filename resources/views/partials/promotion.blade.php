@if(!empty($promotion))
    <my-promotion inline-template code="{{ $promotion->getCode() }}" @booking="openBooking()">
        <transition name="fade">
            <div class="promotion media bg-white" v-show="showPromotion" v-cloak="none">
                <button type="button" class="close" aria-label="Close" v-on:click="hidePromotion">
                    @icon('icon-close')
                </button>

                <div class="media-left pr-0">
                    <a href="{{ !empty($promotion->getLink()) ? $promotion->getLink():'' }}"
                       @if(empty($promotion->getLink())) v-on:click.prevent="booking" @endif>
                        <img class="media-object"
                             src="{{ img_cache($promotion->getImage(), 'promotion') }}"
                             alt="{{ $promotion->getTitle() }}">
                    </a>
                </div>
                <div class="media-body media-middle">
                    <a class="anchor-text" href="{{ !empty($promotion->getLink()) ? $promotion->getLink():'' }}"
                       @if(empty($promotion->getLink())) v-on:click.prevent="booking" @endif>
                        <p class="h6 text-primary ls100 p-relative">
                            <span class="line-title"></span>
                            {{ $promotion->getLabel() }}
                        </p>
                        <h5 class="mb-0 ls30 c-gray-dark">{{ $promotion->getTitle() }}</h5>
                    </a>
                </div>
            </div>
        </transition>
    </my-promotion>
@endif
