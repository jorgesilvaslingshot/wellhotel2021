@if(!empty($analyticsTrackingId))
    <script async src="https://www.googletagmanager.com/gtag/js?id={!! $analyticsTrackingId !!}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '@php echo $analyticsTrackingId @endphp');
    </script>
@endif
