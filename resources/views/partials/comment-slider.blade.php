<my-comment-slider inline-template>

    <div class="bottom-slider">

        <div class="bottom-slider__backgrounds fill slick" ref="backgroundSlick">
            @foreach($messages as $slide)
                <div class="slick-slide">

                </div>
            @endforeach
        </div>


        <div class="slick slick--default slider-comment" ref="slick">
            @foreach($messages as $slide)
                <div class="slick-slide">
                    <div class="bottom-slider__slide">
                        <p class="bottom-slider__slide__label h6 ls100 text-primary titulo-with-line-general">
                            {{trans('site.customers-say')}}
                        </p>
                        <div class="fraction-pagination font-libre-baskerville italic ls60" id="comment-slider-control">
                            <span>{{ $loop->iteration }}</span>
                            <span>{{ $loop->count }}</span>

                        </div>

                        <div class="container main-wrapper">
                            <div class="p-relative">
                                <div class="bottom-slider__slide__content container-fluid mr-0">
                                    <div class="row">
                                        <div class="u-row-table">
                                            <div class="col-md-8 col-lg-7 u-row-table__cell-sm">
                                                    <a href=""
                                                       class="bottom-slider__slide__title h3 ls50">
                                                        {!! $slide->getAuthor() !!}
                                                    </a>
                                            </div>

                                            <div class="comentario-cliente font-merriweather italic ls60 my-3">
                                                    {!! $slide->getMessage() !!}
                                            </div>
                                            <div class="data-comentario fw-500">
                                                {{$slide->getDate()->formatLocalized('%B %e, %Y')}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</my-comment-slider>
