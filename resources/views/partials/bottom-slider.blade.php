<my-bottom-slider inline-template>
    <div class="bottom-slider">
        <div class="bottom-slider__backgrounds fill slick" ref="backgroundSlick">
            @foreach($bottomSlides as $slide)
                <div class="slick-slide">
                    <div class="bottom-slider__image fill bg-cover bg-center bg-no-repeat b-lazy"
                         data-src="{{ $slide->getImage() }}"></div>
                </div>
            @endforeach
        </div>


        <div class="slick slick--default" ref="slick">
            @foreach($bottomSlides as $slide)
                <div class="slick-slide">
                    <div class="bottom-slider__slide">
                        <div class="fraction-pagination font-libre-baskerville italic ls60 c-white">
                            <span>{{ $loop->iteration }}</span>
                            <span>{{ $loop->count }}</span>
                        </div>

                        <div class="lazer-homepage h6 ls100 c-white">
                            {{ $slide->getLabel() }}
                        </div>

                        <div class="container main-wrapper">
                            <div class="p-relative">

                                <div class="bottom-slider__slide__content container-fluid">
                                    <div class="row">
                                        <div class="u-row-table">
                                            <div class="col-md-8 col-lg-7 u-row-table__cell-sm">
                                                @if($slide->hasAction())
                                                    <a href="{{ $slide->getActionLink() }}"
                                                       class="bottom-slider__slide__title h3 ls50 c-white">
                                                        {!! $slide->getTitle() !!}
                                                    </a>
                                                @else
                                                    <div class="bottom-slider__slide__title h3 ls50 c-white">
                                                        {!! $slide->getTitle() !!}
                                                    </div>
                                                @endif
                                            </div>

                                            <div
                                                class="col-md-4 col-lg-5 u-row-table__cell-sm text-right-sm-up vertical-middle hidden-xs">
                                                @if($slide->hasAction())
                                                    <a href="{{ $slide->getActionLink() }}"
                                                       class="btn btn--white btn--animated ls300 fw-600">
                                                        <span class="p-relative">{{ $slide->getActionTitle() }}</span>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</my-bottom-slider>
