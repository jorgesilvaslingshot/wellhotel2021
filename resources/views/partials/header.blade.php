<header xmlns:v-on="http://www.w3.org/1999/xhtml">
    <a class="site-logo c-white" href="/">
        @icon('icon-logo-well', ['class' => 'icon icon-logo-well'])
    </a>

    <my-top-nav class="top-nav">
        <div class="top-nav__item">
            <transition name="fade">
                <ul class="top-nav__languages" v-cloak="sr-only" v-show="showMenuSection">
                    <li class="{{ LaravelLocalization::getCurrentLocale() == 'pt' ? 'active' : '' }}"><a
                            href="{{ url('pt') }}">PT</a></li>
                    <li class="{{ LaravelLocalization::getCurrentLocale() == 'en' ? 'active' : '' }}"><a
                            href="{{ url('en') }}">EN</a></li>
                </ul>
            </transition>
        </div>
        <div class="top-nav__item">
            <a class="top-nav__book-now" href="{{ route('booking') }}">
                @svg('icon-book-now', ['class' => 'icon visible-xs visible-sm'])
                <span class="visible-md visible-lg ls25 fw-600 text-capitalize">@lang('site.book-now')</span>
            </a>
        </div>
        <div class="top-nav__item">
            <a class="top-nav__menu-toggle" href="#" v-on:click.prevent="toggleMenu">
                <div v-if="!showMenuSection">
                    @svg('icon-menu', ['class' => 'icon icon-menu'])
                </div>
                <div v-else v-cloak="none">
                    @svg('icon-close', ['class' => 'icon icon-close'])
                </div>
            </a>
        </div>
    </my-top-nav>

    @include('partials.menu')

    @include('partials.booking')
</header>
