<footer>
    <div class="footer">
        <div class="container main-wrapper">
            <div class="row u-row-no-padding">
                <div class="u-row-table-sm u-row-no-padding">
                    <div class="footer-info-content col-sm-12 u-row-table__cell-sm">
                        <div class="footer-info ls70 text-center text-left-sm-up">
                            <p class="footer-info__title p-relative ls100 fw-500">
                                <span class="line-title"></span>

                                {{ $phrases['contact-us'] }}
                            </p>

                            <p class="footer-info__email font-merriweather italic ls60 fw-300">
                                <a href="mailto:{{ setting('contact_email') }}">{{ setting('contact_email') }}</a>
                            </p>

                            <p class="footer-info__phone ls130 mb-0">
                                <a href="tel:{{ str_replace(' ', '', setting('contact_phone')) }}"><span>{{ setting('contact_phone') }}</span></a>
                            </p>

                            <p class="footer-info__phone ls130 mb-0">
                                <a href="tel:{{ str_replace(' ', '', setting('contact_mobile_phone')) }}"><span>{{ setting('contact_mobile_phone') }}</span></a>
                            </p>
                        </div>

                        <div class="footer-info ls70 text-center text-left-sm-up">
                            <p class="footer-info__title p-relative ls100 fw-500">
                                <span class="line-title"></span>

                                {{ $phrases['you-will-find-us-here'] }}
                            </p>

                            <address class="mb-0">
                                {!! nl2br(setting('address')) !!}
                            </address>
                        </div>
                        {{-- 3º coluna --}}
                        <div class="footer-info ls70 text-center text-left-sm-up">
                            <p class="footer-info__title p-relative ls100 fw-500">
                                <span class="line-title"></span>

                                {{ $phrases['contact-us-here'] }}
                            </p>

                            <p class="footer-info__social-network font-merriweather ls60 fw-300">
                                <img src="/files/new_svg/tripadvisor-novo.svg" alt="">
                                <img src="/files/new_svg/facebook-novo.svg" alt="">
                                <img src="/files/new_svg/instagram-novo.svg" alt="">
                            </p>
                            <ul class="img-descricao footer-social list-inline hidden-xs mb-0">
                                <li>
                                    <a class="botao-social" target="_blank" href="">Tripadvisor</a>
                                </li>
                            @if(!empty(setting('instagram_link')))
                                <li>
                                    <a class="botao-social" target="_blank"
                                       href="{{ setting('facebook_link') }}">| Facebook</a>
                                </li>
                            @endif
                            @if(!empty(setting('facebook_link')))
                                <li>
                                    <a class="botao-social" target="_blank" href="{{ setting('instagram_link') }}">| Instagram</a>
                                </li>
                            @endif

                            </ul>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom container-fluid main-wrapper bg-white">
        <ul class="footer-bottom__list text-center text-left-sm-up">
            <button id="scrollToTopBtn" @click="scrollToTopMethod"><span>Topo</span>@icon('icon-seta-topo', ['class' => 'icon icon-seta-topo'])</button>
            <li>Well Hotel & Spa © 2020</li>
            <li><a href="{{ trans('url.privacy-policy') }}">@lang('site.privacy-policy')</a></li>
            <li><a href="{{ trans('url.contacts') }}">@lang('site.contacts')</a></li>
        </ul>

        <div class="pull-right hidden-xs">
            <a href="http://www.slingshot.pt" target="_blank">@lang('site.by-slingshot')</a>
        </div>
    </div>
</footer>
