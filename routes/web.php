<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Multilanguage Routes
|--------------------------------------------------------------------------
|
| To use multilanguage routes set LOCALIZATION_ENABLE=true in .env file.
|
*/

if (config('laravellocalization.enable')) {
    Route::group([
        'prefix' => app(\Mcamara\LaravelLocalization\LaravelLocalization::class)->setLocale(),
        'middleware' => ['localization']
    ], function () {
        Route::get('/', 'Welcome')->name('routes.welcome');
        Route::get(trans('routes.about'), 'About')->name('about');

        Route::get(trans('routes.sustainability'), 'About@sustainability')->name('sustainability');

        Route::get(trans('routes.event'), 'Event')->name('event');

        Route::get('eco', 'About@eco')->name('eco');

        Route::get(trans('routes.welfare'), 'Welfare')->name('welfare');
        Route::get(trans('routes.spaces.detail'), 'SpaceDetail')->name('spaces.detail');
        Route::get(trans('routes.rooms.list'), 'Rooms')->name('rooms.list');
        Route::get(trans('routes.rooms.detail'), 'RoomDetail')->name('rooms.detail');
        Route::get(trans('routes.experiences.list'), 'Experiences')->name('experiences.list');
        Route::get(trans('routes.experiences.detail'), 'ExperienceDetail')->name('experiences.detail');
        Route::get(trans('routes.packs.list'), 'Packs')->name('packs.list');
        Route::get(trans('routes.packs.detail'), 'PackDetail')->name('packs.detail');
        Route::get(trans('routes.amenities'), 'Amenities')->name('amenities');

        Route::get(trans('routes.booking'), 'Booking')->name('booking');

        Route::get(trans('routes.event-proposal'), 'Booking@event')->name('event-proposal');

        Route::get(trans('routes.booking-voucher'), 'Booking@voucher')->name('booking-voucher');

        Route::post('/api/contact-request', 'Api\SubmitContactRequest')->name('contact-request');
        Route::post('/api/booking-request', 'Api\SubmitBookingRequest')->name('booking-request');
        Route::post('/api/proposal-request', 'Api\SubmitProposalRequest')->name('proposal-request');
        Route::post('/api/voucher-request', 'Api\SubmitVoucherRequest')->name('voucher-request');

        Route::get('/api/booking-products', 'Api\GetBookingProducts')->name('booking-products');
        Route::get('/api/booking-price', 'Api\GetBookingPrice')->name('booking-price');
        Route::get('/api/vacation', 'Api\GetVacationsIntervals')->name('vacation');

        Route::get('/api/event-type', 'Api\GetEventType')->name('event-type');

        Route::get('/api/voucher-type', 'Api\GetVoucherType')->name('voucher-type');

        // Route::get(trans('routes.hotel-restaurant'), 'PageDetail@restaurant')->name('hotel.restaurant');
        // Route::get(trans('routes.hotel-bar'), 'PageDetail@bar')->name('hotel.bar');

        Route::fallback('PageDetail');
    });
}
