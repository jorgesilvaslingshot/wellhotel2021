const fs = require('fs');
const objectValues = require('lodash').values;
const path = require('path');
const webpack = require('webpack');

// Webpack Plugins

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');

// Config

const resourceFolder = 'resources/assets';
const publicFolder = 'public_html';
const distFolder = 'assets';
const manifestName = 'mix-manifest.json';
const productionSourceMap = false; // source map type or false

// Webpack configuration

module.exports = (env = {}) => {
    const NODE_ENV = env.NODE_ENV || 'development';
    const inProduction = NODE_ENV === 'production';
    const hmr = process.argv.includes('--hot');
    const resourcePath = path.resolve(__dirname, resourceFolder);
    const publicPath = path.resolve(__dirname, publicFolder);
    const distPath = path.resolve(publicPath, distFolder);
    const shouldCreateSourceMap = !inProduction || !!productionSourceMap;

    return {
        entry: {
            'js/app': [
                path.resolve(resourcePath, 'js/app.js'),
                path.resolve(resourcePath, 'sass/app.scss')
            ]
        },

        output: {
            filename: '[name].js' + (hmr ? '' : '?id=[chunkhash]'),
            path: hmr ? path.resolve('/') : distPath,
            chunkFilename: 'js/[name].js' + (hmr ? '' : '?id=[chunkhash]'),
            publicPath: (hmr ? 'http://localhost:8080' : '') + path.join('/', distFolder, '/')
        },

        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.(vue|js)$/,
                    loader: 'eslint-loader',
                    exclude: /node_modules/
                },

                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: 'babel-loader'
                },

                {
                    test: /\.vue$/,
                    loader: 'vue-loader',
                    options: {
                        loaders: {
                            scss: [
                                'vue-style-loader',
                                'css-loader',
                                {
                                    loader: 'sass-loader',
                                    options: {
                                        indentedSyntax: true
                                    }
                                }
                            ],
                            sass: [
                                'vue-style-loader',
                                'css-loader',
                                {
                                    loader: 'sass-loader',
                                    options: {
                                        indentedSyntax: true
                                    }
                                }
                            ],
                        },
                        esModule: false // https://vue-loader.vuejs.org/en/options.html#esmodule
                    }
                },

                {
                    test: /\.css$/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: shouldCreateSourceMap,
                                    // Required for PostCSS Loader to work: https://github.com/postcss/postcss-loader#css-modules
                                    importLoaders: 1
                                }
                            },
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: shouldCreateSourceMap
                                }
                            }
                        ]
                    })
                },

                {
                    test: /\.s[ac]ss$/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: shouldCreateSourceMap,
                                    importLoaders: 1
                                }
                            },
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: shouldCreateSourceMap
                                }
                            },
                            {
                                loader: 'resolve-url-loader',
                                options: {
                                    sourceMap: shouldCreateSourceMap
                                }
                            },
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: true
                                }
                            },
                        ]
                    })
                },

                {
                    test: /\.(png|svg|jpe?g|gif)$/,
                    loader: 'file-loader',
                    options: {
                        name: 'img/[name].[ext]'
                    }
                },

                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/,
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[name].[ext]'
                    }
                },
            ]
        },

        plugins: ((plugins = [
            new webpack.LoaderOptionsPlugin({
                // Minimize code on production mode
                minimize: inProduction
            }),

            new webpack.DefinePlugin({
                // Used by Vue to remove browser debugging in production mode
                'process.env.NODE_ENV': JSON.stringify(NODE_ENV)
            }),

            /**
             * Creates a separate file with common modules shared between 2 or more entry points
             * https://webpack.js.org/plugins/commons-chunk-plugin/
             */
            new webpack.optimize.CommonsChunkPlugin({
                name: 'js/common',
                minChunks: 2
            }),

            /**
             * Extracts the webpack bootstrap logic
             * This way, if a file changes, it won't change the "common.js" chunk contents and chunkhash, preserving the cache
             * @see https://medium.com/webpack/predictable-long-term-caching-with-webpack-d3eee1d3fa3 "You have runtime issues"
             */
            new webpack.optimize.CommonsChunkPlugin({
                names: 'js/manifest',
                minChunks: Infinity
            }),
            
            /**
             * Because every chunk gets a numerical *chunk id*, the resulting chunkhash is also changed when importing files.
             * This prevents the "common.js" hash from changing when *async modules* are loaded.
             * @see https://medium.com/webpack/predictable-long-term-caching-with-webpack-d3eee1d3fa3 "Name your chunk"
             */
            new webpack.NamedChunksPlugin(),

            /**
             * Because every chunk gets a numerical *chunk id*, the resulting chunkhash is also changed when importing files.
             * This prevents the "manifest.js" hash from changing when *dependencies change*.
             * @see https://webpack.js.org/guides/caching/#module-identifiers
             * @see https://medium.com/webpack/predictable-long-term-caching-with-webpack-d3eee1d3fa3 "Name your modules"
             */
            new webpack[inProduction ? 'HashedModuleIdsPlugin' : 'NamedModulesPlugin'](),

            /**
             * Extracts css to separate file
             * Eg. js/app -> css/app.css
             */
            new ExtractTextPlugin({
                    filename: (getPath) => {
                        return getPath('[name].css' + (hmr ? '' : '?id=[contenthash:20]')).replace('js', 'css');
                    }
                }
            ),

            /**
             * Delete assets folders, manifest file and hot helper file
             * https://github.com/johnagan/clean-webpack-plugin
             */
            new CleanWebpackPlugin([
                path.resolve(distPath, 'js'),
                path.resolve(distPath, 'css'),
                path.resolve(distPath, 'img'),
                path.resolve(distPath, 'fonts'),
                path.resolve(publicPath, manifestName),
                path.resolve(publicPath, 'hot')
            ], {
                root: __dirname,
                verbose: false,
                dry: false
            }),

            /**
             * Copy images from resources to public
             * https://github.com/webpack-contrib/copy-webpack-plugin
             */
            new CopyWebpackPlugin([
                {
                    context: path.resolve(resourcePath, 'img'),
                    from: '**/*.+(jpg|jpeg|png|gif|svg)',
                    to: 'img'
                }
            ]),

            /**
             * Better error output
             * https://github.com/geowarin/friendly-errors-webpack-plugin
             */
            new FriendlyErrorsWebpackPlugin({clearConsole: false}),

            /**
             * Creates manifest file
             */
            function () {
                this.plugin('done', function (stats) {
                    let assets = Object.assign({}, stats.toJson().assetsByChunkName);

                    let flattenedPaths = [].concat.apply(
                        [], objectValues(assets)
                    );

                    let manifest = {};

                    flattenedPaths.forEach(versionedPath => {
                        versionedPath = path.join('/', distFolder, versionedPath).replace(/\\/g, '/');

                        let original = versionedPath.replace(/\?id=(\w{20}|\w{32})/, '');

                        manifest[original] = versionedPath;
                    });

                    fs.writeFileSync(
                        path.resolve(publicPath, manifestName),
                        JSON.stringify(manifest, null, 2)
                    );
                });
            }
        ]) => {
            if (inProduction) {

                /**
                 * Uglify minification
                 */
                plugins.push(
                    new webpack.optimize.UglifyJsPlugin({
                        sourceMap: shouldCreateSourceMap,
                        compress: {
                            warnings: false,
                            drop_console: true,
                        },
                        output: {
                            comments: false
                        }
                    })
                );

                /**
                 * Enables Scope Hoisting
                 * https://webpack.js.org/plugins/module-concatenation-plugin/
                 */
                plugins.push(
                    new webpack.optimize.ModuleConcatenationPlugin()
                );
            }

            if (hmr) {

                /**
                 * Creates hot helper file
                 */
                plugins.push(
                    function () {
                        this.plugin('done', function () {
                            fs.writeFileSync(
                                path.resolve(publicPath, 'hot'),
                                JSON.stringify('hot reloading', null, 2)
                            );
                        });
                    }
                );
            }

            return plugins;
        })(),

        devtool: inProduction ? productionSourceMap : 'inline-source-map',

        resolve: {
            alias: {
                // resolve Vue from another path
                'vue$': 'vue/dist/vue.common.js'
            }
        },

        performance: {
            hints: false
        },

        devServer: {
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            contentBase: path.join(__dirname, publicFolder),
            historyApiFallback: true,
            noInfo: true,
            compress: true,
            quiet: true
        },

        /**
         * Clean compilation output
         */
        stats: {
            hash: false,
            version: false,
            timings: false,
            children: false,
            errors: false,
            errorDetails: false,
            warnings: false,
            chunks: false,
            modules: false,
            reasons: false,
            source: false,
            publicPath: false
        }
    };
};
