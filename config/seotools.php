<?php

return [
    'meta'      => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'        => 'Well Hotel & Spa', // set false to total remove
            'description'  => 'Situado entre as praias de Santa Rita e Porto Novo, o Well Hotel & Spa surpreende e encanta todos os seus hóspedes e visitantes.', // set false to total remove
            'separator'    => ' | ',
            'keywords'     => [],
            'canonical'    => false, // Set null for using Url::current(), set false to total remove
        ],

        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
        ],
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'       => 'Well Hotel & Spa', // set false to total remove
            'description' => 'Situado entre as praias de Santa Rita e Porto Novo, o Well Hotel & Spa surpreende e encanta todos os seus hóspedes e visitantes.', // set false to total remove
            'url'         => null, // Set null for using Url::current(), set false to total remove
            'type'        => 'website',
            'site_name'   => 'Well Hotel & Spa',
            'images'      => [env('APP_URL').'/assets/img/imagem-partilha.jpg'],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
            //'card'        => 'summary',
            //'site'        => '@LuizVinicius73',
        ],
    ],
];
