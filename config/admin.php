<?php

return [

    /* Website name */
    'site' => 'Well Hotel',
    /* Upload files main folder */
    'files-path' => 'files',
    /*
     * Menu constructor
     *
     * Properties of type header:
     * - header (required)
     *
     * Properties of type link:
     * - key (required)
     * - title (default:null)
     * - controller (required)
     * - icon (default:fa fa-circle)
     * - label (default:null)
     * - create (default:true)
     * - edit (default:true)
     * - delete (default:true)
     * - onlyRoutes (default:false)
     * - autoRoutes (default:true)
     *
     * Properties of type parent
     * - title (default:null)
     * - children (required)
     *
     * */
    'menu' => [
        [
            'header' => 'Administração'
        ],
        [
            'key' => 'users',
            'title' => 'Utilizadores',
            'icon' => 'fa fa-users',
            'controller' => 'Slingshot\Admin\Http\Controllers\Manage\UsersController'
        ],
        [
            'key' => 'files',
            'title' => 'Ficheiros',
            'icon' => 'fa fa-files-o',
            'controller' => 'Slingshot\Admin\Http\Controllers\Manage\FilesController',
            'autoRoutes' => false,
            'edit' => false
        ],
        [
            'key' => 'file-types',
            'title' => 'Tipos de Ficheiros',
            'controller' => 'Slingshot\Admin\Http\Controllers\Manage\FileTypesController',
            'onlyRoutes' => true,
        ],
        [
            'key' => 'config',
            'title' => 'Configurações',
            'icon' => 'fa fa-cogs',
            'controller' => 'Slingshot\Admin\Http\Controllers\Manage\ConfigController',
            'autoRoutes' => false,
        ],
        [
            'header' => 'Marcação de Férias'
        ],
        [
            'key' => 'vacations',
            'title' => 'Marcação de Férias',
            'controller' => \App\Http\Controllers\Manage\VacationsIntervalController::class,
            'delete' => true,
            'create' => true,
        ],

        [
            'header' => 'Pedidos'
        ],
        [
            'key' => 'availability-requests',
            'title' => 'Pedido de disponibilidade',
            'controller' => \App\Http\Controllers\Manage\AvailabilityRequestsController::class,
            'delete' => false,
            'create' => false,
        ],
        [
            'key' => 'contact-requests',
            'title' => 'Pedido de contacto',
            'controller' => \App\Http\Controllers\Manage\ContactRequestController::class,
            'delete' => false,
            'create' => false,
        ],
        [
            'key' => 'proposal-requests',
            'title' => 'Pedido de Proposta',
            'controller' => \App\Http\Controllers\Manage\ProposalRequestsController::class,
            'delete' => false,
            'create' => false,
        ],
        [
            'key' => 'vouchers-Request',
            'title' => 'Pedido Informação Vouchers',
            'controller' => \App\Http\Controllers\Manage\VoucherRequestsController::class,
        ],

        [
            'header' => 'Produtos'
        ],

        [
            'key' => 'rooms',
            'title' => 'Quartos',
            'controller' => \App\Http\Controllers\Manage\RoomController::class,
        ],
        [
            'key' => 'room-price-intervals',
            'title' => 'Preços dos quartos',
            'controller' => \App\Http\Controllers\Manage\RoomPriceIntervalController::class,
            'onlyRoutes' => true,
        ],
        [
            'key' => 'experiences',
            'title' => 'Experiências',
            'controller' => \App\Http\Controllers\Manage\ExperienceController::class,
        ],

        [
            'key' => 'packs',
            'title' => 'Packs',
            'controller' => \App\Http\Controllers\Manage\PackController::class,
        ],
        [
            'key' => 'pack-price-intervals',
            'title' => 'Preços dos packs',
            'controller' => \App\Http\Controllers\Manage\PackPriceIntervalController::class,
            'onlyRoutes' => true,
        ],

        [
            'key' => 'vouchers',
            'title' => 'Vouchers',
            'controller' => \App\Http\Controllers\Manage\VoucherController::class,
        ],

        [
            'header' => 'Comodidades'
        ],

        [
            'key' => 'amenity-categories',
            'title' => 'Categorias de comodidades',
            'controller' => \App\Http\Controllers\Manage\AmenityCategoryController::class,
        ],
        [
            'key' => 'amenities',
            'title' => 'Comodidades gerais',
            'controller' => \App\Http\Controllers\Manage\AmenityController::class,
        ],

        [
            'header' => 'O Hotel'
        ],

        [
            'key' => 'spaces',
            'title' => 'Espaços',
            'controller' => \App\Http\Controllers\Manage\SpacesController::class,
        ],

        [
            'key' => 'space-types',
            'title' => 'Tipos de Espaços',
            'controller' => \App\Http\Controllers\Manage\SpaceTypesController::class,
        ],
        [
            'header' => 'Eventos'
        ],

        [
            'key' => 'events',
            'title' => 'Eventos',
            'controller' => \App\Http\Controllers\Manage\EventsController::class,
        ],

        [
            'key' => 'event-types',
            'title' => 'Tipos de Eventos',
            'controller' => \App\Http\Controllers\Manage\EventTypesController::class,
        ],

        [
            'header' => 'Outros'
        ],

        [
            'key' => 'frases',
            'title' => 'Frases',
            'controller' => \App\Http\Controllers\Manage\PhrasesController::class,
            'delete' => false,
            //'create' => false,
        ],

        [
            'key' => 'pages',
            'title' => 'Páginas',
            'controller' => \App\Http\Controllers\Manage\PagesController::class,
        ],

        [
            'key' => 'blocks',
            'title' => 'Blocos',
            'controller' => \App\Http\Controllers\Manage\BlocksController::class,
            'delete' => false,
            'create' => false,
        ],

        [
            'key' => 'promotions',
            'title' => 'Promoções',
            'controller' => \App\Http\Controllers\Manage\PromotionsController::class,
        ],

        [
            'key' => 'galleries',
            'title' => 'Galerias',
            'controller' => \App\Http\Controllers\Manage\GalleryController::class,
        ],
        [
            'key' => 'gallery-items',
            'title' => 'Imagens de galerias',
            'controller' => \App\Http\Controllers\Manage\GalleryItemController::class,
            'onlyRoutes' => true,
        ],

        [
            'key' => 'messages',
            'title' => 'Mensagens',
            'controller' => \App\Http\Controllers\Manage\MessagesController::class,
        ],

        [
            'key' => 'slides',
            'title' => 'Slides (Topo)',
            'controller' => \App\Http\Controllers\Manage\SlidesController::class,
        ],

        [
            'key' => 'bottom-slides',
            'title' => 'Slides (Rodapé)',
            'controller' => \App\Http\Controllers\Manage\BottomSlidesController::class,
        ],

        [
            'key' => 'mastheads',
            'title' => 'Banners principais',
            'controller' => \App\Http\Controllers\Manage\MastheadsController::class,
        ],
    ],
    /*
     * Config Form View
     * */
    'config-view' => 'manage.config.form',
];
